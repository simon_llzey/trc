package cn.trcfitness.custom;

import android.app.TimePickerDialog;
import android.content.Context;

/**
 * Created by Administrator on 2016/5/24.
 * 返回时不设置时间
 */
public class CustomTimePickerDialog extends TimePickerDialog {

    public CustomTimePickerDialog(Context context,  OnTimeSetListener callBack,
                                  int hourOfDay, int minute, boolean is24HourView) {
        super(context, callBack, hourOfDay, minute, is24HourView);
    }
    @Override
    protected void onStop() {
        //super.onStop();
        //注释这取消点击返回时设置时间
    }
}