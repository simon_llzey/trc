package cn.trcfitness.custom;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import cn.trcfitness.R;
import cn.trcfitness.utils.ScreenUtils;
import cn.trcfitness.view.activity.RegisterActivity;

/**
 * Created by ziv on 2016-9-22.
 */

public class MyDialog {
    private static Dialog dialog;

    public static void Dialog(final Context context) {
        View builderview = LayoutInflater.from(context).inflate(R.layout.dialog_confirm2, null);
        builderview.findViewById(R.id.tv_dialog_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        builderview.findViewById(R.id.tv_dialog_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, RegisterActivity.class);
                context.startActivity(intent);
            }
        });
        ((TextView) builderview.findViewById(R.id.tv_dialog_cancel_prompt)).setWidth(ScreenUtils.getScreenWidth(context) / 5 * 4);

        dialog = new Dialog(context, R.style.selectorDialog);
        dialog.setContentView(builderview);
        dialog.show();
    }
}
