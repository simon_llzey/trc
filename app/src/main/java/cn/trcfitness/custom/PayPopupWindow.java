package cn.trcfitness.custom;

import android.app.Activity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cn.trcfitness.R;

/**
 * Created by ziv on 2016/6/7.
 */
public class PayPopupWindow {
    Activity context;
    PopupWindow popWnd;
    public static PayPopupWindow payPopupWindow;

    public PayPopupWindow(Activity c) {
        this.context = c;
    }

    public static PayPopupWindow getInstance(Activity mCcontext) {
        if (payPopupWindow == null) {
            payPopupWindow = new PayPopupWindow(mCcontext);
        }
        return payPopupWindow;
    }

    private TextView box1, box2, box3, box4, box5, box6;

    private TextView payTitle;
    private CustomGridView mGridView;

    private List<String> mList = new ArrayList<String>();
    private StringBuffer stringBuffer = new StringBuffer();

    private int[] images = {R.drawable.keyboard_one, R.drawable.keyboard_two, R.drawable.keyboard_three,
            R.drawable.keyboard_four, R.drawable.keyboard_five, R.drawable.keyboard_sex, R.drawable.keyboard_seven,
            R.drawable.keyboard_eight, R.drawable.keyboard_nine, R.drawable.keyboard_space,
            R.drawable.keyboard_zero, R.drawable.keyboard_del};

    private int[] codes = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0};

    public void show(LinearLayout rootview, final CallbackListener listener) {
        View contentView = LayoutInflater.from(context.getApplicationContext()).inflate(R.layout.item_paypassword, null);//popu加载的布局

        initView(contentView, listener);
        popWnd = new PopupWindow();
        popWnd.setContentView(contentView);
        popWnd.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        popWnd.setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        popWnd.setAnimationStyle(R.style.contextMenuAnim);
        //显示PopupWindow
        popWnd.showAtLocation(rootview, Gravity.BOTTOM, 0, 0);
    }

    public boolean isShow() {
        if (null != popWnd && popWnd.isShowing()) {
            return true;
        } else {
            return false;
        }
    }

    public void CloseShow() {
        popWnd.dismiss();
    }


    public interface CallbackListener {
        void onFinish(String code);
    }

    private void initView(View view, final CallbackListener listener) {
        mGridView = (CustomGridView) view.findViewById(R.id.pay_grid);
        payTitle = (TextView) view.findViewById(R.id.pay_title);
        box1 = (TextView) view.findViewById(R.id.pay_box1);
        box2 = (TextView) view.findViewById(R.id.pay_box2);
        box3 = (TextView) view.findViewById(R.id.pay_box3);
        box4 = (TextView) view.findViewById(R.id.pay_box4);
        box5 = (TextView) view.findViewById(R.id.pay_box5);
        box6 = (TextView) view.findViewById(R.id.pay_box6);

        mGridView.setAdapter(new GridAdapter());

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //如果密码有6位时清空输入

                if (mList.size() < 6) {
                    if (position == 9) {
                        return;
                    }
                    if (position == 10) {
                        mList.add(codes[position - 1] + "");
                        updateInputCode();
                        return;
                    }
                    if (position == 11) {

                        if (mList.size() <= 0) {
                            Log.i("Pay", "获取删除的值小鱼0==" + mList.size());
                            return;
                        }
                        Log.i("Pay", "获取删除的值" + (mList.size() - 1));
                        mList.remove(mList.size() - 1);
                        updateInputCode();
                        return;
                    }


                    if (mList.size() == 5) {
                        mList.add(codes[position] + "");
                        if (mList.toString() != null && !mList.toString().equals("")) {
                            String userCode = "";
                            for (String code : mList) {
                                userCode += code;
                            }
                            listener.onFinish(userCode);
                        }
                        mList.clear();
                        updateInputCode();
                        return;
                    }


                    mList.add(codes[position] + "");
                    updateInputCode();
                } else {
                    if (position == 11) {

                        if (mList.size() <= 0) {
                            Log.i("Pay", "获取删除的值小鱼0==" + mList.size());
                            return;
                        }
                        Log.i("Pay", "获取删除的值" + (mList.size() - 1));
                        mList.remove(mList.size() - 1);
                        updateInputCode();
                        return;
                    }
                }


            }
        });
    }

    public void setTitle(String s) {
        payTitle.setText(s);
    }

    public void initPayWin(){
        mList.clear();
        updateInputCode();
    }


    /**
     * 键盘
     */
    private class GridAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return images.length;
        }

        @Override
        public Object getItem(int position) {
            return images[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = LayoutInflater.from(context.getApplicationContext()).inflate(R.layout.item_pay_image, null);
            ImageView imageView = (ImageView) convertView.findViewById(R.id.pay_image);

            imageView.setImageResource(images[position]);

            return convertView;
        }
    }


    private void updateInputCode() {

        if (mList.size() == 0) {
            box1.setText("");
            box2.setText("");
            box3.setText("");
            box4.setText("");
            box5.setText("");
            box6.setText("");
        } else if (mList.size() == 1) {
            box1.setText(mList.get(0));
            box2.setText("");
            box3.setText("");
            box4.setText("");
            box5.setText("");
            box6.setText("");
        } else if (mList.size() == 2) {
            box1.setText(mList.get(0));
            box2.setText(mList.get(1));
            box3.setText("");
            box4.setText("");
            box5.setText("");
            box6.setText("");
        } else if (mList.size() == 3) {
            box1.setText(mList.get(0));
            box2.setText(mList.get(1));
            box3.setText(mList.get(2));
            box4.setText("");
            box5.setText("");
            box6.setText("");
        } else if (mList.size() == 4) {
            box1.setText(mList.get(0));
            box2.setText(mList.get(1));
            box3.setText(mList.get(2));
            box4.setText(mList.get(3));
            box5.setText("");
            box6.setText("");
        } else if (mList.size() == 5) {
            box1.setText(mList.get(0));
            box2.setText(mList.get(1));
            box3.setText(mList.get(2));
            box4.setText(mList.get(3));
            box5.setText(mList.get(4));
            box6.setText("");
        } else if (mList.size() == 6) {
            box1.setText(mList.get(0));
            box2.setText(mList.get(1));
            box3.setText(mList.get(2));
            box4.setText(mList.get(3));
            box5.setText(mList.get(4));
            box6.setText(mList.get(5));
        }


    }

}
