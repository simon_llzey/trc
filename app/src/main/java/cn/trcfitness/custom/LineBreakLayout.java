package cn.trcfitness.custom;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import java.util.Hashtable;

/**
 * 自动换行的LinearLayout
 *
 * @author idengpan
 */
public class LineBreakLayout extends LinearLayout {
    int mLeft, mRight, mTop, mBottom;
    Hashtable map = new Hashtable();

    int number = 0;
    int spaceWidth = 10;


    public LineBreakLayout(Context context) {
        super(context);
    }

    public LineBreakLayout(Context context, int horizontalSpacing, int verticalSpacing) {
        super(context);
    }

    public LineBreakLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int mWidth = MeasureSpec.getSize(widthMeasureSpec);
        int mCount = getChildCount();
        int mX = 0;
        int mY = 0;
        mLeft = 0;
        mRight = 0;
        mTop = 0;
        mBottom = 0;
        int intervalX = 20;//控件X轴间隔
        int intervalY = 23;

        for (int i = 0; i < mCount; i++) {
            final View child = getChildAt(i);
            child.setPadding(50,20,50,20);
            child.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
// 此处增加onlayout中的换行判断，用于计算所需的高度
            int childw = child.getMeasuredWidth();
            int childh = child.getMeasuredHeight();

            Position position = new Position();
            mLeft = mX;
            mRight = mLeft + child.getMeasuredWidth();
            number++;
            mX += childw + intervalX; // 将每次子控件宽度进行统计叠加，如果大于设定的宽度则需要换行，高度即Top坐标也需重新设置
            if (mX - intervalX >= mWidth) {
                number = 1;
                mX = childw + intervalX;
                mY += childh;
                mLeft = 0;
                mRight = mLeft + child.getMeasuredWidth();
                mTop = mY + intervalY;
// PS：如果发现高度还是有问题就得自己再细调了
            }
            mBottom = mTop + child.getMeasuredHeight();
            mY = mTop; // 每次的高度必须记录 否则控件会叠加到一起
            position.left = mLeft;
            position.top = mTop;
            position.right = mRight;
            position.bottom = mBottom;
            map.put(child, position);
        }
        setMeasuredDimension(mWidth, mBottom);
    }

    @Override
    protected LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(0, 0); // default of 1px spacing
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {

        int count = getChildCount();
        for (int i = 0; i < count; i++) {
            View child = getChildAt(i);
            Position pos = (Position) map.get(child);
            if (pos != null) {
                child.layout(pos.left, pos.top, pos.right, pos.bottom);
            }
        }
    }

    private class Position {
        int left, top, right, bottom;
    }
}
