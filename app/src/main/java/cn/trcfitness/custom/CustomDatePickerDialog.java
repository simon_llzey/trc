package cn.trcfitness.custom;

import android.app.DatePickerDialog;
import android.content.Context;

/**
 * Created by Administrator on 2016/5/24.
 * 返回时不设置日期
 */
public class CustomDatePickerDialog extends DatePickerDialog {

    public CustomDatePickerDialog(Context context, OnDateSetListener callBack,
                              int year, int monthOfYear, int dayOfMonth) {
        super(context, callBack, year, monthOfYear, dayOfMonth);
    }
    @Override
    protected void onStop() {
        //super.onStop();
        //注释这取消点击返回时设置时间
    }
}
