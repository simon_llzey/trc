package cn.trcfitness.view.fragmet;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;

import cn.trcfitness.R;
import cn.trcfitness.application.OverallApplication;
import cn.trcfitness.controller.UserController;
import cn.trcfitness.custom.MyDialog;
import cn.trcfitness.model.AddCalendarEvent;
import cn.trcfitness.model.AllEvent;
import cn.trcfitness.model.CalendarEvent;
import cn.trcfitness.model.CalendarResultParam;
import cn.trcfitness.model.FreeInfo;
import cn.trcfitness.model.FreeRequestParam;
import cn.trcfitness.model.FreeResultParam;
import cn.trcfitness.utils.DateUtils;
import cn.trcfitness.view.activity.AddMatterAct;
import cn.trcfitness.view.activity.CalendarDetailAct;
import cn.trcfitness.view.activity.CalendarMessageAct;
import cn.trcfitness.view.activity.calendar.TodayEventAdapter;
import cn.trcfitness.view.adapter.FreeAdapter;


/**
 * 日历
 */
public class CalendarFra extends Fragment implements View.OnClickListener {

    private static final String TAG = "CalendarFra";

    //侧滑显示的列表
    private ListView freeList;
    //侧滑列表
    private List<FreeInfo> mList;
    private LinearLayout leftOpenLin;
    private DrawerLayout drawerLayout;//侧滑布局
    //记录免费活动和免费课程，用户添加的事件

    //没有事件,去运动
    private LinearLayout goRunLin;
    //加载中
    private LinearLayout proLin;

    private ListView mListView;
    private TodayEventAdapter todayEventAdapter;
    //当日事件
    private AddCalendarEvent todayEvent;
    private List<CalendarEvent> todayEventList;
    //当月事件
    private AddCalendarEvent monthEvent;
    private List<CalendarEvent> monthEventList;

    //选择的月
    private int selectMonth = -1;
    //当前年月
    private String curMonth = "";

    //选中的日期
    private String selectDate = "";

    //日历相关
    private Calendar currentCalender = Calendar.getInstance(Locale.getDefault());
    private SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("MMM - yyyy", Locale.getDefault());

    //日历view
    private CompactCalendarView compactCalendarView;

    /**
     * 当前的年月，现在日历顶端
     */
    private TextView currentMonth;
    /**
     * 上个月,下个月
     */
//    private ImageView prevMonth, nextMonth;

    private FreeAdapter adapter;

    private WindowManager windowManager;

    private String[] freeNames = {"免费课程", "免费活动"};
    private int[] freeImages = {R.drawable.free_course, R.drawable.free_action};

    private List<String> todayList = new ArrayList<String>();

    //记录所有的免费课程
    private List<FreeResultParam.CalendarFreeEvent.AllCesBean> courseAllCes;
    //记录某天的免费课程
    private List<FreeResultParam.CalendarFreeEvent.CesBean> courseCes;

    //记录所有的免费活动
    private List<FreeResultParam.CalendarFreeEvent.AllCesBean> actionAllCes;
    //记录某天的免费活动
    private List<FreeResultParam.CalendarFreeEvent.CesBean> actionCes;

    //记录所有事件
    private ArrayList<AllEvent> allEvents;
    //判断是否选中免费事件
    private boolean isFreeAction = false;
    private boolean isFreeCourse = false;

    public static CalendarFra instance;

    private FreeRequestParam requestParam = new FreeRequestParam();


    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    todayEventAdapter.notifyDataSetChanged();
                    if (allEvents != null && allEvents.size() > 0)
                        goRunLin.setVisibility(View.GONE);
                    break;
                case 1:
                    goRunLin.setVisibility(View.VISIBLE);
                    break;
            }

            proLin.setVisibility(View.GONE);

        }
    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        instance = this;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.calendar_left_list, container, false);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
        initCalendar(view);
    }

    //刷新日历
    public void refreshDate() {
        //刷新标记
        monthEvent.setSpecific_month(curMonth);
        getMonthEvent();

        //刷新事件列表
        todayEvent.setSpecific_time(selectDate);
        getTodayEvent();
    }

    /**
     * 日历相关
     *
     * @param view
     */
    private void initCalendar(View view) {
        compactCalendarView = (CompactCalendarView) view.findViewById(R.id.compactcalendar_view);
        // 当前默认今天背景色
        compactCalendarView.setCurrentDayBackgroundColor(getResources().getColor(R.color.calendar_def));
        // 选择那天背景色
        compactCalendarView.setCurrentSelectedDayBackgroundColor(getResources().getColor(R.color.calendar_sel));

        addEvents(compactCalendarView, -1);
        compactCalendarView.invalidate();
        currentMonth.setText(dateFormatForMonth.format(compactCalendarView.getFirstDayOfCurrentMonth()));
        //日历选择和滑动事件
        compactCalendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                selectDate = DateUtils.timesTurnDate(dateClicked.getTime());
                todayEvent.setSpecific_time(selectDate);
                getTodayEvent();

                if (isFreeCourse) {
                    String freeDate = DateUtils.yearMonthDay(dateClicked.getTime());
                    requestParam.setType(7);
                    requestParam.setDate(freeDate);
                    getTodayFreeEvent(requestParam);
                }
                if (isFreeAction) {
                    String freeDate = DateUtils.yearMonthDay(dateClicked.getTime());
                    requestParam.setType(8);
                    requestParam.setDate(freeDate);
                    getTodayFreeEvent(requestParam);
                }
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {

                currentMonth.setText(dateFormatForMonth.format(firstDayOfNewMonth));
                //当前月
                curMonth = DateUtils.timesTurnMonth(firstDayOfNewMonth.getTime());
                monthEvent.setSpecific_month(curMonth);
                selectMonth = Integer.parseInt(curMonth.substring((curMonth.lastIndexOf("/") + 1), curMonth.length()));
                getMonthEvent();

                if (isFreeAction) {
                    requestParam.setType(8);
                    //滑动其他月份，先清除当前月的标记
                    getFreeEvent(requestParam, true);
                    getFreeEvent(requestParam, false);
                }
                if (isFreeCourse) {
                    requestParam.setType(7);
                    getFreeEvent(requestParam, true);
                    getFreeEvent(requestParam, false);
                }

                Log.d(TAG, "获取当前月 " + curMonth);
            }
        });

        getMonthEvent();
        getTodayEvent();
    }


    /**
     * 初始化view
     *
     * @param view
     */
    private void init(View view) {
        mListView = (ListView) view.findViewById(R.id.calendar_event_list);

        goRunLin = (LinearLayout) view.findViewById(R.id.no_event_lin);
        proLin = (LinearLayout) view.findViewById(R.id.pro_lin);

        todayEvent = new AddCalendarEvent();
        monthEvent = new AddCalendarEvent();
        todayEventList = new ArrayList<CalendarEvent>();
        monthEventList = new ArrayList<CalendarEvent>();

        courseAllCes = new ArrayList<FreeResultParam.CalendarFreeEvent.AllCesBean>();
        courseCes = new ArrayList<FreeResultParam.CalendarFreeEvent.CesBean>();
        actionAllCes = new ArrayList<FreeResultParam.CalendarFreeEvent.AllCesBean>();
        actionCes = new ArrayList<FreeResultParam.CalendarFreeEvent.CesBean>();

        allEvents = new ArrayList<AllEvent>();

        todayEventAdapter = new TodayEventAdapter(getActivity(), allEvents);
        mListView.setAdapter(todayEventAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent detailIntent = new Intent(getActivity(), CalendarDetailAct.class);
                detailIntent.putExtra("date", todayEvent.getSpecific_time());
                startActivity(detailIntent);
            }
        });
        curMonth = DateUtils.timesTurnMonth(System.currentTimeMillis());
        selectMonth = Integer.parseInt(curMonth.substring((curMonth.lastIndexOf("/") + 1), curMonth.length()));
        selectDate = DateUtils.timesTurnDate(System.currentTimeMillis());
        todayEvent.setSpecific_time(selectDate);
        String current_month = DateUtils.timesTurnMonth(System.currentTimeMillis());
        monthEvent.setSpecific_month(current_month);


        //侧滑列表
        freeList = (ListView) view.findViewById(R.id.calendar_free_listview);

        mList = new ArrayList<FreeInfo>();
        for (int i = 0; i < freeNames.length; i++) {
            FreeInfo info = new FreeInfo();
            info.setImage(freeImages[i]);
            info.setName(freeNames[i]);
            mList.add(info);
        }

        adapter = new FreeAdapter(getActivity(), mList);
        freeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //免费活动和免费课程选择状态
                FreeAdapter.ViewHolder vHolder = (FreeAdapter.ViewHolder) view.getTag();
                // 改变CheckBox的状态
                vHolder.select.toggle();
                // 将CheckBox的选中状况记录下来
                adapter.getIsSelected().put(position, vHolder.select.isChecked());
                // 调整选定条目

                if (vHolder.name.getText().toString().equals("免费活动")) {
                    if (vHolder.select.isChecked() == true) {
                        requestParam.setType(8);
                        getFreeEvent(requestParam, false);
                        isFreeAction = true;
//                        Toast.makeText(getActivity(), "选中免费活动", Toast.LENGTH_SHORT).show();
                    } else {
                        requestParam.setType(8);
                        getFreeEvent(requestParam, true);
                        isFreeAction = false;
                        //取消选择时刷新事件列表
                        if (!selectDate.equals("")) {
                            todayEvent.setSpecific_time(selectDate);
                            getTodayEvent();
                        }
                    }
                } else {
                    if (vHolder.select.isChecked() == true) {
                        requestParam.setType(7);
                        getFreeEvent(requestParam, false);
//                        Toast.makeText(getActivity(), "选中免费课程", Toast.LENGTH_SHORT).show();
                        isFreeCourse = true;
                    } else {
                        requestParam.setType(7);
                        getFreeEvent(requestParam, true);
                        isFreeCourse = false;
                        if (!selectDate.equals("")) {
                            todayEvent.setSpecific_time(selectDate);
                            getTodayEvent();
                        }
                    }
                }


            }
        });
        freeList.setAdapter(adapter);

        leftOpenLin = (LinearLayout) view.findViewById(R.id.calendar_left_open);
        leftOpenLin.setOnClickListener(CalendarFra.this);
        drawerLayout = (DrawerLayout) view.findViewById(R.id.id_drawerlayout);

        //消息
        view.findViewById(R.id.calendar_msg).setOnClickListener(CalendarFra.this);

        LinearLayout calendarLeftLin = (LinearLayout) view.findViewById(R.id.calendar_left_lin);
        windowManager = getActivity().getWindowManager();


        currentMonth = (TextView) view.findViewById(R.id.currentMonth);
//        prevMonth = (ImageView) view.findViewById(R.id.prevMonth);
//        nextMonth = (ImageView) view.findViewById(R.id.nextMonth);
//        prevMonth.setOnClickListener(CalendarFra.this);
//        nextMonth.setOnClickListener(CalendarFra.this);
        view.findViewById(R.id.btn_go_run).setOnClickListener(this);


    }


    /**
     * 日历添加事件
     *
     * @param compactCalendarView
     * @param month
     */
    private void addEvents(CompactCalendarView compactCalendarView, int month) {
        compactCalendarView.removeAllEvents();

        currentCalender.setTime(new Date());
        currentCalender.set(Calendar.DAY_OF_MONTH, 1);
        Date firstDayOfMonth = currentCalender.getTime();
        String eventDay = "";
        String days = "";
        for (int i = 0; i < monthEventList.size(); i++) {
            String day = DateUtils.timesTurnDay(monthEventList.get(i).getSpecific_time());
            Log.i("Calendar", "获取事件天数" + day + "+" + month);
            currentCalender.setTime(firstDayOfMonth);
            if (month > -1) {
                currentCalender.set(Calendar.MONTH, month);
            }

            //去掉相同天
            if (!days.contains(day)) {
                currentCalender.add(Calendar.DATE, Integer.parseInt(day) - 1);
                setToMidnight(currentCalender);
                long timeInMillis = currentCalender.getTimeInMillis();
                Log.i("Calendar", "获取事件天数=====" + eventDay + "+" + timeInMillis);
                List<Event> events = getEvents(timeInMillis, Integer.parseInt(day) - 1, 0);
                compactCalendarView.addEvents(events);
            }
            days += day + "/";

        }
    }


    /**
     * 免费课程日历添加事件
     *
     * @param compactCalendarView
     * @param month
     */
    private void freeCourseAddEvents(CompactCalendarView compactCalendarView, int month) {
        currentCalender.setTime(new Date());
        currentCalender.set(Calendar.DAY_OF_MONTH, 1);
        Date firstDayOfMonth = currentCalender.getTime();
        DateUtils.timesTurnMonth(System.currentTimeMillis());
        //获取当前月

        Log.e("Calendar", "当前月" + selectMonth);
        String days = "";
        for (int i = 0; i < courseAllCes.size(); i++) {
            currentCalender.setTime(firstDayOfMonth);
            if (month > -1) {
                currentCalender.set(Calendar.MONTH, month);
            }
            String day = DateUtils.timesTurnDay(courseAllCes.get(i).getSpecific_time());

            if (!days.contains(day)) {
                currentCalender.add(Calendar.DATE, (Integer.parseInt(day) - 1));
                setToMidnight(currentCalender);
                long timeInMillis = currentCalender.getTimeInMillis();
                //获取事件的月份，不是本月不加标记
                String eventMonth = DateUtils.timesTurnMonth(courseAllCes.get(i).getSpecific_time());
                Log.e("Calendar", "当前月有事件" + eventMonth + "和" + curMonth);
                if (eventMonth.equals(curMonth)) {
                    List<Event> events = getEvents(timeInMillis, (Integer.parseInt(day) - 1), 2);
                    compactCalendarView.addEvents(events);
                }

            }
            days += day + "/";


        }

    }

    /**
     * 免费活动日历添加事件
     *
     * @param compactCalendarView
     * @param month
     */
    private void freeActionAddEvents(CompactCalendarView compactCalendarView, int month) {
        currentCalender.setTime(new Date());
        currentCalender.set(Calendar.DAY_OF_MONTH, 1);
        Date firstDayOfMonth = currentCalender.getTime();
        String days = "";
        for (int i = 0; i < actionAllCes.size(); i++) {
            currentCalender.setTime(firstDayOfMonth);
            if (month > -1) {
                currentCalender.set(Calendar.MONTH, month);
            }
            String day = DateUtils.timesTurnDay(actionAllCes.get(i).getSpecific_time());

            if (!days.contains(day)) {
                currentCalender.add(Calendar.DATE, (Integer.parseInt(day) - 1));
                setToMidnight(currentCalender);
                long timeInMillis = currentCalender.getTimeInMillis();

                List<Event> events = getEvents(timeInMillis, (Integer.parseInt(day) - 1), 1);

                compactCalendarView.addEvents(events);
            }
            days += day + "/";
        }

    }

    /**
     * 取消免费活动
     *
     * @param compactCalendarView
     * @param month
     */
    private void freeActionCancel(CompactCalendarView compactCalendarView, int month) {
        currentCalender.setTime(new Date());
        currentCalender.set(Calendar.DAY_OF_MONTH, 1);
        Date firstDayOfMonth = currentCalender.getTime();
        for (int i = 0; i < actionAllCes.size(); i++) {
            currentCalender.setTime(firstDayOfMonth);
            if (month > -1) {
                currentCalender.set(Calendar.MONTH, month);
            }
            int day = Integer.parseInt(DateUtils.timesTurnDay(actionAllCes.get(i).getSpecific_time()));


            currentCalender.add(Calendar.DATE, (day - 1));
            setToMidnight(currentCalender);
            long timeInMillis = currentCalender.getTimeInMillis();

            List<Event> events = getEvents(timeInMillis, day, 1);
            compactCalendarView.removeEvents(events);
        }
        actionAllCes.clear();

    }

    /**
     * 取消免费课程
     *
     * @param compactCalendarView
     * @param month
     */
    private void freeCourseCancel(CompactCalendarView compactCalendarView, int month) {
        currentCalender.setTime(new Date());
        currentCalender.set(Calendar.DAY_OF_MONTH, 1);
        Date firstDayOfMonth = currentCalender.getTime();
        for (int i = 0; i < courseAllCes.size(); i++) {
            currentCalender.setTime(firstDayOfMonth);
            if (month > -1) {
                currentCalender.set(Calendar.MONTH, month);
            }
            int day = Integer.parseInt(DateUtils.timesTurnDay(courseAllCes.get(i).getSpecific_time()));
            currentCalender.add(Calendar.DATE, (day - 1));
            setToMidnight(currentCalender);
            long timeInMillis = currentCalender.getTimeInMillis();

            List<Event> events = getEvents(timeInMillis, day, 2);
            compactCalendarView.removeEvents(events);
        }
        courseAllCes.clear();
    }

    /**
     * 添加多个标记
     *
     * @param timeInMillis
     * @param day
     * @param isFree       2：代表免费课程 1：代表免费活动， 默认为用户自己的事件
     * @return
     */
    private List<Event> getEvents(long timeInMillis, int day, int isFree) {

        if (isFree == 2) {
            return Arrays.asList(
                    new Event(Color.parseColor("#88ABDA"), timeInMillis, "Event 2 at " + new Date(timeInMillis)));
        } else if (isFree == 1) {
            return Arrays.asList(new Event(Color.parseColor("#1D2088"), timeInMillis, "Event at " + new Date(timeInMillis)));
        } else {
            return Arrays.asList(
                    new Event(Color.argb(255, 169, 68, 65), timeInMillis, "Event at " + new Date(timeInMillis)));
        }

        /*if (day < 2) {
            return Arrays.asList(new Event(Color.argb(255, 169, 68, 65), timeInMillis, "Event at " + new Date(timeInMillis)));
        } else if ( day > 2 && day <= 4) {
            return Arrays.asList(
                    new Event(Color.argb(255, 169, 68, 65), timeInMillis, "Event at " + new Date(timeInMillis)),
                    new Event(Color.argb(255, 100, 68, 65), timeInMillis, "Event 2 at " + new Date(timeInMillis)));
        } else {
            return Arrays.asList(
                    new Event(Color.argb(255, 169, 68, 65), timeInMillis, "Event at " + new Date(timeInMillis) ));
        }*/

       /* if (eventDay< 10) {
            return Arrays.asList(new Event(Color.parseColor("#43CD80"), timeInMillis, "Event at " + new Date(timeInMillis)),
                    new Event(Color.argb(255, 100, 68, 65), timeInMillis, "Event 2 at " + new Date(timeInMillis)));
        }else {
            return Arrays.asList(
                    new Event(Color.argb(255, 100, 68, 65), timeInMillis, "Event 2 at " + new Date(timeInMillis)));
        }*/
    }

    private void setToMidnight(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.calendar_left_open:
//                menu.toggle(true);
                drawerLayout.openDrawer(Gravity.LEFT);
                break;

            /*case R.id.nextMonth:
                // 下一个月
                compactCalendarView.showNextMonth();
                break;*/
            /*case R.id.prevMonth:
                // 上一个月
                compactCalendarView.showPreviousMonth();
                break;*/
            case R.id.calendar_msg:
                if (!OverallApplication.tourists) {
                    Intent msgIntent = new Intent(getActivity().getApplicationContext(), CalendarMessageAct.class);
                    startActivity(msgIntent);
                } else {
                    MyDialog.Dialog(getActivity());
                }
                break;

            case R.id.btn_go_run:
                if (OverallApplication.tourists) {
                    MyDialog.Dialog(getActivity());
                    return;
                }
                Intent runIntent = new Intent(getActivity(), AddMatterAct.class);
                runIntent.putExtra("time", todayEvent.getSpecific_time());
                startActivity(runIntent);
                break;

            default:
                break;
        }
    }


    /**
     * 获取当月事件
     */
    public void getMonthEvent() {
        if (OverallApplication.tourists) {
            proLin.setVisibility(View.GONE);
            return;
        }
        monthEventList.clear();
        UserController.get_calendar_event(monthEvent, new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                CalendarResultParam resultParam = (CalendarResultParam) data;
                if (resultParam != null) {
                    List<CalendarEvent> list = resultParam.getData().getCalendarEvents();
                    if (list != null && list.size() > 0) {
                        for (CalendarEvent event : list) {
                            monthEventList.add(event);
                        }
                        addEvents(compactCalendarView, selectMonth - 1);
                    }
                } else {
                    Toast.makeText(getActivity(), "没有数据", Toast.LENGTH_LONG).show();
                }


            }
        });
    }


    /**
     * 获取选中当日事件
     */
    private void getTodayEvent() {
        if (OverallApplication.tourists) {
            proLin.setVisibility(View.GONE);
            return;
        }
        todayEventList.clear();
        allEvents.clear();
        UserController.get_calendar_event(todayEvent, new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                CalendarResultParam resultParam = (CalendarResultParam) data;
                if (resultParam != null) {
                    allEvents.clear();
                    List<CalendarEvent> list = resultParam.getData().getCalendarEvents();
                    if (list != null && list.size() > 0) {
                        for (CalendarEvent event : list) {
                            todayEventList.add(event);
                            AllEvent allEvent = new AllEvent();
                            allEvent.setBegin(event.getBegin());
                            allEvent.setCreate_time(event.getCreate_time());
                            allEvent.setSpecific_time(event.getSpecific_time());
                            allEvent.setEnd(event.getEnd());
                            allEvent.setRemark(event.getRemark());
                            allEvent.setTitle(event.getTitle());
                            allEvent.setLocation(event.getLocation());
                            allEvent.setRemind(event.getRemind());
                            allEvent.setType(event.getType());
                            if (!event.isDeleted()) {
                                allEvents.add(allEvent);
                            }

                        }
                        mHandler.sendEmptyMessage(0);
                    } else {
                        mHandler.sendEmptyMessage(1);
                    }
                } else {
                    mHandler.sendEmptyMessage(1);
                    Toast.makeText(getActivity(), "没有数据", Toast.LENGTH_LONG).show();
                }

            }
        });
    }


    /**
     * 获取所有免费事件
     *
     * @param requestParam
     * @param isCancel     是否选中
     */
    private void getFreeEvent(final FreeRequestParam requestParam, final boolean isCancel) {
        if (OverallApplication.tourists) {
            proLin.setVisibility(View.GONE);
            return;
        }
        UserController.free_calendar_event(requestParam, new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                FreeResultParam resultParam = (FreeResultParam) data;
                if (resultParam != null) {
                    if (resultParam.isStatus()) {
                        FreeResultParam.CalendarFreeEvent freeEvent = resultParam.getData();
                        if (freeEvent != null) {
                            if (requestParam.getType() == 7) {
                                //免费课程
                                List<FreeResultParam.CalendarFreeEvent.AllCesBean> courses = freeEvent.getAllCes();
                                if (courses != null && courses.size() > 0) {

                                    for (FreeResultParam.CalendarFreeEvent.AllCesBean allCesBean : courses) {
                                        courseAllCes.add(allCesBean);
                                    }
                                }

                                //如果未选择就取消选中事件
                                if (isCancel) {
                                    freeCourseCancel(compactCalendarView, selectMonth - 1);
                                } else {
                                    freeCourseAddEvents(compactCalendarView, selectMonth - 1);
                                }

                            } else if (requestParam.getType() == 8) {
                                //免费活动
                                List<FreeResultParam.CalendarFreeEvent.AllCesBean> actions = freeEvent.getAllCes();
                                if (actions != null && actions.size() > 0) {
                                    for (FreeResultParam.CalendarFreeEvent.AllCesBean allCesBean : actions) {
                                        actionAllCes.add(allCesBean);
                                    }
                                }
                                if (isCancel) {
                                    freeActionCancel(compactCalendarView, selectMonth - 1);
                                } else {
                                    freeActionAddEvents(compactCalendarView, selectMonth - 1);
                                }
                            }
                        }

                    } else {
                        Toast.makeText(getActivity(), "没有免费事件", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });
    }

    /**
     * 获取当日免费事件
     *
     * @param requestParam
     */
    private void getTodayFreeEvent(final FreeRequestParam requestParam) {
        if (OverallApplication.tourists) {
            proLin.setVisibility(View.GONE);
            return;
        }
        UserController.free_calendar_event(requestParam, new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                FreeResultParam resultParam = (FreeResultParam) data;
                if (resultParam != null) {
                    if (resultParam.isStatus()) {
                        FreeResultParam.CalendarFreeEvent freeEvent = resultParam.getData();
                        if (freeEvent != null) {
                            if (requestParam.getType() == 7) {
                                //免费课程
                                List<FreeResultParam.CalendarFreeEvent.CesBean> courses = freeEvent.getCes();
                                if (courses != null && courses.size() > 0) {
                                    for (FreeResultParam.CalendarFreeEvent.CesBean allCesBean : courses) {
                                        courseCes.add(allCesBean);
                                        AllEvent allEvent = new AllEvent();
                                        allEvent.setBegin(allCesBean.getBegin());
                                        allEvent.setCreate_time(allCesBean.getCreate_time());
                                        allEvent.setSpecific_time(allCesBean.getSpecific_time());
                                        allEvent.setEnd(allCesBean.getEnd());
                                        allEvent.setRemark(allCesBean.getRemark());
                                        allEvent.setTitle(allCesBean.getTitle());
                                        allEvent.setLocation(allCesBean.getLocation());
                                        allEvent.setRemind(allCesBean.getRemind());
                                        allEvent.setType(allCesBean.getType());
                                        allEvents.add(allEvent);
                                    }
                                    mHandler.sendEmptyMessage(0);
                                }
                            } else if (requestParam.getType() == 8) {
                                //免费活动
                                List<FreeResultParam.CalendarFreeEvent.CesBean> actions = freeEvent.getCes();
                                if (actions != null && actions.size() > 0) {
                                    for (FreeResultParam.CalendarFreeEvent.CesBean allCesBean : actions) {
                                        actionCes.add(allCesBean);
                                        AllEvent allEvent = new AllEvent();
                                        allEvent.setBegin(allCesBean.getBegin());
                                        allEvent.setCreate_time(allCesBean.getCreate_time());
                                        allEvent.setSpecific_time(allCesBean.getSpecific_time());
                                        allEvent.setEnd(allCesBean.getEnd());
                                        allEvent.setRemark(allCesBean.getRemark());
                                        allEvent.setTitle(allCesBean.getTitle());
                                        allEvent.setLocation(allCesBean.getLocation());
                                        allEvent.setRemind(allCesBean.getRemind());
                                        allEvent.setType(allCesBean.getType());
                                        allEvents.add(allEvent);
                                    }
                                    mHandler.sendEmptyMessage(0);
                                }
                            }

                        }

                    } else {
                        Toast.makeText(getActivity(), "没有免费事件", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

}
