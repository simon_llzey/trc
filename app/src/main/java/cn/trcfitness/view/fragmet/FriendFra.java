package cn.trcfitness.view.fragmet;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import cn.trcfitness.R;
import cn.trcfitness.controller.FriendController;
import cn.trcfitness.model.Frends;
import cn.trcfitness.model.FriendResultParam;
import cn.trcfitness.model.UserRequestParam;
import cn.trcfitness.view.activity.SearchFriendAct;
import cn.trcfitness.view.adapter.FriendAdapter;


/**
 * 好友
 */
public class FriendFra extends Fragment {

    private PullToRefreshListView mListView;
    private FriendAdapter adapter;
    private EditText et_Search;

    private List<Frends> mList;
    private int pageOffset = 0;//页数
    private static final int REFRESH = 0;
    private static final int MORE = 1;
    private UserRequestParam userRequest;

    private LinearLayout Linlay_no_content;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_friend, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    /**
     * 初始化view
     *
     * @param view
     */
    private void initView(View view) {
        et_Search = (EditText) view.findViewById(R.id.et_fragment_friend_search);
        Linlay_no_content = (LinearLayout) view.findViewById(R.id.Linlay_frigment_friend_no_content);
        mListView = (PullToRefreshListView) view.findViewById(R.id.friend_list);
        mList = new ArrayList<Frends>();
        adapter = new FriendAdapter(getActivity().getApplicationContext(), mList, false);
        mListView.setAdapter(adapter);
        mListView.setMode(PullToRefreshBase.Mode.BOTH);
        mListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            //下拉刷新
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                pageOffset = 0;
                userRequest.setPageOffset(pageOffset);
                getFriendList(REFRESH);
            }

            //上拉加载更多
            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                if (mList != null && mList.size() > 0) {
                    pageOffset = mList.size();
                }
                userRequest.setPageOffset(pageOffset);
                getFriendList(MORE);
            }
        });

        userRequest = new UserRequestParam();
        userRequest.setPageSize(20);
        userRequest.setPageOffset(pageOffset);

        //搜索
        view.findViewById(R.id.friend_phone).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent searchIntent = new Intent(getActivity().getApplicationContext(), SearchFriendAct.class);
                startActivity(searchIntent);
            }
        });

        et_Search.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    HideKeyboard(v);
                    pageOffset = 0;
                    userRequest.setPageOffset(pageOffset);
                    userRequest.setKey(et_Search.getText().toString().trim());
                    getFriendList(REFRESH);
                }
                return false;
            }
        });
    }

    //隐藏虚拟键盘
    public static void HideKeyboard(View v) {
        InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm.isActive()) {
            imm.hideSoftInputFromWindow(v.getApplicationWindowToken(), 0);
        }
    }

    /**
     * 获取我的好友
     */
    private void getFriendList(final int state) {
        FriendController.getMyFriendList(new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                if (data != null && (FriendResultParam) data != null) {
                    FriendResultParam resultParam = (FriendResultParam) data;
                    if (state == REFRESH) {
                        mList.clear();
                        mList = resultParam.getData().getFrends();
                        RefreshFriend();
                    } else if (state == MORE) {
                        mList.addAll(resultParam.getData().getFrends());
                        RefreshFriend();
                        if (resultParam.getData().getFrends().size() == 0) {
                            mToast("没有更多好友");
                        }
                    }
                } else {
                    mListView.onRefreshComplete();
//                    mToast("更新失败");
                }
            }
        }, userRequest);
    }

    private void mToast(String text) {
        Toast.makeText(getContext().getApplicationContext(), text, Toast.LENGTH_SHORT).show();
    }

    private void RefreshFriend() {
        if (mList != null && !mList.isEmpty()) {
            Linlay_no_content.setVisibility(View.GONE);
        } else {
            Linlay_no_content.setVisibility(View.VISIBLE);
        }
        adapter.setList(mList);
        mListView.onRefreshComplete();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onStart() {
        getFriendList(REFRESH);
        super.onStart();
    }
}
