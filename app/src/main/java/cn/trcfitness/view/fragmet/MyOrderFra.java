package cn.trcfitness.view.fragmet;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import cn.trcfitness.R;
import cn.trcfitness.controller.UserController;
import cn.trcfitness.model.OrdersBean;
import cn.trcfitness.model.RequestOrders;
import cn.trcfitness.model.ResultMyOrderPaam;
import cn.trcfitness.view.adapter.MyOrderAdapter;

/**
 * Created by ziv on 2016/5/17.
 * 我的订单
 */
public class MyOrderFra extends android.support.v4.app.Fragment {

    private String type;
    private View view;
    private PullToRefreshListView listview;
    private List<OrdersBean> mlist;
    private MyOrderAdapter adapter;
    private RequestOrders requestOrders;

    private int pageOffset = 0;//页数
    private static final int REFRESH = 0;
    private static final int MORE = 1;

    public void setType(String type) {
        this.type = type;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_myorder, null);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getData(REFRESH);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        requestOrders = new RequestOrders();
        requestOrders.setPageSize(20);
        if (type.equals("1")) {
            requestOrders.setStatus(0);//待发货
        } else if (type.equals("2")) {
            requestOrders.setStatus(1);//待收货
        } else if (type.equals("3")) {
            requestOrders.setStatus(3);//待评价
        }
    }

    private void getData(final int state) {
        UserController.get_Orders(new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                if (data != null && (ResultMyOrderPaam) data != null) {
                    ResultMyOrderPaam resultMyOrderPaam = (ResultMyOrderPaam) data;
                    if (state == REFRESH) {
                        mlist.clear();
                        mlist = resultMyOrderPaam.getData().getOrders();
                        RefreshList();
                    } else if (state == MORE) {
                        mlist.addAll(resultMyOrderPaam.getData().getOrders());
                        RefreshList();
                        if (resultMyOrderPaam.getData().getOrders().size() == 0) {
                            Toast.makeText(getContext().getApplicationContext(), "没有更多数据", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        }, requestOrders);
    }

    private void RefreshList() {
        adapter.setList(mlist);
        listview.onRefreshComplete();
        adapter.notifyDataSetChanged();
    }

    private void initView() {
        listview = (PullToRefreshListView) view.findViewById(R.id.lv_fragment_myorder);
        mlist = new ArrayList<OrdersBean>();
        adapter = new MyOrderAdapter(getActivity().getApplicationContext(), mlist, type);
        listview.setAdapter(adapter);
        listview.setMode(PullToRefreshBase.Mode.BOTH);
        listview.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            //下拉刷新
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                pageOffset = 0;
                requestOrders.setPageOffset(pageOffset);
                getData(REFRESH);
            }

            //上拉加载更多
            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                if (mlist != null && mlist.size() > 0) {
                    pageOffset = mlist.size();
                }
                requestOrders.setPageOffset(pageOffset);
                getData(MORE);
            }
        });
    }
}
