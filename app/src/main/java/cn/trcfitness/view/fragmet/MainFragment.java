package cn.trcfitness.view.fragmet;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.tencent.android.tpush.XGIOperateCallback;
import com.tencent.android.tpush.XGPushConfig;
import com.tencent.android.tpush.XGPushManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import cn.trcfitness.R;
import cn.trcfitness.application.OverallApplication;
import cn.trcfitness.controller.UserController;
import cn.trcfitness.custom.MyDialog;
import cn.trcfitness.http.model.ResultParam;
import cn.trcfitness.model.TokenParam;

public class MainFragment extends FragmentActivity implements View.OnClickListener {

    private LinearLayout btnCalendar, btnTRC, btnStore, btnFriend;
    private Fragment calendarFra, trcFra, storeFra, friendFra;
    private ImageView imgCalendar, imgTrc, imgStore, imgFriend;

    private FragmentManager manager;
    private List<Fragment> fragments = new ArrayList<Fragment>();
    private List<ImageView> imgs = new ArrayList<ImageView>();

    private int currentTab;//当前Tab页面索引

    private int[][] backgraunds = {{R.drawable.calendar, R.drawable.calendar_sel},
            {R.drawable.trc, R.drawable.trc_sel},
            {R.drawable.store, R.drawable.store_sel},
            {R.drawable.friend, R.drawable.friend_sel}};

    public static MainFragment instancemian = null;

    //获取权限返回
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main_fragment);
        instancemian = this;
        manager = getSupportFragmentManager();
        initView();

        init();

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{
                            Manifest.permission.READ_PHONE_STATE},
                    MY_PERMISSIONS_REQUEST_READ_CONTACTS);
        } else {
            init();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                    init();

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }


    /**
     * 初始化view
     */
    private void initView() {
        btnCalendar = (LinearLayout) findViewById(R.id.btn_calendar);
        btnTRC = (LinearLayout) findViewById(R.id.btn_trc);
        btnStore = (LinearLayout) findViewById(R.id.btn_store);
        btnFriend = (LinearLayout) findViewById(R.id.btn_friend);

        imgCalendar = (ImageView) findViewById(R.id.img_calendar);
        imgTrc = (ImageView) findViewById(R.id.img_trc);
        imgStore = (ImageView) findViewById(R.id.img_store);
        imgFriend = (ImageView) findViewById(R.id.img_friend);

        imgs.add(imgCalendar);
        imgs.add(imgTrc);
        imgs.add(imgStore);
        imgs.add(imgFriend);

        calendarFra = new CalendarFra();
        trcFra = new TRCFra();
        storeFra = new StoreFra();
        friendFra = new FriendFra();

        fragments.add(calendarFra);
        fragments.add(trcFra);
        fragments.add(storeFra);
        fragments.add(friendFra);

        switchFragment(0);

        btnCalendar.setOnClickListener(this);
        btnTRC.setOnClickListener(this);
        btnStore.setOnClickListener(this);
        btnFriend.setOnClickListener(this);

    }

    private void switchFragment(int index) {
        Fragment fragment = fragments.get(index);
        FragmentTransaction transaction = obtainFragmentTransaction(index);
        getCurrentFragment().onPause();

        if (fragment.isAdded()) {
            fragment.onResume();
        } else {
            transaction.add(R.id.fragmentRoot, fragment);
        }
        showTab(index);
        transaction.commit();

    }

    private Fragment getCurrentFragment() {
        return fragments.get(currentTab);
    }

    private FragmentTransaction obtainFragmentTransaction(int index) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (index > currentTab) {
            transaction.setCustomAnimations(R.anim.slide_left_in, R.anim.slide_left_out);
        } else {
            transaction.setCustomAnimations(R.anim.slide_right_in, R.anim.slide_right_out);
        }

        return transaction;
    }

    private void showTab(int index) {
        for (int i = 0; i < fragments.size(); i++) {
            Fragment fragment = fragments.get(i);
            FragmentTransaction transaction = obtainFragmentTransaction(index);
            if (index == i) {
                transaction.show(fragment);
            } else {
                transaction.hide(fragment);
            }
            transaction.commit();
        }

        imgs.get(currentTab).setImageResource(backgraunds[currentTab][0]);
        currentTab = index;
        imgs.get(currentTab).setImageResource(backgraunds[currentTab][1]);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_calendar:
                switchFragment(0);
                break;
            case R.id.btn_trc:
                switchFragment(1);
                break;
            case R.id.btn_store:
                switchFragment(2);
                break;
            case R.id.btn_friend:
                if (OverallApplication.tourists) {
                    MyDialog.Dialog(this);
                    return;
                }
                switchFragment(3);
                break;

        }
    }


    /**
     * 信鸽推送
     */
    private void init() {
        // 开启logcat输出，方便debug，发布时请关闭
        XGPushConfig.enableDebug(this, true);
// 如果需要知道注册是否成功，请使用registerPush(getApplicationContext(), XGIOperateCallback)带callback版本
// 如果需要绑定账号，请使用registerPush(getApplicationContext(),account)版本
// 具体可参考详细的开发指南
// 传递的参数为ApplicationContext
        XGPushManager.registerPush(getApplicationContext(), new XGIOperateCallback() {
            @Override
            public void onSuccess(Object data, int flag) {
                Log.d("TPush", "注册成功，设备token为：" + data);
                /*Intent intent = new Intent(CMD.XINGE_REGISTER);
                intent.putExtra("token", data.toString());
                sendBroadcast(intent);*/
                initToken(data.toString());
            }

            @Override
            public void onFail(Object data, int errCode, String msg) {
                Log.d("TPush", "注册失败，错误码：" + errCode + ",错误信息：" + msg);
            }
        });


// 其它常用的API：
// 绑定账号（别名）注册：registerPush(context,account)或registerPush(context,account, XGIOperateCallback)，其中account为APP账号，可以为任意字符串（qq、openid或任意第三方），业务方一定要注意终端与后台保持一致。
// 取消绑定账号（别名）：registerPush(context,"*")，即account="*"为取消绑定，解绑后，该针对该账号的推送将失效
// 反注册（不再接收消息）：unregisterPush(context)
// 设置标签：setTag(context, tagName)
// 删除标签：deleteTag(context, tagName)
    }

    public void initToken(final String token) {
        if (OverallApplication.tourists) {
            return;
        }
        TokenParam param = new TokenParam();

        if (!token.equals("") && token != null) {
            param.setToken(token);
            param.setApp_type(1);
            Log.e("TPush", "推送保存" + param.toString());
            UserController.save_token(param, new Observer() {
                @Override
                public void update(Observable observable, Object data) {
                    ResultParam resultParam = (ResultParam) data;
                    if (resultParam != null) {
                        if (resultParam.isStatus()) {
                            Log.e("TPush", "推送保存成功" + token);
                        } else {
                            Log.e("TPush", "推送保存失败");
                        }
                    } else {
                        Log.e("TPush", "推送保存失败1");
                    }

                }
            });
        } else {
            Log.e("TPush", "推送保存失败2");
        }
    }
}
