package cn.trcfitness.view.pay;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import cn.trcfitness.R;
import cn.trcfitness.controller.UserController;
import cn.trcfitness.custom.CustomGridView;
import cn.trcfitness.http.model.ResultParam;
import cn.trcfitness.model.UserRequestParam;

/**
 * Created by Administrator on 2016/5/12.
 * 支付输入密码界面
 */
public class PayKeyboardAct extends Activity{

    private Context mContext = this;

    private TextView box1, box2, box3, box4, box5, box6;

    private TextView payTitle;
    private CustomGridView mGridView;

    //请求参数
    private UserRequestParam requestParam;

    private List<String> mList = new ArrayList<String>();
    private String reList = "";
    private StringBuffer stringBuffer = new StringBuffer();

    private int[] images = {R.drawable.keyboard_one, R.drawable.keyboard_two, R.drawable.keyboard_three,
            R.drawable.keyboard_four, R.drawable.keyboard_five, R.drawable.keyboard_sex, R.drawable.keyboard_seven,
            R.drawable.keyboard_eight, R.drawable.keyboard_nine, R.drawable.keyboard_space,
            R.drawable.keyboard_zero, R.drawable.keyboard_del};

    //密码数字
    private int[] codes = {1,2,3,4,5,6,7,8,9,0};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.item_paypassword);
        initView();
    }

    private void initView() {

        requestParam = new UserRequestParam();

        mGridView = (CustomGridView) findViewById(R.id.pay_grid);
        payTitle = (TextView)findViewById(R.id.pay_title);
        box1 = (TextView) findViewById(R.id.pay_box1);
        box2 = (TextView) findViewById(R.id.pay_box2);
        box3 = (TextView) findViewById(R.id.pay_box3);
        box4 = (TextView) findViewById(R.id.pay_box4);
        box5 = (TextView) findViewById(R.id.pay_box5);
        box6 = (TextView) findViewById(R.id.pay_box6);


        mGridView.setAdapter(new GridAdapter());

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //如果密码有6位时清空输入

                if (mList.size() < 6) {
                    if (position == 9) {
                        return;
                    }
                    if (position == 10) {
                        mList.add(codes[position - 1] + "");
                        updateInputCode();
                        return;
                    }
                    if (position == 11) {

                        if (mList.size() <= 0) {
                            Log.i("Pay", "获取删除的值小鱼0==" + mList.size());
                            return;
                        }
                        Log.i("Pay", "获取删除的值" + (mList.size() - 1));
                        mList.remove(mList.size() - 1);
                        updateInputCode();
                        return;
                    }


                    if (mList.size() == 5) {
                        mList.add(codes[position] + "");

                        if (reList.length() > 0 && mList.size() == 6) {
                            updateInputCode();

                            if (reList.equals(mList.toString())) {
                                String userCode= "";
                                for (String code : mList){
                                    userCode+=code;
                                }

                                requestParam.setPwd(userCode);
                                setPayCode();
                                PayKeyboardAct.this.finish();
                            } else {
                                Toast.makeText(mContext, "两次输入不一样", Toast.LENGTH_LONG).show();
                                //两次密码不一样时，清除
                                mList.clear();
                                updateInputCode();
                            }

                            return;
                        }
                        reList = mList.toString();
                        mList.clear();
                        updateInputCode();
                        payTitle.setText("请确认支付密码");

                        return;
                    }


                    mList.add(codes[position] + "");
                    updateInputCode();
                } else {
                    if (position == 11) {

                        if (mList.size() <= 0) {
                            Log.i("Pay", "获取删除的值小鱼0==" + mList.size());
                            return;
                        }
                        Log.i("Pay", "获取删除的值" + (mList.size() - 1));
                        mList.remove(mList.size() - 1);
                        updateInputCode();
                        return;
                    }
                }


            }
        });
    }


    /**
     * 设置支付密码
     * @param
     */
    public void setPayCode(){
        UserController.set_paycode(requestParam, new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                ResultParam result = (ResultParam) data;
                if (result.isStatus()) {
                    Toast.makeText(mContext, "设置密码成功", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(mContext, result.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }


    /**
     * 键盘
     */
    private class GridAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return images.length;
        }

        @Override
        public Object getItem(int position) {
            return images[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_pay_image, null);
            ImageView imageView = (ImageView) convertView.findViewById(R.id.pay_image);

            imageView.setImageResource(images[position]);

            return convertView;
        }
    }



    private void updateInputCode() {

        if (mList.size() == 0) {
            box1.setText("");
            box2.setText("");
            box3.setText("");
            box4.setText("");
            box5.setText("");
            box6.setText("");
        } else if (mList.size() == 1) {
            box1.setText(mList.get(0));
            box2.setText("");
            box3.setText("");
            box4.setText("");
            box5.setText("");
            box6.setText("");
        } else if (mList.size() == 2) {
            box1.setText(mList.get(0));
            box2.setText(mList.get(1));
            box3.setText("");
            box4.setText("");
            box5.setText("");
            box6.setText("");
        } else if (mList.size() == 3) {
            box1.setText(mList.get(0));
            box2.setText(mList.get(1));
            box3.setText(mList.get(2));
            box4.setText("");
            box5.setText("");
            box6.setText("");
        } else if (mList.size() == 4) {
            box1.setText(mList.get(0));
            box2.setText(mList.get(1));
            box3.setText(mList.get(2));
            box4.setText(mList.get(3));
            box5.setText("");
            box6.setText("");
        } else if (mList.size() == 5) {
            box1.setText(mList.get(0));
            box2.setText(mList.get(1));
            box3.setText(mList.get(2));
            box4.setText(mList.get(3));
            box5.setText(mList.get(4));
            box6.setText("");
        } else if (mList.size() == 6) {
            box1.setText(mList.get(0));
            box2.setText(mList.get(1));
            box3.setText(mList.get(2));
            box4.setText(mList.get(3));
            box5.setText(mList.get(4));
            box6.setText(mList.get(5));
        }


    }

}
