package cn.trcfitness.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Observable;
import java.util.Observer;

import cn.trcfitness.R;
import cn.trcfitness.controller.UserController;
import cn.trcfitness.http.model.ResultParam;
import cn.trcfitness.model.ResultAddressParam;
import cn.trcfitness.model.RequestSaveAddressParam;

/**
 * Created by ziv on 2016/5/18.
 * 添加收货地址
 */
public class NewReciptAddressActivity extends Activity implements View.OnClickListener {
    private LinearLayout Linlay_alter, Linlay_back, Linlay_region;
    private EditText et_consignee, et_phone, et_postcode, et_address;
    private TextView et_region;
    private Button bt_save_delete;
    private CheckBox ck_default;
    private RequestSaveAddressParam requestSaveAddressParam = new RequestSaveAddressParam();

    private String tapy;

    ResultAddressParam.ReciptAddressparam reciptAddressparam;//传递过来的收货地址对象

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_newreciptaddress);
        initView();
    }

    private void initView() {
        Linlay_alter = (LinearLayout) findViewById(R.id.linlay_activity_newreciptddress_alter);//修改按钮
        Linlay_back = (LinearLayout) findViewById(R.id.linlay_activity_newreciptddress_back);//返回按钮
        Linlay_region = (LinearLayout) findViewById(R.id.linlay_activity_newreciptddress_region);//选择地区点击的框

        et_consignee = (EditText) findViewById(R.id.et_activity_newreciptddress_consignee);//输入收货人
        et_phone = (EditText) findViewById(R.id.et_activity_newreciptddress_phone);//输入电话号码
        et_postcode = (EditText) findViewById(R.id.et_activity_newreciptddress_postcode);//输入邮政编码
        et_region = (TextView) findViewById(R.id.tv_activity_newreciptddress_region);//所在地区
        et_address = (EditText) findViewById(R.id.et_activity_newreciptddress_address);//输入详细地址

        bt_save_delete = (Button) findViewById(R.id.bt_activity_newreciptddress_sava);//确认按钮

        ck_default = (CheckBox) findViewById(R.id.ck_activity_newreciptddress_default);//选择是否设置为默认收货地址


        Linlay_alter.setOnClickListener(this);
        Linlay_back.setOnClickListener(this);
        Linlay_region.setOnClickListener(this);
        bt_save_delete.setOnClickListener(this);

        Intent intent = getIntent();
        tapy = intent.getStringExtra("tapy");
        if (tapy.equals("add")) {//点击添加收获地址过来的
            Linlay_alter.setVisibility(View.GONE);
            bt_save_delete.setText("确认添加");
//            Rellay_ck.setVisibility(View.VISIBLE);
        } else if (tapy.equals("alter")) {//点击每个list中的收货地址过来的，可以进行删除和修改
            Linlay_alter.setVisibility(View.VISIBLE);
            bt_save_delete.setText("删除收获地址");
            Bundle bundle = intent.getExtras();
            reciptAddressparam = (ResultAddressParam.ReciptAddressparam) bundle.getSerializable("reciptAddress");
            et_consignee.setText(reciptAddressparam.getConsignee());
            et_phone.setText(reciptAddressparam.getPhone());
            et_postcode.setText(reciptAddressparam.getPostcode());
            et_region.setText(reciptAddressparam.getRegion());
            et_address.setText(reciptAddressparam.getAddress());
            ck_default.setChecked(reciptAddressparam.getDefaults());
//            Rellay_ck.setVisibility(View.GONE);

        }
    }

    /**
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.linlay_activity_newreciptddress_back://返回
                finish();
                break;
            case R.id.linlay_activity_newreciptddress_alter://修改
                click_alter(true);
                break;

            case R.id.bt_activity_newreciptddress_sava://确认
                click_alter(false);
                break;

            case R.id.linlay_activity_newreciptddress_region://选择城市
                Intent intent = new Intent(NewReciptAddressActivity.this, SelectcityActivity.class);
                startActivityForResult(intent, 1);
                break;
        }
    }


    /**
     * 增加 修改 删除
     *
     * @param bool 判断是否为修改 true代表是修改
     */
    private void click_alter(boolean bool) {
        String sconsignee = et_consignee.getText().toString().trim();
        String sphone = et_phone.getText().toString().trim();
        String spostcode = et_postcode.getText().toString().trim();
        String sregion = et_region.getText().toString().trim();
        String saddress = et_address.getText().toString().trim();

        if (sconsignee.equals("")) {
            mToast("请输入收货人");
            return;
        } else if (sphone.equals("")) {
            mToast("请输入电话");
            return;
        } else if (sregion.equals("")) {
            mToast("请选择地区");
            return;
        } else if (saddress.equals("")) {
            mToast("请输入详细地址");
            return;
        }

        Boolean b = ck_default.isChecked();

        requestSaveAddressParam.setConsignee(sconsignee);
        requestSaveAddressParam.setPhone(sphone);
        requestSaveAddressParam.setPostcode(spostcode);
        requestSaveAddressParam.setRegion(sregion);
        requestSaveAddressParam.setAddress(saddress);
        requestSaveAddressParam.setDefaults(b);


        if (tapy.equals("add")) {//如果是添加
            alter("保存成功");
            return;
        }
        if (bool) {
            requestSaveAddressParam.setId(reciptAddressparam.getId());
            alter("修改成功");
        } else {
            delect();
        }

    }


    private void delect() {
        UserController.delReciptAddress(new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                if (data != null && (ResultParam) data != null) {
                    ResultParam resultParam = (ResultParam) data;
                    if (resultParam.getStatus().equals("200")) {
                        mToast("删除成功");
                        finish();
                    }
                }
            }
        }, reciptAddressparam.getId() + "");

    }

    private void alter(final String s) {
        UserController.saveReciptAddress(new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                if (data != null && (ResultParam) data != null) {
                    ResultParam resultParam = (ResultParam) data;
                    if (resultParam.getStatus().equals("200")) {
                        mToast(s);
                        finish();
                    }
                }
            }
        }, requestSaveAddressParam);
    }

    private void mToast(String s) {
        Toast.makeText(NewReciptAddressActivity.this, s, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            String city = data.getStringExtra("city");
            et_region.setText(city);
        }
    }
}
