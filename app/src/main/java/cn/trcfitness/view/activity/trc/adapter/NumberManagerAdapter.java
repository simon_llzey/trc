package cn.trcfitness.view.activity.trc.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.List;

import cn.trcfitness.R;
import cn.trcfitness.model.OrdersBean;
import cn.trcfitness.view.activity.ApplyAppointmentActivity;

/**
 * Created by Administrator on 2016/5/13.
 * 未消费管理
 */
public class NumberManagerAdapter extends BaseAdapter {


    private Context mContext;
    List<OrdersBean> mList;

    public NumberManagerAdapter(Context context, List<OrdersBean> mList) {
        this.mContext = context;
        this.mList = mList;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.number_item_list, null);
            viewHolder.consumer_name = (TextView) convertView.findViewById(R.id.tv_item_number_consumer_name);
            viewHolder.price = (TextView) convertView.findViewById(R.id.tv_item_number_price);
            viewHolder.product_name = (TextView) convertView.findViewById(R.id.tv_item_number_product_name);
            viewHolder.info = (TextView) convertView.findViewById(R.id.tv_item_number_info);
            viewHolder.consumer_address = (TextView) convertView.findViewById(R.id.tv_item_number_consumer_address);
            viewHolder.state = (TextView) convertView.findViewById(R.id.tv_item_number_state);
            viewHolder.number = (TextView) convertView.findViewById(R.id.tv_item_order_number);
            viewHolder.service_number = (TextView) convertView.findViewById(R.id.tv_item_number_service_number);
            viewHolder.cover = (ImageView) convertView.findViewById(R.id.im_item_number_cover);
            viewHolder.tv_applyAppointment = (TextView) convertView.findViewById(R.id.tv_item_number_applyAppointment);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        OrdersBean ordersBean = mList.get(position);

        viewHolder.consumer_name.setText(ordersBean.getConsumer_name());
        DecimalFormat fnum = new DecimalFormat("##0.00");
        String dd = fnum.format(ordersBean.getPrice());
        viewHolder.price.setText(dd + "元");
        viewHolder.product_name.setText(ordersBean.getProduct().getName());
        viewHolder.info.setText(ordersBean.getProduct().getInfo());
        viewHolder.consumer_address.setText(ordersBean.getConsumer_address());
        viewHolder.service_number.setText(ordersBean.getService_number() + "");
        viewHolder.number.setText(ordersBean.getOrder_number());
        String ImgUrl = ordersBean.getProduct().getPcs().get(0).getCover();
        if (ImgUrl != null && ImgUrl.length() > 0) {
            Picasso.with(mContext).load(ImgUrl).into(viewHolder.cover);
        }

        switch (ordersBean.getAppointment()) {
            case -1:
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                viewHolder.tv_applyAppointment.setText("预约");
                viewHolder.tv_applyAppointment.setEnabled(true);
                viewHolder.tv_applyAppointment.setBackgroundResource(R.drawable.btn_resercation);
                break;
            default:
                viewHolder.tv_applyAppointment.setText("预约中");
                viewHolder.tv_applyAppointment.setEnabled(false);
                viewHolder.tv_applyAppointment.setBackgroundResource(R.color.white);
                break;
        }
        viewHolder.tv_applyAppointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ApplyAppointmentActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("OrdersBean", mList.get(position));
                intent.putExtras(bundle);
                mContext.startActivity(intent);

            }
        });

        return convertView;
    }

    public void setList(List<OrdersBean> list) {
        this.mList = list;
    }

    public class ViewHolder {
        TextView consumer_name;
        TextView price;
        TextView product_name;
        TextView info;
        TextView consumer_address;
        TextView state;
        TextView number;
        TextView service_number;
        ImageView cover;
        TextView tv_applyAppointment;
    }

}
