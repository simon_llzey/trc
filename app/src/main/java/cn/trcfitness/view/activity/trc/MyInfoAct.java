package cn.trcfitness.view.activity.trc;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.joooonho.SelectableRoundedImageView;
import com.squareup.picasso.Picasso;

import org.hybridsquad.android.library.BitmapUtil;
import org.hybridsquad.android.library.CropHandler;
import org.hybridsquad.android.library.CropHelper;
import org.hybridsquad.android.library.CropParams;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import cn.trcfitness.R;
import cn.trcfitness.application.OverallApplication;
import cn.trcfitness.controller.UserController;
import cn.trcfitness.http.model.ResultParam;
import cn.trcfitness.model.UserDetail;
import cn.trcfitness.model.UserRequestParam;
import cn.trcfitness.model.UserResultParam;
import cn.trcfitness.utils.DataUtils;
import cn.trcfitness.utils.ImageCompressUtil;
import cn.trcfitness.utils.PhotoUtils;
import cn.trcfitness.utils.SPUtils;
import cn.trcfitness.view.activity.FeedbackActivity;
import cn.trcfitness.view.activity.LoginActivity;
import cn.trcfitness.view.activity.SetPayPSWActivity;
import cn.trcfitness.view.fragmet.MainFragment;

/**
 * trc-我的信息
 */
public class MyInfoAct extends Activity implements CropHandler, View.OnClickListener {

    private Context mContext = this;
    private LinearLayout nicknameLin, nameLin, inviteLin, selHeadLin, backLin, setpaypsw;
    private SelectableRoundedImageView headImage;
    private TextView mNickName, mName;
    private UserDetail userDetail;
    private Button bt_Exit_login;

    private static int RESULT_LOAD_IMAGE = 1;

    private UserRequestParam param;

    private CropParams mCropParams;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_my_info);

        userDetail = (UserDetail) getIntent().getSerializableExtra("user_info");
        if (userDetail != null) {
            initView();
        } else {
            Toast.makeText(mContext, "请重新登录", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        userDetail = OverallApplication.userData.getData().getUser();
    }

    /**
     * 初始化view
     */
    private void initView() {
        param = new UserRequestParam();
        param.setUserId(userDetail.getUser_id());
        mCropParams = new CropParams(mContext);
        headImage = (SelectableRoundedImageView) findViewById(R.id.my_head);
        nicknameLin = (LinearLayout) findViewById(R.id.my_nickname_lin);
        nameLin = (LinearLayout) findViewById(R.id.my_name_lin);
        inviteLin = (LinearLayout) findViewById(R.id.my_invite_code_lin);
        selHeadLin = (LinearLayout) findViewById(R.id.my_select_head);
        bt_Exit_login = (Button) findViewById(R.id.bt_activity_info_Exit_login);
        backLin = (LinearLayout) findViewById(R.id.my_back);
        setpaypsw = (LinearLayout) findViewById(R.id.Linlay_activity_info_setpaypsw);

        mNickName = (TextView) findViewById(R.id.my_nickname);
        mName = (TextView) findViewById(R.id.my_name);

        if (userDetail.getImg() != null) {
            Picasso.with(mContext).load(userDetail.getImg()).into(headImage);
        }

        mNickName.setText(userDetail.getNick_name());
        mName.setText(userDetail.getName());

        nicknameLin.setOnClickListener(this);
        nameLin.setOnClickListener(this);
        inviteLin.setOnClickListener(this);
        selHeadLin.setOnClickListener(this);
        backLin.setOnClickListener(this);
        setpaypsw.setOnClickListener(this);
        bt_Exit_login.setOnClickListener(this);
        findViewById(R.id.feedback).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.my_select_head:
                //获取裁剪的图片
                mCropParams.refreshUri();
                mCropParams.enable = true;
                mCropParams.compress = false;
                Intent intent = CropHelper.buildGalleryIntent(mCropParams);
                startActivityForResult(intent, CropHelper.REQUEST_CROP);
                break;

            case R.id.my_nickname_lin:
                //设置昵称
                Intent nickIntent = new Intent(mContext, NickNameAct.class);
                nickIntent.putExtra("name", "nickname");
                startActivityForResult(nickIntent, 2);
                break;
            case R.id.my_name_lin:
                //设置姓名
                Intent nameIntent = new Intent(mContext, NickNameAct.class);
                nameIntent.putExtra("name", "name");
                startActivityForResult(nameIntent, 3);
                break;
            case R.id.my_invite_code_lin:
                Intent inviteIntent = new Intent(mContext, InviteCodeAct.class);
                startActivity(inviteIntent);
                break;

            case R.id.my_back:
                MyInfoAct.this.finish();
                break;

            case R.id.Linlay_activity_info_setpaypsw:
                //设置支付密码
                Intent intent1 =new Intent(MyInfoAct.this, SetPayPSWActivity.class);
                startActivity(intent1);
                break;
            case R.id.bt_activity_info_Exit_login:
                //退出当前账号
                Exit_Login();
                break;
            case R.id.feedback:
                //反馈
                Intent feedIntent = new Intent(mContext, FeedbackActivity.class);
                startActivity(feedIntent);
                break;
        }
    }

    private void Exit_Login() {
        UserController.ExitLogin(new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                if (data != null && (ResultParam) data != null) {
                    ResultParam resultParam = (ResultParam) data;
                    if (resultParam.getStatus().equals("200")) {
//                        SPUtils.remove(MyInfoAct.this, "username");
                        SPUtils.remove(MyInfoAct.this, "password");
                        Intent intent = new Intent(MyInfoAct.this, LoginActivity.class);
                        startActivity(intent);
                        MyInfoAct.this.finish();
                        MainFragment.instancemian.finish();
                    }
                }
            }
        });
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        CropHelper.handleResult(this, requestCode, resultCode, data);

        switch (requestCode) {
            case 2:
                String result = data.getStringExtra("name");//只能在这，拿出去裁剪图片会出问题
                if (result != null && !result.equals("")) {
                    mNickName.setText(result);
                    OverallApplication.userData.getData().getUser().setNick_name(result);
                }
                break;

            case 3:
                String result2 = data.getStringExtra("name");
                if (result2 != null && !result2.equals("")) {
                    mName.setText(result2);
                    OverallApplication.userData.getData().getUser().setName(result2);
                }
                break;
        }
    }

    @Override
    public void onPhotoCropped(Uri uri) {
        if (!mCropParams.compress) {
            String path = getRealPathFromURI(uri);
//            param.setFile(new File(path));
            uploadImages(path);
            Log.i("Myinfo", "获取用户====" + path);
        }
    }

    @Override
    public void onCompressed(Uri uri) {
        String path = getRealPathFromURI(uri);
//        param.setFile(new File(path));
//        setPostHead(param);

        headImage.setImageBitmap(BitmapUtil.decodeUriAsBitmap(this, uri));
    }

    @Override
    public void onCancel() {
    }

    @Override
    public void onFailed(String message) {
        Toast.makeText(mContext, "选择图片失败", Toast.LENGTH_LONG).show();
    }

    @Override
    public void handleIntent(Intent intent, int requestCode) {
        startActivityForResult(intent, requestCode);
    }

    @Override
    public CropParams getCropParams() {
        return mCropParams;
    }

    /**
     * uri转string
     *
     * @param uri
     * @return
     */
    public String getRealPathFromURI(Uri uri) {
        if (null == uri) return null;
        final String scheme = uri.getScheme();
        String data = null;
        if (scheme == null)
            data = uri.getPath();
        else if (ContentResolver.SCHEME_FILE.equals(scheme)) {
            data = uri.getPath();
        } else if (ContentResolver.SCHEME_CONTENT.equals(scheme)) {
            Cursor cursor = mContext.getContentResolver().query(uri, new String[]{MediaStore.Images.ImageColumns.DATA}, null, null, null);
            if (null != cursor) {
                if (cursor.moveToFirst()) {
                    int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                    if (index > -1) {
                        data = cursor.getString(index);
                    }
                }
                cursor.close();
            }
        }
        return data;
    }


    /**
     * 单张图片上传
     *
     * @param chosePath
     */
    public void uploadImages(String chosePath) {

        File tempFile = new File(chosePath);
        if (chosePath.equals(null) || chosePath.equals("") || tempFile == null || DataUtils.getFileSizes(tempFile) == 0) {
            Toast.makeText(mContext, "请选择正确的图片", Toast.LENGTH_SHORT).show();
            return;
        }
        try {
            if (DataUtils.getFileSizes(tempFile) > ImageCompressUtil.UPLOADIMG_MAX_SIZE) {
                tempFile = PhotoUtils.scal(Uri.fromFile(tempFile));
            }
            final File tempLowFile = tempFile;
            final Map<String, File> upFiles = new HashMap<String, File>();
            upFiles.put("file", tempLowFile);

            //上传成功设置头像
            UserController.uploadImgs(param, upFiles, new Observer() {
                @Override
                public void update(Observable observable, Object data) {
                    UserResultParam resultParam = (UserResultParam) data;
                    if (resultParam != null) {
                        if (resultParam.isStatus()) {
                            Toast.makeText(mContext, "上传成功", Toast.LENGTH_LONG).show();
                            if (resultParam.getData() != null) {
                                Picasso.with(mContext).load(resultParam.getData().getPath()).into(headImage);
                                OverallApplication.userData.getData().getUser().setImg(resultParam.getData().getPath());
                            } else {
                                Toast.makeText(mContext, "上传失败", Toast.LENGTH_LONG).show();
                            }

                        } else {
                            Toast.makeText(mContext, "上传失败", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(mContext, "请检查网络", Toast.LENGTH_LONG).show();
                    }


                }
            });
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "选择的图片太大，请重新选择", Toast.LENGTH_SHORT).show();
            return;
        }
    }

    @Override
    protected void onDestroy() {
        CropHelper.clearCacheDir();
        super.onDestroy();
    }

}
