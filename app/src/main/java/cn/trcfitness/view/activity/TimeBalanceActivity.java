package cn.trcfitness.view.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import cn.trcfitness.R;
import cn.trcfitness.controller.UserController;
import cn.trcfitness.custom.LoadDialog;
import cn.trcfitness.model.OrdersBean;
import cn.trcfitness.model.ResultUnPreingOrdersParam;
import cn.trcfitness.model.UserRequestParam;
import cn.trcfitness.view.adapter.TimeBalanceAdapter;

/**
 * Created by ziv on 2016/7/8.
 * 余额管理-时间余额
 */
public class TimeBalanceActivity extends Activity implements View.OnClickListener {
    private LinearLayout Linlay_back;
    private PullToRefreshListView mListView;
    private TextView tv_no_content;
    private TimeBalanceAdapter adapter;

    private List<OrdersBean> mList;
    private int pageOffset = 0;//页数
    private static final int REFRESH = 0;
    private static final int MORE = 1;
    private UserRequestParam page;

    private LoadDialog loadDialog;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_timebalance);
        loadDialog = new LoadDialog(this);
        loadDialog.show();
        initView();
        page = new UserRequestParam();
        page.setPageSize(20);
        page.setPageOffset(pageOffset);
        getData(REFRESH);
    }

    private void initView() {
        tv_no_content = (TextView) findViewById(R.id.tv_activity_timebalance_no_content);
        Linlay_back = (LinearLayout) findViewById(R.id.Linlay_activity_timebalance_back);
        mListView = (PullToRefreshListView) findViewById(R.id.lv_activity_timebalance);
        Linlay_back.setOnClickListener(this);

        mList = new ArrayList<OrdersBean>();
        adapter = new TimeBalanceAdapter(this, mList);
        mListView.setAdapter(adapter);

        mListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            //下拉刷新
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                pageOffset = 0;
                page.setPageOffset(pageOffset);
                getData(REFRESH);
            }

            //上拉加载更多
            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
//                if (mList != null && mList.size() > 0) {
//                    pageOffset = mList.size();
//                }
                page.setPageOffset(pageOffset);
                getData(MORE);
            }
        });

    }

    private void getData(final int state) {
        UserController.get_UnPreingOrders(new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                loadDialog.cancel();
                if (data != null && (ResultUnPreingOrdersParam) data != null) {
                    ResultUnPreingOrdersParam resultUnPreingOrdersParam = (ResultUnPreingOrdersParam) data;
                    if (resultUnPreingOrdersParam.getStatus().equals("200")) {
                        List<OrdersBean> listbean = resultUnPreingOrdersParam.getData().getOrders();
                        pageOffset = pageOffset + listbean.size();
                        for (int i = 0; i < listbean.size(); i++) {
                            OrdersBean bean = listbean.get(i);
                            if (!bean.getService_type()) {
                                listbean.remove(i);
                                i--;
                            } else {
                            }
                        }
                        if (state == REFRESH) {
                            mList.clear();
                            mList = listbean;
                            RefreshList();
                        } else if (state == MORE) {
                            mList.addAll(listbean);
                            RefreshList();
                            if (listbean.size() == 0) {
                                Toast.makeText(TimeBalanceActivity.this, "没有更多数据", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            }
        }, page);
    }

    private void RefreshList() {
//        if (mList != null && !mList.isEmpty()) {
//            tv_no_content.setVisibility(View.GONE);
//        } else {
//            tv_no_content.setVisibility(View.VISIBLE);
//        }
        adapter.setList(mList);
        mListView.onRefreshComplete();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.Linlay_activity_timebalance_back:
                finish();
                break;
        }
    }
}
