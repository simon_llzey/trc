package cn.trcfitness.view.activity.trc.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Observable;
import java.util.Observer;

import cn.trcfitness.R;
import cn.trcfitness.controller.UserController;
import cn.trcfitness.http.model.ResultParam;
import cn.trcfitness.model.RequestProducts;
import cn.trcfitness.model.ResultAppointmentParam;
import cn.trcfitness.model.ResultProductParam;
import cn.trcfitness.model.UserRequestParam;
import cn.trcfitness.utils.DateUtils;

/**
 * Created by Administrator on 2016/5/17.
 */
public class ReservationDialog {
    public interface ReservationDialogListener {
        void onFinish(String code);
    }

    /**
     * @param context
     * @param cesBean  订单对象
     * @param listener 成功回调
     * @return
     */
    public static Dialog showDialog(final Context context, ResultAppointmentParam.DataBean.CesBean cesBean, final ReservationDialogListener listener) {
        View view = LayoutInflater.from(context).inflate(R.layout.reservation_commit_dialog, null);
        final ViewGroup linlay_storedetailspec;
        final EditText opinion = (EditText) view.findViewById(R.id.tv_reservation_dialog_input_opinion);
        TextView tv_begin = (TextView) view.findViewById(R.id.tv_reservation_dialog_begin);
        TextView tv_end = (TextView) view.findViewById(R.id.tv_reservation_dialog_end);
        TextView tv_price = (TextView) view.findViewById(R.id.tv_reservation_dialog_price);
        TextView tv_name = (TextView) view.findViewById(R.id.tv_reservation_dialog_name);
        final TextView tv_info = (TextView) view.findViewById(R.id.tv_reservation_dialog_info);
        Button bt = (Button) view.findViewById(R.id.tv_reservation_dialog_set_mianmi_open);

        final UserRequestParam userRequestParam = new UserRequestParam();
        userRequestParam.setOid(cesBean.getOrder().getId());
        userRequestParam.setCid(cesBean.getId());
        userRequestParam.setPid(cesBean.getOrder().getPid());


        tv_begin.setText(DateUtils.date3(cesBean.getBegin()));
        tv_end.setText("～" + DateUtils.date4(cesBean.getEnd()));
        tv_price.setText(cesBean.getOrder().getPrice() + "");
        tv_name.setText(cesBean.getOrder().getProduct_name());
        RequestProducts requestProducts = new RequestProducts();
        requestProducts.setId(cesBean.getOrder().getPid());
        UserController.get_Products(new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                if (data != null && (ResultProductParam) data != null) {
                    ResultProductParam resultProductParam = (ResultProductParam) data;
                    if (resultProductParam.getStatus().equals("200")) {
                        tv_info.setText(resultProductParam.getData().getProducts().get(0).getInfo());
                    }
                }
            }
        }, requestProducts);

        linlay_storedetailspec = (ViewGroup) view.findViewById(R.id.linlay_reservation_dialog_ecaluation);//星级
        final int[] currentTabIndex = {-1};//当前选择 星级=currentTabIndex+1
        View.OnClickListener mClickListene = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Boolean b = false;
                if (v == linlay_storedetailspec.getChildAt(currentTabIndex[0])) {
                    return;
                }
                for (int i = 0; i < linlay_storedetailspec.getChildCount(); i++) {
                    ((ImageView) linlay_storedetailspec.getChildAt(i)).setImageResource(R.drawable.star);//将所有的设为选中+
                    if (b) {//以后的全改为未选中
                        ((ImageView) linlay_storedetailspec.getChildAt(i)).setImageResource(R.drawable.grv_activity_storepersonal5);
                    }
                    if (v == linlay_storedetailspec.getChildAt(i)) {//当前选择
                        currentTabIndex[0] = i;
                        b = true;
                    }
                }
            }
        };
        for (int i = 0; i < linlay_storedetailspec.getChildCount(); i++) {
            (linlay_storedetailspec.getChildAt(i)).setOnClickListener(mClickListene);
        }

        final Dialog dialog = new AlertDialog.Builder(context).create();
        dialog.show();
        dialog.getWindow().setContentView(view);
        //显示输入框
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("测试", "星级" + currentTabIndex[0] + "--------------评价" + opinion.getText().toString().trim());
                if (currentTabIndex[0] == -1) {
                    Toast.makeText(context, "请选择星级", Toast.LENGTH_SHORT).show();
                    return;
                }
                String evaluate = opinion.getText().toString().trim();
                if (evaluate.equals("")) {
                    Toast.makeText(context, "请填写评论", Toast.LENGTH_SHORT).show();
                    return;
                }
                userRequestParam.setScore(currentTabIndex[0] + 1);
                userRequestParam.setEvaluate(evaluate);
                UserController.evaluate(new Observer() {
                    @Override
                    public void update(Observable observable, Object data) {
                        if (data != null && (ResultParam) data != null) {
                            ResultParam resultParam = (ResultParam) data;
                            if (resultParam.getStatus().equals("200")) {
                                Toast.makeText(context, "评价成功", Toast.LENGTH_SHORT).show();
                                listener.onFinish("1");
                                dialog.dismiss();
                            }
                        }
                    }
                }, userRequestParam);

            }
        });
        return dialog;
    }


}
