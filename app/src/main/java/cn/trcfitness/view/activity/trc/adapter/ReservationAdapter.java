package cn.trcfitness.view.activity.trc.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import cn.trcfitness.R;
import cn.trcfitness.controller.UserController;
import cn.trcfitness.http.model.ResultParam;
import cn.trcfitness.model.RequestProducts;
import cn.trcfitness.model.ResultAppointmentParam;
import cn.trcfitness.model.ResultProductParam;
import cn.trcfitness.model.UserRequestParam;
import cn.trcfitness.utils.DateUtils;
import cn.trcfitness.view.activity.trc.dialog.ReservationDialog;

/**
 * Created by Administrator on 2016/5/12.
 * 预约管理
 */
public class ReservationAdapter extends BaseAdapter {

    private Context mContext;
    private List<ResultAppointmentParam.DataBean.CesBean> mList;

    public ReservationAdapter(Context context, List<ResultAppointmentParam.DataBean.CesBean> datas) {
        this.mContext = context;
        this.mList = datas;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder vHolder = null;
        if (convertView == null) {
            vHolder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.reservation_item_info, null);
            vHolder.mCancel = (TextView) convertView.findViewById(R.id.reservation_item_cancel);//取消
            vHolder.mConsumernName = (TextView) convertView.findViewById(R.id.reservation_item_consumer_name);//消费商家名称
            vHolder.mProduct_name = (TextView) convertView.findViewById(R.id.reservation_item_product_name);//商品名称
            vHolder.mPrice = (TextView) convertView.findViewById(R.id.reservation_item_price);//商品单价
            vHolder.mInfo = (TextView) convertView.findViewById(R.id.reservation_item_info);//商品卖点
            vHolder.mConsumernAddress = (TextView) convertView.findViewById(R.id.reservation_item_consumer_addres);//消费地址
            vHolder.mState = (TextView) convertView.findViewById(R.id.reservation_item_state);//商品数量
            vHolder.mOrder_number = (TextView) convertView.findViewById(R.id.reservation_item_order_number);//订单号
            vHolder.mPreTime = (TextView) convertView.findViewById(R.id.reservation_item_pre_time);//时间
            vHolder.im = (ImageView) convertView.findViewById(R.id.item_reservation_item);
            convertView.setTag(vHolder);
        } else {
            vHolder = (ViewHolder) convertView.getTag();
        }

        vHolder.mConsumernName.setText(mList.get(position).getOrder().getConsumer_name());
        vHolder.mProduct_name.setText(mList.get(position).getOrder().getProduct_name());

        DecimalFormat fnum = new DecimalFormat("##0.00");
        String dd = fnum.format(mList.get(position).getOrder().getPrice());

        vHolder.mPrice.setText(dd + "元");
        vHolder.mConsumernAddress.setText(mList.get(position).getOrder().getConsumer_address());
        vHolder.mOrder_number.setText(mList.get(position).getOrder().getOrder_number() + "");

        String begin = DateUtils.date3(mList.get(position).getBegin());
        String end = DateUtils.date4(mList.get(position).getEnd());
        vHolder.mPreTime.setText(begin + "～" + end);

        final int state = mList.get(position).getStatus();
        switch (state) {
            case 0://等待确认
                vHolder.mState.setText("等待商家确认预约");
                vHolder.mCancel.setVisibility(View.VISIBLE);
                vHolder.mCancel.setText("取消");
                break;
            case 1://接受
                vHolder.mState.setText("商家已确认预约");
                vHolder.mCancel.setVisibility(View.GONE);
                break;
            case 2://拒绝
                vHolder.mState.setText("商家已拒绝预约");
                vHolder.mCancel.setVisibility(View.GONE);
                break;
            case 3://完成
                vHolder.mState.setText("预约已完成");
                vHolder.mCancel.setVisibility(View.VISIBLE);
                vHolder.mCancel.setText("去评价");
                break;
        }

        RequestProducts requestProducts = new RequestProducts();
        requestProducts.setId(mList.get(position).getOrder().getPid());
        getProducts(requestProducts, vHolder.mInfo, vHolder.im);

        View.OnClickListener mOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (state) {
                    case 0://等待确认
                        cancellation(position);
                        break;
                    case 1://接受
                        break;
                    case 2://拒绝
                        break;
                    case 3://完成
                        ReservationDialog.showDialog(mContext, mList.get(position), new ReservationDialog.ReservationDialogListener() {
                            @Override
                            public void onFinish(String code) {
                                if (code.equals("1")) {
                                    mList.remove(position);
                                    notifyDataSetChanged();
                                }
                            }
                        });
                        break;
                }
            }
        };
        vHolder.mCancel.setOnClickListener(mOnClickListener);
        return convertView;
    }

    private void cancellation(final int position) {
        UserRequestParam userRequestParam = new UserRequestParam();
        userRequestParam.setId(mList.get(position).getId());
        userRequestParam.setStatus(4);
        UserController.updateCalendarStatus(new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                if (data != null && (ResultParam) data != null) {
                    ResultParam resultParam = (ResultParam) data;
                    if (resultParam.getStatus().equals("200")) {
                        Toast.makeText(mContext, "取消成功", Toast.LENGTH_SHORT).show();
                        mList.remove(position);
                        notifyDataSetChanged();
                    }
                }
            }
        }, userRequestParam);

    }


    /**
     * 获取商品详情
     */
    private void getProducts(final RequestProducts requestProducts, final TextView mInfo, final ImageView im) {
        UserController.get_Products(new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                if (data != null && (ResultProductParam) data != null) {
                    ResultProductParam resultProductParam = (ResultProductParam) data;
                    if (resultProductParam.getStatus().equals("200")) {
                        mInfo.setText(resultProductParam.getData().getProducts().get(0).getInfo());

                        String ImgUrl = resultProductParam.getData().getProducts().get(0).getPcs().get(0).getCover();
                        if (ImgUrl != null && ImgUrl.length() > 0) {
                            Picasso.with(mContext).load(ImgUrl).into(im);
                        }
                    }
                }
            }
        }, requestProducts);
    }

    public void setList(List<ResultAppointmentParam.DataBean.CesBean> list) {
        this.mList = list;
    }

    private class ViewHolder {
        TextView mCancel, mConsumernName, mProduct_name, mPrice, mInfo, mConsumernAddress, mState, mOrder_number, mPreTime;
        ImageView im;
    }

}
