package cn.trcfitness.view.activity;

import android.app.Activity;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Observable;
import java.util.Observer;

import cn.trcfitness.R;
import cn.trcfitness.controller.UserController;
import cn.trcfitness.custom.LoadDialog;
import cn.trcfitness.http.model.ResultParam;
import cn.trcfitness.model.RequestValidateCode;
import cn.trcfitness.model.UserRequestParam;

/**
 * Created by ziv on 2016/7/14.
 */
public class SetPayPSWActivity extends Activity implements View.OnClickListener {
    private EditText phone, validateCode, password, password1;
    private TextView tv_getverification;
    private Button bt_lobin;
    private TimeCount time;
    private LoadDialog loadDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_setpaypsw);
        loadDialog = new LoadDialog(this);
        initview();
    }

    private void initview() {
        phone = (EditText) findViewById(R.id.et_activity_setpsw_phone);//输入手机号
        validateCode = (EditText) findViewById(R.id.et_activity_setpsw_verification);//输入验证码
        password = (EditText) findViewById(R.id.et_activity_setpsw_password);//输入密码
        password1 = (EditText) findViewById(R.id.et_activity_setpsw_password2);//确认密码


        tv_getverification = (TextView) findViewById(R.id.tv_activity_setpsw_getverification);//获取验证码
        bt_lobin = (Button) findViewById(R.id.bt_activity_setpsw_login);//确定
        tv_getverification.setOnClickListener(this);
        bt_lobin.setOnClickListener(this);

        time = new TimeCount(60000, 1000);
        findViewById(R.id.linlay_activity_setpsw_back).setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_activity_setpsw_getverification:
                ValidateCode();
                break;
            case R.id.bt_activity_setpsw_login:
                Change_pwd();
                break;
            case R.id.linlay_activity_setpsw_back:
                SetPayPSWActivity.this.finish();
                break;
        }

    }

    /**
     * 确认更改密码
     */
    private void Change_pwd() {
        String sphone = phone.getText().toString().trim();
        String svalidateCode = validateCode.getText().toString().trim();
        String spassword = password.getText().toString().trim();
        String spassword1 = password1.getText().toString().trim();
        if (sphone.equals("")) {
            mToast("请输入手机号");
            return;
        }
        if (svalidateCode.equals("")) {
            mToast("请输入验证码");
            return;
        }
        if (spassword.equals("") || spassword.length() != 6) {
            mToast("请正确输入支付密码");
            return;
        }
        if ((spassword1.equals("") || spassword1.length() != 6)) {
            mToast("请正确输入确认密码");
            return;
        }

        if (spassword.equals(spassword1)) {
            UserRequestParam userRequestParam = new UserRequestParam();
            userRequestParam.setPhone(sphone);
            userRequestParam.setPwd(spassword);
            userRequestParam.setCaptcha(Integer.parseInt(svalidateCode));
            loadDialog.show();
            UserController.change_paypwd(new Observer() {
                @Override
                public void update(Observable observable, Object data) {
                    loadDialog.cancel();
                    if (data != null && (ResultParam) data != null) {
                        ResultParam resultParam = (ResultParam) data;
                        if (resultParam.getStatus().equals("200")) {
                            mToast("支付密码修改成功");
                            SetPayPSWActivity.this.finish();
                            finish();
                        }
                    }
                }
            }, userRequestParam);


        } else {
            Toast.makeText(this, "密码不一致", Toast.LENGTH_SHORT).show();
        }


    }

    /**
     * 获取验证码
     */
    private void ValidateCode() {
        String sphone = phone.getText().toString().trim();
        if (sphone.equals("")) {
            mToast("请输入手机号");
            return;
        }
        loadDialog.show();
        RequestValidateCode requestValidateCode = new RequestValidateCode();
        requestValidateCode.setPhone(sphone);
        requestValidateCode.setType("mod");
        UserController.validate_code(new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                loadDialog.cancel();
                if (data != null && (ResultParam) data != null) {
                    ResultParam resultParam = (ResultParam) data;
                    if (resultParam.getStatus().equals("200")) {
                        mToast("短信发送成功");
                        time.start();//开启倒计时
                    }
                }
            }
        }, requestValidateCode);
    }

    private void mToast(String s) {
        Toast.makeText(SetPayPSWActivity.this, s, Toast.LENGTH_SHORT).show();
    }

    /**
     * 获取验证码的按钮变化
     */
    class TimeCount extends CountDownTimer {

        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            tv_getverification.setClickable(false);
            tv_getverification.setText(millisUntilFinished / 1000 + "s后重新发送");
        }

        @Override
        public void onFinish() {
            tv_getverification.setText("重新获取验证码");
            tv_getverification.setClickable(true);

        }
    }
}
