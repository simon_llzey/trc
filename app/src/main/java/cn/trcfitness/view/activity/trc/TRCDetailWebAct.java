package cn.trcfitness.view.activity.trc;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;

import cn.trcfitness.R;
import cn.trcfitness.model.ProductsBean;
import cn.trcfitness.model.TRCParam;

/**
 * 帖子详情界面
 */
public class TRCDetailWebAct extends Activity {

    private WebView mWebView;
    private TextView titleText, tv_back;
    private Bundle bundle;

    private TRCParam.DataBean.PostsBean postsBean;
    private ProductsBean productsBean;//商品详细


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_trcdetail_web);
        initView();
        bundle = getIntent().getExtras();
        String state = bundle.getString("state");
        if (state.equals("TRC")) {
            showTRC();
        } else {
            showStore();
        }

    }

    private void initView() {
        titleText = (TextView) findViewById(R.id.title);
        tv_back = (TextView) findViewById(R.id.tv_activity_web_back);
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TRCDetailWebAct.this.finish();
            }
        });
    }

    private void showStore() {
        productsBean = (ProductsBean) bundle.getSerializable("products");//获取商品对象
        tv_back.setText("返回");
        if (productsBean != null) {
            titleText.setText(productsBean.getName());
            init(productsBean.getDetail());
        } else {
            Toast.makeText(this, "请检查网络", Toast.LENGTH_SHORT).show();
        }
    }

    private void showTRC() {
        postsBean = (TRCParam.DataBean.PostsBean) getIntent().getSerializableExtra("web_content");
        if (postsBean != null) {
            titleText.setText(postsBean.getTitle());
            init(postsBean.getContent());
        } else {
            Toast.makeText(this, "请检查网络", Toast.LENGTH_SHORT).show();
        }
    }

    private void init(String content) {
        mWebView = (WebView) findViewById(R.id.webView);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setDefaultTextEncodingName("UTF -8");//设置默认为utf-8
        mWebView.loadData(content, "text/html; charset=UTF-8", null);
    }

}
