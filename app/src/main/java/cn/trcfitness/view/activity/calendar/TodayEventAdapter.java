package cn.trcfitness.view.activity.calendar;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cn.trcfitness.R;
import cn.trcfitness.model.AllEvent;
import cn.trcfitness.model.CalendarEvent;
import cn.trcfitness.utils.DateUtils;

/**
 * Created by Administrator on 2016/6/7.
 * 当日事件
 */
public class TodayEventAdapter extends BaseAdapter{
    private ArrayList<AllEvent> eventList;
    private Context mContext;
    public TodayEventAdapter(Context context, ArrayList<AllEvent> list){
        this.mContext = context;
        this.eventList = list;
    }

    @Override
    public int getCount() {
        return eventList.size();
    }

    @Override
    public Object getItem(int position) {
        return eventList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null){
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.calendar_today_event_item, null);
            viewHolder.title = (TextView)convertView.findViewById(R.id.event_title);
            viewHolder.create_time = (TextView)convertView.findViewById(R.id.event_create_time);
            viewHolder.remark = (TextView)convertView.findViewById(R.id.event_remark);
            viewHolder.reservation = (TextView)convertView.findViewById(R.id.event_reservation_time);
            viewHolder.mType = (ImageView)convertView.findViewById(R.id.event_type);

            convertView.setTag(viewHolder);

        }else {
            viewHolder = (ViewHolder)convertView.getTag();
        }

        int type = eventList.get(position).getType();
        if(type == 1){
            viewHolder.mType.setImageResource(R.drawable.user);
            viewHolder.reservation.setText(DateUtils.timesTurnDate(eventList.get(position).getBegin()));
        }else {
            viewHolder.mType.setImageResource(R.drawable.activity);
            viewHolder.create_time.setText(DateUtils.timesTurnDate(eventList.get(position).getBegin()));
            viewHolder.reservation.setText(DateUtils.date4(eventList.get(position).getBegin())+"~"+DateUtils.date4(eventList.get(position).getEnd()));
        }

        viewHolder.title.setText(eventList.get(position).getTitle());

        viewHolder.remark.setText(eventList.get(position).getRemark());

        return convertView;
    }
    private class ViewHolder{
        TextView title, create_time, remark, reservation;
        ImageView mType;
    }
}
