package cn.trcfitness.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.Observable;
import java.util.Observer;

import cn.trcfitness.R;
import cn.trcfitness.controller.UserController;
import cn.trcfitness.model.ResultAddressParam;
import cn.trcfitness.view.adapter.ReciptAddressAdapter;

/**
 * Created by ziv on 2016/5/18.
 * 收货地址
 */
public class ManageReciptAddressActivity extends Activity implements View.OnClickListener {
    private LinearLayout Linlay_back, Linlay_manage;
    private Button bt_new;
    private ListView lv;
    private String tapy, store;//tapy为0时选择收货地址，为1时修改收货地址        store为0时订单详情跳转，为1时个人中心跳转


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activite_managereciptaddress);
        initView();
        initlv_chick();
    }

    private void initlv_chick() {
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                bundle.putSerializable("reciptAddress", resultAddressParam.getData().getReciptAddress().get(position));
                intent.putExtras(bundle);
                if (tapy.equals("0")) {
                    ManageReciptAddressActivity.this.setResult(RESULT_OK, intent);
                    ManageReciptAddressActivity.this.finish();
                }
                if (tapy.equals("1")) {
                    intent.setClass(ManageReciptAddressActivity.this, NewReciptAddressActivity.class);
                    intent.putExtra("tapy", "alter");
                    startActivity(intent);
                }
            }
        });
    }

    ResultAddressParam resultAddressParam;

    private void LoadData() {
        UserController.get_ReciptAddress(new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                if (data != null && (ResultAddressParam) data != null) {
                    resultAddressParam = (ResultAddressParam) data;
                    if (resultAddressParam.getStatus().equals("200")) {
                        ReciptAddressAdapter addressAdapter = new ReciptAddressAdapter(ManageReciptAddressActivity.this, resultAddressParam.getData().getReciptAddress());
                        lv.setAdapter(addressAdapter);
                    }
                }
            }
        }, "");

    }

    private void initView() {
        Intent intent = getIntent();
        tapy = intent.getStringExtra("tapy");
        store = intent.getStringExtra("store");
        bt_new = (Button) findViewById(R.id.bt_activity_managereciptaddress_new);//新收获地址按钮
        Linlay_back = (LinearLayout) findViewById(R.id.linlay_activity_managereciptaddress_back);//返回
        Linlay_manage = (LinearLayout) findViewById(R.id.linlay_activity_managereciptaddress_manage);//管理按钮
        lv = (ListView) findViewById(R.id.lv_activity_managereciptaddress_address);
        bt_new.setOnClickListener(this);
        Linlay_back.setOnClickListener(this);
        Linlay_manage.setOnClickListener(this);

        if (tapy.equals("0")) {
            bt_new.setVisibility(View.GONE);
        }

        if (store.equals("1")) {
            Linlay_manage.setVisibility(View.GONE);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.linlay_activity_managereciptaddress_back:
                finish();
                break;
            case R.id.bt_activity_managereciptaddress_new:
                Intent intent = new Intent(ManageReciptAddressActivity.this, NewReciptAddressActivity.class);
                intent.putExtra("tapy", "add");
                startActivity(intent);
                break;
            case R.id.linlay_activity_managereciptaddress_manage:
                tapy = "1";
                bt_new.setVisibility(View.VISIBLE);
                break;
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        LoadData();
    }

    @Override
    public void onBackPressed() {
        if (store.equals("1")) {
            ManageReciptAddressActivity.this.finish();
        } else if (tapy.equals("1")) {
            tapy = "0";
            bt_new.setVisibility(View.GONE);
        } else if (tapy.equals("0")) {
            ManageReciptAddressActivity.this.finish();
        }
    }
}
