package cn.trcfitness.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Observable;
import java.util.Observer;

import cn.trcfitness.R;
import cn.trcfitness.controller.UserController;
import cn.trcfitness.custom.LoadDialog;
import cn.trcfitness.http.model.ResultParam;
import cn.trcfitness.model.RequestUser;
import cn.trcfitness.model.RequestValidateCode;
import cn.trcfitness.model.RequestValidateVCode;
import cn.trcfitness.view.fragmet.MainFragment;

/**
 * Created by ziv on 2016/5/11.
 * 账户注册
 */
public class RegisterActivity extends Activity implements View.OnClickListener {
    private EditText phone, verification, password, password2, code;
    private Button bt_login;
    private TextView getverification, tv_login;
    private TimeCount time;
    private LoadDialog loadDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_register);
        loadDialog = new LoadDialog(RegisterActivity.this);
        initview();
    }

    private void initview() {
        phone = (EditText) findViewById(R.id.et_activity_register_phone);//手机号码输入
        verification = (EditText) findViewById(R.id.et_activity_register_verification);//验证码输入
        password = (EditText) findViewById(R.id.et_activity_register_password);//输入密码
        password2 = (EditText) findViewById(R.id.et_activity_register_password2);//确认密码
        code = (EditText) findViewById(R.id.et_activity_register_code);//输入邀请码

        bt_login = (Button) findViewById(R.id.bt_activity_register_login);//确认

        getverification = (TextView) findViewById(R.id.tv_activity_register_getverification);//获取验证码
        tv_login = (TextView) findViewById(R.id.tv_activity_register_login);//返回登录

        bt_login.setOnClickListener(this);
        getverification.setOnClickListener(this);
        tv_login.setOnClickListener(this);

        time = new TimeCount(60000, 1000);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_activity_register_login:
                Register();//注册新用户
                break;

            case R.id.tv_activity_register_getverification:
                ValidateCode();//发送验证码
                break;

            case R.id.tv_activity_register_login:
                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }

    private void mToast(String s) {
        Toast.makeText(RegisterActivity.this, s, Toast.LENGTH_SHORT).show();
    }

    String sphone, checkValicode, spassword, spassword1, scode;


    /**
     * 注册用户
     */
    private void Register() {
        sphone = phone.getText().toString().trim();
        checkValicode = verification.getText().toString().trim();
        spassword = password.getText().toString().trim();
        spassword1 = password2.getText().toString().trim();
        scode = code.getText().toString().trim();
        if (sphone.equals("")) {
            mToast("请输入手机号");
            return;
        }
        if (checkValicode.equals("")) {
            mToast("请输入验证码");
            return;
        }
        if (spassword.equals("") || spassword1.equals("")) {
            mToast("请输入密码");
            return;
        }
        loadDialog.show();
        //首先判断验证码是否正确
        RequestValidateVCode requestValidateVCode = new RequestValidateVCode();
        requestValidateVCode.setPhone(sphone);
        requestValidateVCode.setCheckValicode(checkValicode);
        UserController.v_validatecode(new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                if (data != null && (ResultParam) data != null) {
                    ResultParam resultParam = (ResultParam) data;
                    if (resultParam.getStatus().equals("200")) {
                        RegisterUser();//验证码正确，注册新用户
                    } else {
                        loadDialog.cancel();
                    }
                } else {
                    loadDialog.cancel();
                }
            }
        }, requestValidateVCode);
    }


    /**
     * 注册新用户
     */
    public void RegisterUser() {
        if (spassword.equals(spassword1)) {
            RequestUser requestUser = new RequestUser();
            requestUser.setPhone(sphone);
            requestUser.setPassword(spassword);
            requestUser.setCode(scode);
            UserController.registerUser(new Observer() {
                @Override
                public void update(Observable observable, Object data) {
                    loadDialog.cancel();
                    if (data != null && (ResultParam) data != null) {
                        ResultParam resultParam = (ResultParam) data;
                        if (resultParam.getStatus().equals("200")) {
                            mToast("注册成功");
                            MainFragment.instancemian.finish();
                            Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                            startActivity(intent);//注册成功，返回登录界面
                            finish();
                        }
                    }
                }
            }, requestUser);
        } else {
            Toast.makeText(this, "密码不一致", Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * 获取验证码
     */
    private void ValidateCode() {
        String sphone = phone.getText().toString().trim();
        if (sphone.equals("")) {
            mToast("请输入手机号");
            return;
        }
        RequestValidateCode requestValidateCode = new RequestValidateCode();
        requestValidateCode.setPhone(sphone);
        requestValidateCode.setType("add");
        loadDialog.show();
        UserController.validate_code(new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                loadDialog.cancel();
                if (data != null && (ResultParam) data != null) {
                    ResultParam resultParam = (ResultParam) data;
                    if (resultParam.getStatus().equals("200")) {
                        mToast("短信发送成功");
                        time.start();//开启倒计时
                    }
                }
            }
        }, requestValidateCode);
    }

    /**
     * 获取验证码的按钮变化
     */
    class TimeCount extends CountDownTimer {

        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            getverification.setClickable(false);
            getverification.setText(millisUntilFinished / 1000 + "s后重新发送");
        }

        @Override
        public void onFinish() {
            getverification.setText("重新获取验证码");
            getverification.setClickable(true);

        }
    }
}
