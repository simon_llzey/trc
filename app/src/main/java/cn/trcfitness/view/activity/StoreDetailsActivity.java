package cn.trcfitness.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.joooonho.SelectableRoundedImageView;
import com.lidroid.xutils.BitmapUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import cn.trcfitness.R;
import cn.trcfitness.application.OverallApplication;
import cn.trcfitness.custom.CustomListView;
import cn.trcfitness.custom.ImageCycleView;
import cn.trcfitness.custom.LineBreakLayout;
import cn.trcfitness.custom.MyDialog;
import cn.trcfitness.model.ProductsBean;
import cn.trcfitness.utils.ScreenUtils;
import cn.trcfitness.view.activity.trc.TRCDetailWebAct;
import cn.trcfitness.view.adapter.StoreDetailsPesAdapter;

/**
 * Created by ziv on 2016/5/11.
 * 商品详情
 */
public class StoreDetailsActivity extends Activity implements View.OnClickListener {
    private Button bt_newbuy, bt_popu_confirm;
    private PopupWindow popWnd;
    private ImageCycleView mImageCycleView;
    private ProductsBean productsBean;//商品详细
    private View contentView;
    private ImageView im_back;
    private TextView tv_name, tv_postage, tv_price, tv_xx, tv_popu_xx;
    private CustomListView lv_pes;
    private RelativeLayout Rellay_popu, Rellay_detail;

    private TextView tv_popu_minus, tv_popu_number, tv_popu_add;//商品个数加减
    private TextView tv_popu_price, tv_popu_stock;
    private TextView tv_no_content;
    private ImageView im_popu_back;
    private SelectableRoundedImageView im_popu;
    private LineBreakLayout Linlay_popu_pss, Linlay_popu_pcos;
    private CheckedTextView[] tabpss;//尺码
    private CheckedTextView[] tabpcos;//颜色
    private LinearLayout Linlay_postage, Linlay_popu_psspcos;

    List<ProductsBean.ProductCover> list_pcs;//商品封面
    List<ProductsBean.Size> pss;
    List<ProductsBean.ProductColor> pcos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_storedetails);
        initview();
        initdata();
    }

    /**
     * 初始化数据，获取从上个页面传过来的数据
     */
    private void initdata() {
        Bundle bundle = getIntent().getExtras();
        productsBean = (ProductsBean) bundle.getSerializable("productsBean");

        list_pcs = productsBean.getPcs();
        if (list_pcs.size() > 0) {
            loadImageCycleView();
        }

        if (productsBean.getType() == 0) {
            tv_xx.setText("选择 购买次数");
            Linlay_postage.setVisibility(View.GONE);//隐藏快递费用
        } else if (productsBean.getType() == 1) {
            tv_xx.setText("选择 颜色分类 尺码");
            tv_postage.setText(productsBean.getPostage() + "");
        }

        tv_name.setText(productsBean.getName());
        tv_price.setText("￥" + productsBean.getPrice());

        StoreDetailsPesAdapter adapter = new StoreDetailsPesAdapter(StoreDetailsActivity.this, productsBean.getPes());
        lv_pes.setAdapter(adapter);//商品评价列表
        if (productsBean.getPes() == null || productsBean.getPes().size() == 0) {
            tv_no_content.setVisibility(View.VISIBLE);
        }
    }

    /**
     * 顶部图片轮播 图片显示和加载
     */
    private void loadImageCycleView() {
        List<ImageCycleView.ImageInfo> list = new ArrayList<ImageCycleView.ImageInfo>();
        for (int i = 0; i < list_pcs.size(); i++) {
            list.add(new ImageCycleView.ImageInfo(list_pcs.get(i).getCover(), list_pcs.get(i).getId() + "", list_pcs.get(i).getPid()));  //1图片链接    2封面ID   3商品ID
        }
//        mImageCycleView.setOnPageClickListener(new ImageCycleView.OnPageClickListener() {
//            @Override
//            public void onClick(View imageView, ImageCycleView.ImageInfo imageInfo) {
//                Toast.makeText(StoreDetailsActivity.this, "你点击了" + imageInfo.value.toString(), Toast.LENGTH_SHORT).show();
//            }
//        });
        mImageCycleView.loadData(list, new ImageCycleView.LoadImageCallBack() {
            @Override
            public ImageView loadAndDisplay(ImageCycleView.ImageInfo imageInfo) {
                BitmapUtils bitmapUtils = new BitmapUtils(StoreDetailsActivity.this);
                ImageView imageView = new ImageView(StoreDetailsActivity.this);
                bitmapUtils.display(imageView, imageInfo.image.toString());
                return imageView;
            }
        });
    }

    /**
     * 初始化控件
     */
    private void initview() {
        mImageCycleView = (ImageCycleView) findViewById(R.id.imcv_activity_storedetails);//头部图片显示
        bt_newbuy = (Button) findViewById(R.id.bt_activity_storedetails_newbuy);//立即购买按钮
        tv_name = (TextView) findViewById(R.id.tv_activity_storedetails_name);//商品名称
        tv_postage = (TextView) findViewById(R.id.tv_activity_storedetails_postage);//商品邮费
        tv_price = (TextView) findViewById(R.id.tv_activity_storedetails_price);//商品单价
        tv_xx = (TextView) findViewById(R.id.tv_activity_storedetails_xx);
        lv_pes = (CustomListView) findViewById(R.id.lv_activity_storedetails_pes);
        Rellay_popu = (RelativeLayout) findViewById(R.id.rellay_activity_storedetails);//选择颜色分类尺码
        Rellay_detail = (RelativeLayout) findViewById(R.id.rellay_activity_storedetails_detail);//图文详情
        im_back = (ImageView) findViewById(R.id.im_activity_storedetails_back);
        Linlay_postage = (LinearLayout) findViewById(R.id.linlay_activity_storedetails_postage);
        tv_no_content = (TextView) findViewById(R.id.tv_activity_storedetails_no_content);

        Rellay_popu.setOnClickListener(this);
        Rellay_detail.setOnClickListener(this);
        bt_newbuy.setOnClickListener(this);
        im_back.setOnClickListener(this);
    }

    /**
     * 初始化popupwindow
     */
    private void popupwindow() {
        contentView = LayoutInflater.from(this).inflate(R.layout.popupwindow_buy, null);//popu加载的布局
        popWnd = new PopupWindow();
        popWnd.setContentView(contentView);
        popWnd.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        popWnd.setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        popWnd.setAnimationStyle(R.style.contextMenuAnim);
        //显示PopupWindow
        View rootview = LayoutInflater.from(this).inflate(R.layout.activity_storedetails, null);
        popWnd.showAtLocation(rootview, Gravity.BOTTOM, 0, 0);
        backgroundAlpha(0.6f);
        // 点击contentView以外的位置的操作，关闭popuwindow
        LinearLayout Linlay_popupwin_k = (LinearLayout) contentView.findViewById(R.id.linlay_popupwin_k);
        Linlay_popupwin_k.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popWnd.dismiss();
                backgroundAlpha(1f);
                return false;
            }
        });

        bt_popu_confirm = (Button) contentView.findViewById(R.id.bt_popupwindow_buy_confirm);//popu里的确认按钮
        tv_popu_minus = (TextView) contentView.findViewById(R.id.tv_popu_minus);//商品个数加
        tv_popu_add = (TextView) contentView.findViewById(R.id.tv_popu_add);//商品个数减
        tv_popu_number = (TextView) contentView.findViewById(R.id.tv_popu_number);//商品个数
        tv_popu_price = (TextView) contentView.findViewById(R.id.tv_popu_price);//商品单价
        tv_popu_stock = (TextView) contentView.findViewById(R.id.tv_popu_stock);//商品库存
        tv_popu_xx = (TextView) contentView.findViewById(R.id.tv_popu_xx);//商品库存
        im_popu = (SelectableRoundedImageView) contentView.findViewById(R.id.im_popupwindow);//popu左上角的图片
        im_popu_back = (ImageView) contentView.findViewById(R.id.im_popupwindow_back);//popu关闭按钮
        Linlay_popu_pss = (LineBreakLayout) contentView.findViewById(R.id.linlay_popu_pss);//选择尺码
        Linlay_popu_pcos = (LineBreakLayout) contentView.findViewById(R.id.linlay_popu_pcos);//选择尺码
        Linlay_popu_psspcos = (LinearLayout) contentView.findViewById(R.id.linlay_popu_psspcos);
        bt_popu_confirm.setOnClickListener(this);
        tv_popu_minus.setOnClickListener(this);
        tv_popu_add.setOnClickListener(this);
        im_popu_back.setOnClickListener(this);

        initpopudata();
    }

    /**
     * 初始化PopuWindow的数据
     */
    private void initpopudata() {
        Picasso.with(StoreDetailsActivity.this).load(list_pcs.get(0).getCover()).into(im_popu);
        tv_popu_price.setText(productsBean.getPrice() + "");
        tv_popu_stock.setText(productsBean.getStock() + "件");

        tv_popu_xx.setText("选择 颜色分类");
        if (productsBean.getPss().size() > 0) {
            pss = productsBean.getPss();//尺码
            size = pss.get(0);
        }
        if (productsBean.getPcos().size() > 0) {
            pcos = productsBean.getPcos();//颜色
            productColor = pcos.get(0);
        }

        int width = ScreenUtils.getScreenWidth(StoreDetailsActivity.this);

        if (pss != null) {
            //循环添加选择尺码控件
            for (int i = 0; i < pss.size(); i++) {
                CheckedTextView ctv = new CheckedTextView(StoreDetailsActivity.this);
                ctv.setText(pss.get(i).getSize());
                ctv.setTextSize(15);
                ctv.setGravity(Gravity.CENTER);
                ctv.setBackgroundResource(R.drawable.selector_popuptext_check);
                ctv.setTextColor(Color.parseColor("#5f606d"));
                Linlay_popu_pss.addView(ctv);
            }

            //为每个控件设置单击事件
            ViewGroup viewGroup = (ViewGroup) contentView.findViewById(R.id.linlay_popu_pss);//获取布局
            tabpss = new CheckedTextView[viewGroup.getChildCount()];// getChildCount获取容器个数
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                tabpss[i] = (CheckedTextView) viewGroup.getChildAt(i);// getChildAt获取布局
                tabpss[i].setOnClickListener(onClickListenerpss);
            }
            tabpss[0].setChecked(true);
            tabpss[0].setTextColor(Color.parseColor("#ffffff"));
        } else {
            contentView.findViewById(R.id.Linlay_popupwin_size).setVisibility(View.GONE);
        }

        if (pcos != null) {
            //循环添加选择颜色控件
            for (int i = 0; i < pcos.size(); i++) {
                CheckedTextView ctv = new CheckedTextView(StoreDetailsActivity.this);
                ctv.setText(pcos.get(i).getColor());
                ctv.setTextSize(15);
                ctv.setGravity(Gravity.CENTER);
                ctv.setBackgroundResource(R.drawable.selector_popuptext_check);
                ctv.setTextColor(Color.parseColor("#5f606d"));
                Linlay_popu_pcos.addView(ctv);
            }

            //为每个控件设置单击事件
            ViewGroup viewGroup1 = (ViewGroup) contentView.findViewById(R.id.linlay_popu_pcos);//获取布局
            tabpcos = new CheckedTextView[viewGroup1.getChildCount()];// getChildCount获取容器个数
            for (int i = 0; i < viewGroup1.getChildCount(); i++) {
                tabpcos[i] = (CheckedTextView) viewGroup1.getChildAt(i);// getChildAt获取布局
                tabpcos[i].setOnClickListener(onClickListenerpcos);
            }
            tabpcos[0].setChecked(true);
            tabpcos[0].setTextColor(Color.parseColor("#ffffff"));
        } else {
            contentView.findViewById(R.id.Linlay_popupwin_color).setVisibility(View.GONE);
        }
    }

    private int currentTabIndexpss = 0;//记录当前显示的是哪个控件
    private ProductsBean.Size size;//记录当前选择的尺寸
    View.OnClickListener onClickListenerpss = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub
            if (v == tabpss[currentTabIndexpss]) {// 如果就是当前选中的这个条目，不做处理
                return;
            }
            for (int i = 0; i < tabpss.length; i++) {// 遍历查找点击的哪个条目
                if (v == tabpss[i]) {
                    tabpss[i].setChecked(true);
                    tabpss[currentTabIndexpss].setChecked(false);
                    tabpss[i].setTextColor(Color.parseColor("#ffffff"));
                    tabpss[currentTabIndexpss].setTextColor(Color.parseColor("#5f606d"));
                    size = pss.get(i);//记录选择的是哪一个对象，传递到确认订单
                    currentTabIndexpss = i;
                    break;
                }
            }
        }
    };

    private int currentTabIndexpcos = 0;
    private ProductsBean.ProductColor productColor;//记录当前选择的颜色
    View.OnClickListener onClickListenerpcos = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub
            if (v == tabpcos[currentTabIndexpcos]) {// 如果就是当前选中的这个条目，不做处理
                return;
            }
            for (int i = 0; i < tabpcos.length; i++) {// 遍历查找点击的哪个条目
                if (v == tabpcos[i]) {
                    tabpcos[i].setChecked(true);
                    tabpcos[currentTabIndexpcos].setChecked(false);
                    tabpcos[i].setTextColor(Color.parseColor("#ffffff"));
                    tabpcos[currentTabIndexpcos].setTextColor(Color.parseColor("#5f606d"));
                    productColor = pcos.get(i);
                    currentTabIndexpcos = i;
                    break;
                }
            }
        }
    };

    /**
     * 设置添加屏幕的背景透明度
     */
    public void backgroundAlpha(float bgAlpha) {
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.alpha = bgAlpha; //0.0-1.0
        getWindow().setAttributes(lp);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(StoreDetailsActivity.this, OrderActivity.class);
        Bundle bundle = new Bundle();

        switch (v.getId()) {
            case R.id.bt_activity_storedetails_newbuy://商品详情的确定
                if (OverallApplication.tourists) {
                    MyDialog.Dialog(this);
                    return;
                }
                if (productsBean.getType() == 0) {
                    bundle.putSerializable("products", productsBean);//传递商品对象
                    bundle.putString("number", "1");//传递商品个数
                    intent.putExtras(bundle);
                    startActivity(intent);
                } else {
                    popupwindow();
                }
                break;
            case R.id.bt_popupwindow_buy_confirm://popup的确定
                if (OverallApplication.tourists) {
                    MyDialog.Dialog(this);
                    return;
                }

                bundle.putSerializable("products", productsBean);//传递商品对象
                if (productsBean.getType() == 1) {
                    bundle.putSerializable("size", size);//传递商品尺寸
                    bundle.putSerializable("productColor", productColor);//传递商品颜色
                }
                bundle.putString("number", tv_popu_number.getText().toString());//传递商品个数
                intent.putExtras(bundle);
                startActivity(intent);
                break;
            case R.id.tv_popu_minus://减商品个数
                if (Integer.parseInt(tv_popu_number.getText() + "") > 1) {
                    tv_popu_number.setText((Integer.parseInt(tv_popu_number.getText() + "") - 1) + "");
                }
                break;
            case R.id.tv_popu_add://加商品个数
                if (productsBean.getStock().intValue() > (Integer.parseInt(tv_popu_number.getText() + ""))) {
                    tv_popu_number.setText((Integer.parseInt(tv_popu_number.getText() + "") + 1) + "");
                }
                break;
            case R.id.rellay_activity_storedetails://选择颜色分类尺码
                if (productsBean.getType() == 0) {
                    if (OverallApplication.tourists) {
                        MyDialog.Dialog(this);
                        return;
                    }
                    bundle.putSerializable("products", productsBean);//传递商品对象
                    bundle.putString("number", "1");//传递商品个数
                    intent.putExtras(bundle);
                    startActivity(intent);
                } else {
                    popupwindow();
                }
                break;
            case R.id.im_activity_storedetails_back://返回
                finish();
                break;
            case R.id.im_popupwindow_back:
                popWnd.dismiss();
                backgroundAlpha(1f);
                break;

            case R.id.rellay_activity_storedetails_detail:
                Intent intentweb = new Intent(StoreDetailsActivity.this, TRCDetailWebAct.class);
                bundle.putSerializable("products", productsBean);//传递商品对象
                bundle.putString("state", "store");
                intentweb.putExtras(bundle);
                startActivity(intentweb);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (null != popWnd && popWnd.isShowing()) {
            popWnd.dismiss();
            backgroundAlpha(1f);
        } else {
            finish();
        }
    }
}
