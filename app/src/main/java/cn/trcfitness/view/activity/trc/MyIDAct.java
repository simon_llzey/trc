package cn.trcfitness.view.activity.trc;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import java.util.Hashtable;
import java.util.Observable;
import java.util.Observer;

import cn.trcfitness.R;
import cn.trcfitness.controller.UserController;
import cn.trcfitness.model.UserResultParam;

/**
 * 我的id-二维码
 */
public class MyIDAct extends Activity {
    private Context mContext = this;
    private ImageView idImage, tiaomaImage;
    private int QR_WIDTH = 0;
    private int QR_HEIGHT = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_my_id);

        idImage = (ImageView) findViewById(R.id.myid_image);
        tiaomaImage = (ImageView)findViewById(R.id.myid_image_tiaoma);
        getIdErweima();


        WindowManager wm = (WindowManager)getSystemService(Context.WINDOW_SERVICE);

        QR_WIDTH = wm.getDefaultDisplay().getWidth();
        QR_HEIGHT = wm.getDefaultDisplay().getHeight() / 2;

        findViewById(R.id.myid_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyIDAct.this.finish();
            }
        });
    }

    /**
     * 获取ID二维码
     */
    private void getIdErweima() {
        UserController.get_myid(new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                UserResultParam resultParam = (UserResultParam) data;
                if (resultParam != null) {
                    Log.i("Myid", "获取二维码图" + resultParam.toString());
                    if (resultParam.isStatus()) {
                        if (resultParam.getData() != null && resultParam.getData().getPath() != null) {
                            Bitmap bitmap = createImage(resultParam.getData().getPath());
                            Bitmap tiaoma = createImage_TiaoMa(resultParam.getData().getPath(), QR_WIDTH-80, QR_HEIGHT / 4 );
                            idImage.setImageBitmap(bitmap);
                            tiaomaImage.setImageBitmap(tiaoma);
                        }
                    } else {
                        Toast.makeText(mContext, "加载失败", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(mContext, "加载失败", Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    private Bitmap bitMatrix2Bitmap(BitMatrix matrix) {
        int w = matrix.getWidth();
        int h = matrix.getHeight();
        int[] rawData = new int[w * h];
        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                int color = 0xFFEEEEEE;
                if (matrix.get(i, j)) {
                    color = Color.BLACK;
                }
                rawData[i + (j * w)] = color;
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.RGB_565);
        bitmap.setPixels(rawData, 0, w, 0, 0, w, h);
        return bitmap;
    }
    /**
     * 生成二维码
     * @param text
     * @return
     */
    private Bitmap createImage(String text) {


        try {
            QRCodeWriter writer = new QRCodeWriter();
            // MultiFormatWriter writer = new MultiFormatWriter();
            BitMatrix matrix = writer.encode(text, BarcodeFormat.QR_CODE, QR_WIDTH, QR_HEIGHT);
            return bitMatrix2Bitmap(matrix);
        } catch (WriterException e) {
            e.printStackTrace();
        }
        return null;

        /*try {

            if (TextUtils.isEmpty(text)) {
                return null;
            }
            QRCodeWriter writer = new QRCodeWriter();
            BitMatrix martix = writer.encode(text, BarcodeFormat.QR_CODE,
                    QR_WIDTH, QR_HEIGHT);

            Hashtable<EncodeHintType, String> hints = new Hashtable<EncodeHintType, String>();
            hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
            BitMatrix bitMatrix = new QRCodeWriter().encode(text,
                    BarcodeFormat.QR_CODE, QR_WIDTH, QR_HEIGHT, hints);
            int[] pixels = new int[QR_WIDTH * QR_HEIGHT];
            for (int y = 0; y < QR_HEIGHT; y++) {
                for (int x = 0; x < QR_WIDTH; x++) {
                    if (bitMatrix.get(x, y)) {
                        pixels[y * QR_WIDTH + x] = 0xff000000;
                    } else {
                        pixels[y * QR_WIDTH + x] = 0xffffffff;
                    }
                }
            }

            Bitmap bitmap = Bitmap.createBitmap(QR_WIDTH, QR_HEIGHT,
                    Bitmap.Config.RGB_565);

            bitmap.setPixels(pixels, 0, QR_WIDTH, 0, 0, QR_WIDTH, QR_HEIGHT);
            return bitmap;
        } catch (WriterException e) {
            e.printStackTrace();
            return null;
        }*/
    }


    /**
     * 生成条码
     * @param contents
     * @param desiredWidth
     * @param desiredHeight
     * @return
     */
    private Bitmap createImage_TiaoMa(String contents,
                                                  int desiredWidth, int desiredHeight) {
       /**
         * 条形码的编码类型
         */
        BarcodeFormat barcodeFormat = BarcodeFormat.CODE_128;


        Bitmap ruseltBitmap = encodeAsBitmap(contents, barcodeFormat,
                    desiredWidth, desiredHeight);


        return ruseltBitmap;
    }



    /**
     * 生成条形码的Bitmap
     *
     * @param contents
     *            需要生成的内容
     * @param format
     *            编码格式
     * @param desiredWidth
     * @param desiredHeight
     * @return
     * @throws WriterException
     */
    protected static Bitmap encodeAsBitmap(String contents,
                                           BarcodeFormat format, int desiredWidth, int desiredHeight) {
        final int WHITE = 0xFFFFFFFF;
        final int BLACK = 0xFF000000;

        MultiFormatWriter writer = new MultiFormatWriter();
        BitMatrix result = null;
        Hashtable<EncodeHintType, String> hints = new Hashtable<EncodeHintType, String>();
        hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
        try {
            result = writer.encode(contents, format, desiredWidth,
                    desiredHeight, hints);
        } catch (WriterException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        int width = result.getWidth();
        int height = result.getHeight();
        int[] pixels = new int[width * height];
        // All are 0, or black, by default
        for (int y = 0; y < height; y++) {
            int offset = y * width;
            for (int x = 0; x < width; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(width, height,
                Bitmap.Config.RGB_565);
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }
}
