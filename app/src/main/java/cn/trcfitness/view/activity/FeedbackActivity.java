package cn.trcfitness.view.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.Observable;
import java.util.Observer;

import cn.trcfitness.R;
import cn.trcfitness.controller.UserController;
import cn.trcfitness.http.model.ResultParam;
import cn.trcfitness.model.RequestFeedback;

/**
 * Created by ziv on 2016/6/8.
 */
public class FeedbackActivity extends Activity implements View.OnClickListener {
    private EditText et_email, et_feedback;
    private LinearLayout linlay_back, linlay_send;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_feedback);
        initView();
    }

    private void initView() {
        et_email = (EditText) findViewById(R.id.et_activite_feedback_email);
        et_feedback = (EditText) findViewById(R.id.et_activite_feedback_feedback);
        linlay_back = (LinearLayout) findViewById(R.id.linlay_activity_feedback_back);
        linlay_send = (LinearLayout) findViewById(R.id.linlay_activity_feedback_send);
        linlay_back.setOnClickListener(this);
        linlay_send.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.linlay_activity_feedback_back:
                finish();
                break;
            case R.id.linlay_activity_feedback_send:
                String s = et_feedback.getText().toString().trim();
                String s1 = et_email.getText().toString().trim();
                if (s == null || s.length() <= 0) {
                    Toast.makeText(FeedbackActivity.this, "请填写反馈意见", Toast.LENGTH_SHORT).show();
                } else if (s1 == null || s.length() <= 0) {
                    Toast.makeText(FeedbackActivity.this, "请填写您的邮箱", Toast.LENGTH_SHORT).show();
                } else {
                    send();
                }
                break;
        }
    }

    private void send() {
        final RequestFeedback requestFeedback = new RequestFeedback();
        requestFeedback.setTitle("Android端用户反馈");
        requestFeedback.setContent(et_feedback.getText().toString().trim());
        requestFeedback.setLink(et_email.getText().toString().trim());
        UserController.save_feedback(new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                if (data != null && (ResultParam) data != null) {
                    ResultParam resultParam = (ResultParam) data;
                    if (resultParam.getStatus().equals("200")) {
                        Toast.makeText(FeedbackActivity.this, "感谢您的反馈！", Toast.LENGTH_SHORT).show();
                        FeedbackActivity.this.finish();
                    }
                }
            }
        }, requestFeedback);
    }
}
