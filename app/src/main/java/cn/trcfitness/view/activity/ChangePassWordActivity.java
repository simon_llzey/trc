package cn.trcfitness.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Observable;
import java.util.Observer;

import cn.trcfitness.R;
import cn.trcfitness.controller.UserController;
import cn.trcfitness.custom.LoadDialog;
import cn.trcfitness.http.model.ResultParam;
import cn.trcfitness.model.RequestChangepwdParam;
import cn.trcfitness.model.RequestValidateCode;

/**
 * Created by ziv on 2016/5/11.
 * 修改密码
 */
public class ChangePassWordActivity extends Activity implements View.OnClickListener {
    private EditText phone, validateCode, password, password1;
    private TextView tv_login, tv_getverification;
    private Button bt_lobin;
    private TimeCount time;
    private LoadDialog loadDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_cahngepassword);
        loadDialog = new LoadDialog(this);
        initview();
    }

    private void initview() {
        phone = (EditText) findViewById(R.id.et_activity_changepassword_phone);//输入手机号
        validateCode = (EditText) findViewById(R.id.et_activity_changepassword_verification);//输入验证码
        password = (EditText) findViewById(R.id.et_activity_changepassword_password);//输入密码
        password1 = (EditText) findViewById(R.id.et_activity_changepassword_password2);//确认密码

        tv_login = (TextView) findViewById(R.id.tv_activity_changepassword_login);//返回登录
        tv_getverification = (TextView) findViewById(R.id.tv_activity_changepassword_getverification);//获取验证码
        bt_lobin = (Button) findViewById(R.id.bt_activity_changepassword_login);//确定
        tv_login.setOnClickListener(this);
        tv_getverification.setOnClickListener(this);
        bt_lobin.setOnClickListener(this);

        time = new TimeCount(60000, 1000);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_activity_changepassword_login:
                Intent intent = new Intent(ChangePassWordActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.tv_activity_changepassword_getverification:
                ValidateCode();
                break;
            case R.id.bt_activity_changepassword_login:
                Change_pwd();
                break;
        }

    }


    /**
     * 确认更改密码
     */
    private void Change_pwd() {
        String sphone = phone.getText().toString().trim();
        String svalidateCode = validateCode.getText().toString().trim();
        String spassword = password.getText().toString().trim();
        String spassword1 = password1.getText().toString().trim();
        if (sphone.equals("")) {
            mToast("请输入手机号");
            return;
        }
        if (svalidateCode.equals("")) {
            mToast("请输入验证码");
            return;
        }
        if (spassword.equals("") || spassword1.equals("")) {
            mToast("请输入密码");
            return;
        }

        if (spassword.equals(spassword1)) {
            RequestChangepwdParam requestChangepwdParam = new RequestChangepwdParam();
            requestChangepwdParam.setPhone(sphone);
            requestChangepwdParam.setPassword(spassword);
            requestChangepwdParam.setValidateCode(svalidateCode);
            loadDialog.show();
            UserController.change_pwd(new Observer() {
                @Override
                public void update(Observable observable, Object data) {
                    loadDialog.cancel();
                    if (data != null && (ResultParam) data != null) {
                        ResultParam resultParam = (ResultParam) data;
                        if (resultParam.getStatus().equals("200")) {
                            mToast("密码修改成功");
                            Intent intent = new Intent(ChangePassWordActivity.this, LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    }
                }
            }, requestChangepwdParam);


        } else {
            Toast.makeText(this, "密码不一致", Toast.LENGTH_SHORT).show();
        }


    }

    /**
     * 获取验证码
     */
    private void ValidateCode() {
        String sphone = phone.getText().toString().trim();
        if (sphone.equals("")) {
            mToast("请输入手机号");
            return;
        }
        loadDialog.show();
        RequestValidateCode requestValidateCode = new RequestValidateCode();
        requestValidateCode.setPhone(sphone);
        requestValidateCode.setType("mod");
        UserController.validate_code(new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                loadDialog.cancel();
                if (data != null && (ResultParam) data != null) {
                    ResultParam resultParam = (ResultParam) data;
                    if (resultParam.getStatus().equals("200")) {
                        mToast("短信发送成功");
                        time.start();//开启倒计时
                    }
                }
            }
        }, requestValidateCode);
    }

    private void mToast(String s) {
        Toast.makeText(ChangePassWordActivity.this, s, Toast.LENGTH_SHORT).show();
    }

    /**
     * 获取验证码的按钮变化
     */
    class TimeCount extends CountDownTimer {

        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            tv_getverification.setClickable(false);
            tv_getverification.setText(millisUntilFinished / 1000 + "s后重新发送");
        }

        @Override
        public void onFinish() {
            tv_getverification.setText("重新获取验证码");
            tv_getverification.setClickable(true);

        }
    }
}
