package cn.trcfitness.view.activity.trc;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import cn.trcfitness.R;
import cn.trcfitness.application.OverallApplication;
import cn.trcfitness.controller.UserController;
import cn.trcfitness.custom.CustomGridView;
import cn.trcfitness.http.model.ResultParam;
import cn.trcfitness.model.UserData;
import cn.trcfitness.model.UserDetail;
import cn.trcfitness.model.UserRequestParam;
import cn.trcfitness.view.pay.PayKeyboardAct;

/**
 * 充值输入密码界面
 */
public class RechargeInputCodeAct extends Activity {

    private Context mContext = this;

    private TextView box1, box2, box3, box4, box5, box6;

    private TextView payTitle;
    private CustomGridView mGridView;

    private List<String> mList = new ArrayList<String>();

    //用户信息
    private UserRequestParam param;

    private int[] images = {R.drawable.keyboard_one, R.drawable.keyboard_two, R.drawable.keyboard_three,
            R.drawable.keyboard_four, R.drawable.keyboard_five, R.drawable.keyboard_sex, R.drawable.keyboard_seven,
            R.drawable.keyboard_eight, R.drawable.keyboard_nine, R.drawable.keyboard_space,
            R.drawable.keyboard_zero, R.drawable.keyboard_del};

    private int[] codes = {1,2,3,4,5,6,7,8,9,0};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_recharge_input_code);
        initView();
    }


    private void initView() {

        param = new UserRequestParam();


        mGridView = (CustomGridView) findViewById(R.id.recharge_grid);
        payTitle = (TextView)findViewById(R.id.recharge_title);

        box1 = (TextView) findViewById(R.id.recharge_box1);
        box2 = (TextView) findViewById(R.id.recharge_box2);
        box3 = (TextView) findViewById(R.id.recharge_box3);
        box4 = (TextView) findViewById(R.id.recharge_box4);
        box5 = (TextView) findViewById(R.id.recharge_box5);
        box6 = (TextView) findViewById(R.id.recharge_box6);

        mGridView.setAdapter(new RechargeAdapter());

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //如果密码有6位时清空输入

                if (mList.size() < 6) {
                    if (position == 9) {
                        return;
                    }
                    if (position == 10) {
                        mList.add(codes[position - 1] + "");
                        updateInputCode();
                        return;
                    }
                    if (position == 11) {

                        if (mList.size() <= 0) {
                            Log.i("Pay", "获取删除的值小鱼0==" + mList.size());
                            return;
                        }
                        Log.i("Pay", "获取删除的值" + (mList.size() - 1));
                        mList.remove(mList.size() - 1);
                        updateInputCode();
                        return;
                    }


                    if (mList.size() == 5) {
                        mList.add(codes[position] + "");
                        String userCode = "";
                        for (String code : mList) {
                            userCode += code;
                        }

                        param.setPwd(userCode);
                        checkPayPasswd(param);
                        mList.clear();
                        updateInputCode();

                        return;
                    }


                    mList.add(codes[position] + "");
                    updateInputCode();
                } else {
                    if (position == 11) {

                        if (mList.size() <= 0) {
                            Log.i("Pay", "获取删除的值小鱼0==" + mList.size());
                            return;
                        }
                        Log.i("Pay", "获取删除的值" + (mList.size() - 1));
                        mList.remove(mList.size() - 1);
                        updateInputCode();
                        return;
                    }
                }


            }
        });




    }

    /**
     * 校验支付密码
     * @param requestParam
     */
    private void checkPayPasswd(UserRequestParam requestParam){
        UserController.set_check_paypasswd(requestParam, new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                ResultParam resultParam = (ResultParam) data;
                if (resultParam != null) {
                    if (resultParam.isStatus()) {
                        Toast.makeText(mContext, "密码有效跳转", Toast.LENGTH_LONG).show();
                        Intent selectPayIntent = new Intent(mContext, SelectPayAct.class);
                        startActivity(selectPayIntent);
                    } else {
                        Toast.makeText(mContext, "密码无效，请重新输入", Toast.LENGTH_LONG).show();
                    }
                }else {
                    Toast.makeText(mContext, "密码无效，请重新输入", Toast.LENGTH_LONG).show();
                }
            }
        });
    }


    /**
     * 密码键盘
     */
    private class RechargeAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return images.length;
        }

        @Override
        public Object getItem(int position) {
            return images[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_pay_image, null);
            ImageView imageView = (ImageView) convertView.findViewById(R.id.pay_image);

            imageView.setImageResource(images[position]);

            return convertView;
        }
    }



    private void updateInputCode() {

        if (mList.size() == 0) {
            box1.setText("");
            box2.setText("");
            box3.setText("");
            box4.setText("");
            box5.setText("");
            box6.setText("");
        } else if (mList.size() == 1) {
            box1.setText(mList.get(0));
            box2.setText("");
            box3.setText("");
            box4.setText("");
            box5.setText("");
            box6.setText("");
        } else if (mList.size() == 2) {
            box1.setText(mList.get(0));
            box2.setText(mList.get(1));
            box3.setText("");
            box4.setText("");
            box5.setText("");
            box6.setText("");
        } else if (mList.size() == 3) {
            box1.setText(mList.get(0));
            box2.setText(mList.get(1));
            box3.setText(mList.get(2));
            box4.setText("");
            box5.setText("");
            box6.setText("");
        } else if (mList.size() == 4) {
            box1.setText(mList.get(0));
            box2.setText(mList.get(1));
            box3.setText(mList.get(2));
            box4.setText(mList.get(3));
            box5.setText("");
            box6.setText("");
        } else if (mList.size() == 5) {
            box1.setText(mList.get(0));
            box2.setText(mList.get(1));
            box3.setText(mList.get(2));
            box4.setText(mList.get(3));
            box5.setText(mList.get(4));
            box6.setText("");
        } else if (mList.size() == 6) {
            box1.setText(mList.get(0));
            box2.setText(mList.get(1));
            box3.setText(mList.get(2));
            box4.setText(mList.get(3));
            box5.setText(mList.get(4));
            box6.setText(mList.get(5));
        }


    }

}
