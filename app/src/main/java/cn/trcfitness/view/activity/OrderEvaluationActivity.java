package cn.trcfitness.view.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.Observable;
import java.util.Observer;

import cn.trcfitness.R;
import cn.trcfitness.controller.UserController;
import cn.trcfitness.http.model.ResultParam;
import cn.trcfitness.model.OrdersBean;
import cn.trcfitness.model.UserRequestParam;

/**
 * Created by ziv on 2016/5/17.
 * 发表评价
 */
public class OrderEvaluationActivity extends Activity implements View.OnClickListener {
    private OrdersBean ordersBean;
    private LinearLayout linlay_back;
    private ViewGroup linlay_storedetailspec;
    private ImageView im_myorder;
    private TextView tv_name, tv_money, tv_number, tv_money2, tv_postage;
    private EditText et_opinion;
    private Button bt_newbuy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_order_evaluation);
        initView();
        initData();
    }

    private void initView() {
        linlay_back = (LinearLayout) findViewById(R.id.linlay_activity_order_evaluation_back);//返回
        im_myorder = (ImageView) findViewById(R.id.im_activite_order_ecaluation_myorder);//商品封面
        tv_name = (TextView) findViewById(R.id.tv_activite_order_ecaluation_myorder_name);//商品名称
        tv_money = (TextView) findViewById(R.id.tv_activite_order_ecaluation_myorder_money);//单价
        tv_money2 = (TextView) findViewById(R.id.tv_activite_order_ecaluation_myorder_money2);//总价
        tv_number = (TextView) findViewById(R.id.tv_activite_order_ecaluation_myorder_number);//数量
        tv_postage = (TextView) findViewById(R.id.tv_activite_order_ecaluation_myorder_postage);//邮费
        et_opinion = (EditText) findViewById(R.id.et_activite_order_ecaluation_input_opinion);//用户评论
        linlay_storedetailspec = (ViewGroup) findViewById(R.id.linlay_activite_order_ecaluation);//星级
        bt_newbuy = (Button) findViewById(R.id.bt_activite_order_ecaluation_newbuy);

        linlay_back.setOnClickListener(this);
        bt_newbuy.setOnClickListener(this);
        initstar();
    }

    private void initData() {
        Bundle bundle = getIntent().getExtras();
        ordersBean = (OrdersBean) bundle.get("OrdersBean");
        if (ordersBean.getProduct().getPcs() != null && ordersBean.getProduct().getPcs().size() > 0) {
            Picasso.with(this).load(ordersBean.getProduct().getPcs().get(0).getCover()).into(im_myorder);
        }
        tv_name.setText(ordersBean.getProduct_name());
        tv_money.setText("¥" + ordersBean.getPrice() + "");
        tv_number.setText("X" + ordersBean.getNumber() + "");
        Float Postage = 0f;
        if (ordersBean.getPostage() != null) {
            Postage = ordersBean.getPostage();
            tv_postage.setText(Postage + "");
        } else {
            tv_postage.setText("0.00");
        }
        Float money = (ordersBean.getNumber().intValue() * ordersBean.getPrice() + Postage);
        tv_money2.setText(money + "");

    }

    private void initstar() {
        for (int i = 0; i < linlay_storedetailspec.getChildCount(); i++) {
            (linlay_storedetailspec.getChildAt(i)).setOnClickListener(mClickListene);
        }
        currentTabIndex = 0;
    }

    private int currentTabIndex;//当前选择 星级=currentTabIndex+1
    View.OnClickListener mClickListene = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub
            Boolean b = false;
            if (v == linlay_storedetailspec.getChildAt(currentTabIndex)) {
                return;
            }
            for (int i = 0; i < linlay_storedetailspec.getChildCount(); i++) {
                ((ImageView) linlay_storedetailspec.getChildAt(i)).setImageResource(R.drawable.star);//将所有的设为选中+
                if (b) {//以后的全改为未选中
                    ((ImageView) linlay_storedetailspec.getChildAt(i)).setImageResource(R.drawable.grv_activity_storepersonal5);
                }
                if (v == linlay_storedetailspec.getChildAt(i)) {//当前选择
                    currentTabIndex = i;
                    b = true;
                }
            }
        }
    };

    private void sendevaluation() {
        UserRequestParam userRequestParam = new UserRequestParam();
        userRequestParam.setOid(ordersBean.getId());
        userRequestParam.setPid(ordersBean.getProduct().getId());
        userRequestParam.setScore(currentTabIndex + 1);
        userRequestParam.setEvaluate(et_opinion.getText().toString());
        UserController.evaluate(new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                if (data != null && (ResultParam) data != null) {
                    ResultParam resultParam = (ResultParam) data;
                    if (resultParam.getStatus().equals("200")) {
                        Toast.makeText(OrderEvaluationActivity.this, "评价成功", Toast.LENGTH_SHORT).show();
                        OrderEvaluationActivity.this.finish();
                    }
                }
            }
        }, userRequestParam);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_activite_order_ecaluation_newbuy:
                sendevaluation();
                break;
            case R.id.linlay_activity_order_evaluation_back:
                finish();
                break;

        }

    }
}
