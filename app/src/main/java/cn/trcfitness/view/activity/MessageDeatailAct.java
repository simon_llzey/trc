package cn.trcfitness.view.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.joooonho.SelectableRoundedImageView;
import com.squareup.picasso.Picasso;

import cn.trcfitness.R;
import cn.trcfitness.application.OverallApplication;
import cn.trcfitness.model.ResultMessageParam;
import cn.trcfitness.utils.DateUtils;

/**
 * 消息—详细资料
 */
public class MessageDeatailAct extends Activity {
    private SelectableRoundedImageView iv;
    private TextView tv_title;
    private TextView tv_time;
    private TextView tv_content;
    private TextView tv_bt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_message_deatail);
        initView();
        initData();
    }

    private void initView() {
        iv = (SelectableRoundedImageView) findViewById(R.id.iv_activity_message_deatail);
        tv_title = (TextView) findViewById(R.id.tv_activity_message_deatail_titel);
        tv_time = (TextView) findViewById(R.id.tv_activity_message_deatail_time);
        tv_content = (TextView) findViewById(R.id.tv_activity_message_deatail_content);
        tv_bt = (TextView) findViewById(R.id.tv_activity_message_deatail_bt);
    }

    private void initData() {
        ResultMessageParam.DataBean.MessageBean messageBean = (ResultMessageParam.DataBean.MessageBean) getIntent().getExtras().getSerializable("MessageBean");
        Picasso.with(this).load(messageBean.getImg()).into(iv);
        tv_title.setText(messageBean.getTitle());
        tv_time.setText(DateUtils.timesTurnDate(messageBean.getCreate_time()));
        tv_content.setText(messageBean.getContent());

        if (messageBean.getType() == 0) {
//            holder.tv_bt.setText("文本消息");
//            holder.tv_bt.setBackgroundResource(R.color.title_color);
            tv_bt.setEnabled(false);
            tv_bt.setVisibility(View.GONE);
        } else if (messageBean.getType() == 1) {
            tv_bt.setVisibility(View.VISIBLE);
            switch (messageBean.getRecevie()) {
                case 0:
                    tv_bt.setText("接受");
                    break;
                case 1:
                    tv_bt.setText("已接受");
                    tv_bt.setEnabled(false);
                    tv_bt.setBackgroundResource(R.color.white);
                    tv_bt.setTextColor(Color.parseColor("#7f7f7f"));
                    break;
                case 2:
                    tv_bt.setText("已拒绝");
                    tv_bt.setBackgroundResource(R.color.white);
                    tv_bt.setTextColor(Color.parseColor("#7f7f7f"));
                    tv_bt.setEnabled(false);
                    break;
            }
        } else {
            if (OverallApplication.userData.getData().getUser().getUser_id() == messageBean.getSid()) {
                tv_bt.setVisibility(View.GONE);
            } else {
                tv_bt.setText("接受");
                tv_bt.setVisibility(View.VISIBLE);
            }
        }

        tv_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("测试", "--------------------------------");
            }
        });
    }

}
