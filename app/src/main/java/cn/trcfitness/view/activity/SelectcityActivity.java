package cn.trcfitness.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.Observable;
import java.util.Observer;

import cn.trcfitness.R;
import cn.trcfitness.controller.UserController;
import cn.trcfitness.model.ResultCity_CidParam;
import cn.trcfitness.model.ResultCity_CityParam;
import cn.trcfitness.model.ResultCity_ProvincesParam;
import cn.trcfitness.view.adapter.City_Cid_Adapter;
import cn.trcfitness.view.adapter.City_City_Adapter;
import cn.trcfitness.view.adapter.City_Provinces_Adapter;

/**
 * Created by ziv on 2016/5/19.
 * 地址  选择城市
 */
public class SelectcityActivity extends Activity {
    private ListView lv;
    LinearLayout Linlay_back;
    String Provinces, city, cid;
    String tapy;
    String city_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_selectcity);
        initView();

        LoadData();
        listviewclick();
    }

    private void listviewclick() {
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (tapy.equals("Provinces")) {
                    Provinces = resultCity_provincesParam.getData().get(position).getName();
                    city_id= resultCity_provincesParam.getData().get(position).getId();
                    loadcity(city_id);
                }

                if (tapy.equals("city")) {
                    city = resultCityCity_param.getData().get(position).getName();
                    loadcid(resultCityCity_param.getData().get(position).getId());
                }

                if (tapy.equals("cid")) {
                    cid = cid_cidparpm.getData().get(position).getName();
                    Intent intent = new Intent();
                    intent.putExtra("city", Provinces + " " + city + " " + cid);
                    SelectcityActivity.this.setResult(RESULT_OK,intent);
                    SelectcityActivity.this.finish();
                }

            }
        });
    }

    ResultCity_CidParam cid_cidparpm;//区

    private void loadcid(String id) {
        UserController.get_Cid(new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                if (data != null && (ResultCity_CidParam) data != null) {
                    cid_cidparpm = (ResultCity_CidParam) data;
                    if (cid_cidparpm.getStatus().equals("200")) {
                        tapy = "cid";
                        City_Cid_Adapter cityAdapter = new City_Cid_Adapter(SelectcityActivity.this, cid_cidparpm.getData());
                        lv.setAdapter(cityAdapter);
                    }
                }
            }
        }, id);
    }


    ResultCity_CityParam resultCityCity_param;//市

    private void loadcity(String id) {
        UserController.get_City(new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                if (data != null && (ResultCity_CityParam) data != null) {
                    resultCityCity_param = (ResultCity_CityParam) data;
                    if (resultCityCity_param.getStatus().equals("200")) {
                        tapy = "city";
                        City_City_Adapter cityAdapter = new City_City_Adapter(SelectcityActivity.this, resultCityCity_param.getData());
                        lv.setAdapter(cityAdapter);

                    }

                }
            }
        }, id);

    }

    ResultCity_ProvincesParam resultCity_provincesParam;//省份

    private void LoadData() {
        UserController.get_Provinces(new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                if (data != null && (ResultCity_ProvincesParam) data != null) {
                    resultCity_provincesParam = (ResultCity_ProvincesParam) data;
                    if (resultCity_provincesParam.getStatus().equals("200")) {
                        tapy = "Provinces";
                        City_Provinces_Adapter cityAdapter = new City_Provinces_Adapter(SelectcityActivity.this, resultCity_provincesParam.getData());
                        lv.setAdapter(cityAdapter);

                    }
                }
            }
        });
    }

    private void initView() {
        lv = (ListView) findViewById(R.id.lv_activity_selectcity);
        Linlay_back = (LinearLayout) findViewById(R.id.linlay_activity_selectcity_back);

        Linlay_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectcityActivity.this.finish();
            }
        });
    }

    public void onBackPressed() {
        if (tapy.equals("Provinces")) {
            SelectcityActivity.this.finish();
        }

        if (tapy.equals("city")) {
           LoadData();
        }

        if (tapy.equals("cid")) {
            loadcity(city_id);
        }
    }
}
