package cn.trcfitness.view.activity.trc;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import cn.trcfitness.R;
import cn.trcfitness.application.OverallApplication;
import cn.trcfitness.controller.UserController;
import cn.trcfitness.http.model.ResultParam;

/**
 * 余额管理-设置
 */
public class SetActvity extends Activity implements View.OnClickListener {

    private Context mContext = this;
    private PopupWindow popupWindow;
    private TextView mianmiMoney;

    private List<String> listMoneys = new ArrayList<String>();
    private String[] moneys = {"￥0", "￥500", "￥1000", "￥5000"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_set_actvity);

        mianmiMoney = (TextView) findViewById(R.id.set_mianmi_number);
        mianmiMoney.setText("￥ " + OverallApplication.exempt_money);

        //打开免密金额
        findViewById(R.id.set_mianmi_open).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserController.set_ExemptMoney(new Observer() {
                    @Override
                    public void update(Observable observable, Object data) {
                        if (data != null && (ResultParam) data != null) {
                            ResultParam resultParam = (ResultParam) data;
                            if (resultParam.getStatus().equals("200")) {
                                Toast.makeText(mContext, "免密金额为" + mianmiMoney.getText().toString(), Toast.LENGTH_LONG).show();
                                OverallApplication.exempt_money = mianmiMoney.getText().toString().replace("￥", "").trim();
                            } else {
                                Toast.makeText(mContext, "设置失败", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(mContext, "设置失败", Toast.LENGTH_LONG).show();
                        }
                    }
                }, mianmiMoney.getText().toString().replace("￥", "").trim());
                SetActvity.this.finish();
            }
        });

        //选择免密
        findViewById(R.id.set_mianmi).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showWindow(v);
            }
        });
        findViewById(R.id.set_mianmi_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SetActvity.this.finish();
            }
        });

        for (int i = 0; i < moneys.length; i++) {
            listMoneys.add(moneys[i]);
        }

    }


    int x = -1;
    int y = -1;

    /**
     * 弹出选择框
     *
     * @param parent
     */
    @TargetApi(Build.VERSION_CODES.KITKAT)
    private void showWindow(View parent) {
        if (popupWindow == null) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.popup_view, null);
            view.findViewById(R.id.tv_popup_0).setOnClickListener(this);
            view.findViewById(R.id.tv_popup_500).setOnClickListener(this);
            view.findViewById(R.id.tv_popup_1000).setOnClickListener(this);
            view.findViewById(R.id.tv_popup_5000).setOnClickListener(this);
            // 创建一个PopuWidow对象
            popupWindow = new PopupWindow(view, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
        }
        // 使其聚集
        popupWindow.setFocusable(true);
        // 设置允许在外点击消失
        popupWindow.setOutsideTouchable(true);
        // 这个是为了点击“返回Back”也能使其消失，并且并不会影响你的背景
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        if (x == -1 && y == -1) {
            int[] location = new int[2];
            findViewById(R.id.set_mianmi_number).getLocationOnScreen(location);
            x = location[0] - (location[0] / 20);
            y = location[1] + findViewById(R.id.set_mianmi_number).getHeight() + (location[0] / 30);
        }
        popupWindow.showAtLocation(findViewById(R.id.set_mianmi_number), Gravity.NO_GRAVITY, x, y);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_popup_0:
                mianmiMoney.setText("￥ 0");
                popupWindow.dismiss();
                break;
            case R.id.tv_popup_500:
                mianmiMoney.setText("￥ 500");
                popupWindow.dismiss();
                break;
            case R.id.tv_popup_1000:
                mianmiMoney.setText("￥ 1000");
                popupWindow.dismiss();
                break;
            case R.id.tv_popup_5000:
                mianmiMoney.setText("￥ 5000");
                popupWindow.dismiss();
                break;
        }
    }
}
