package cn.trcfitness.view.activity.trc;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Observable;
import java.util.Observer;

import cn.trcfitness.R;
import cn.trcfitness.controller.UserController;
import cn.trcfitness.http.model.ResultParam;
import cn.trcfitness.model.UserRequestParam;

/**
 * 余额管理-充值
 */
public class RechargeMoneyAct extends Activity {

    private Context mContext = this;
    private Button btnRecharge;
    private EditText moneyNum;

    private UserRequestParam param;

    private final float min_money = 0.01f;
    //记录当前输入的金额变化
    private String cur_monry = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_recharge_money);

        param = new UserRequestParam();
        moneyNum = (EditText)findViewById(R.id.edit_money);
        moneyNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (moneyNum.getText().toString().indexOf(".") >= 0) {
                    if (moneyNum.getText().toString().indexOf(".", moneyNum.getText().toString().indexOf(".") + 1) > 0) {
                        Toast.makeText(mContext, "不能添加多个小数点", Toast.LENGTH_SHORT).show();
                        moneyNum.setText(moneyNum.getText().toString().substring(0, moneyNum.getText().toString().length() - 1));
                        moneyNum.setSelection(moneyNum.getText().toString().length());
                    }

                }

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        btnRecharge = (Button)findViewById(R.id.btn_recharge);
        btnRecharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String str = moneyNum.getText().toString();

                if (str.equals("") || str == null){
                    Toast.makeText(mContext, "请输入充值金额", Toast.LENGTH_LONG).show();
                    return;
                }
                if (!str.contains(".") && str.substring(0,1).equals("0")){
                    Toast.makeText(mContext, "请正确输入金额", Toast.LENGTH_LONG).show();
                    return;
                }
                if (str.substring(0,1).equals(".")){
                    Toast.makeText(mContext, "请正确输入金额", Toast.LENGTH_LONG).show();
                    return;
                }
                if (str.substring(str.length()-1, str.length()).equals(".")){
                    Toast.makeText(mContext, "请正确输入金额", Toast.LENGTH_LONG).show();
                    return;
                }
                Float money = Float.parseFloat(str);
                if (money <= 0f){
                    Toast.makeText(mContext, "金额不能为0", Toast.LENGTH_LONG).show();
                    return;
                }
                if (money < min_money){
                    Toast.makeText(mContext, "金额不能小于0.01", Toast.LENGTH_LONG).show();
                    return;
                }

                if (money > 5000f){
                    Toast.makeText(mContext, "金额不能大于5000", Toast.LENGTH_LONG).show();
                    return;
                }

                Intent selectPayIntent = new Intent(mContext, SelectPayAct.class);
                selectPayIntent.putExtra("money_num", money);
                startActivity(selectPayIntent);
            }
        });
        findViewById(R.id.recharge_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RechargeMoneyAct.this.finish();
            }
        });

    }

}
