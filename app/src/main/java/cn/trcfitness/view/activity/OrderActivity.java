package cn.trcfitness.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.joooonho.SelectableRoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

import cn.trcfitness.R;
import cn.trcfitness.application.OverallApplication;
import cn.trcfitness.controller.UserController;
import cn.trcfitness.custom.PayPopupWindow;
import cn.trcfitness.http.model.ResultParam;
import cn.trcfitness.model.ProductsBean;
import cn.trcfitness.model.RequestOrderParam;
import cn.trcfitness.model.ResultAddressParam;
import cn.trcfitness.model.ResultisUsePwdParam;
import cn.trcfitness.model.UserData;
import cn.trcfitness.model.UserDetail;
import cn.trcfitness.model.UserRequestParam;

/**
 * Created by ziv on 2016/5/16.
 * 确认订单
 */
public class OrderActivity extends Activity implements View.OnClickListener {
    private LinearLayout Linlay_Selectaddress, Linlay_back, Linlay_sizecolor, Linlay_xx;
    private RelativeLayout Rellay_address;
    private TextView tv_consignee, tv_consignee_name, tv_phone, tv_address, tv_price, tv_number,
            tv_minus, tv_add, tv_postage, tv_money, tv_number1, tv_money1, tv_name, tv_color, tv_size;
    private EditText et_message;
    private Button bt_order;
    private ProductsBean productsBean;//商品详细
    private ProductsBean.Size size;//尺码
    private ProductsBean.ProductColor productColor;//颜色
    ResultAddressParam.ReciptAddressparam mReciptAddressparam;//收货地址
    private int number;//选择的个数
    private SelectableRoundedImageView im;
    Float postage = 0f;//邮费
    private Boolean isaddress = false;//是否有默认收获地址
    static UserDetail userDetail;//y用户数据

    private PayPopupWindow payPopupWindow;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_order);
        UserData userData = OverallApplication.userData;
        if (userData != null) {
            userDetail = userData.getData().getUser();
        }
        initView();
        payPopupWindow = PayPopupWindow.getInstance(OrderActivity.this);
        if (userDetail != null) {
            initData();
        } else {
            Toast.makeText(this, "重新登录", Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * 初始化数据
     */
    private void initData() {
        Bundle bundle = getIntent().getExtras();
        productsBean = (ProductsBean) bundle.getSerializable("products");//获取商品对象
        number = Integer.parseInt(bundle.get("number") + "");//获取商品个数
        if (productsBean.getType() == 1) {//实体商品独有参数
            size = (ProductsBean.Size) bundle.getSerializable("size");//获取商品尺寸
            productColor = (ProductsBean.ProductColor) bundle.getSerializable("productColor");//获取商品颜色
            UserController.get_ReciptAddress(new Observer() {
                @Override
                public void update(Observable observable, Object data) {
                    if (data != null && (ResultAddressParam) data != null) {
                        ResultAddressParam resultAddressParam = (ResultAddressParam) data;
                        if (resultAddressParam.getStatus().equals("200")) {
                            if (resultAddressParam.getData().getReciptAddress().size() > 0) {
                                LoadingAddress(resultAddressParam.getData().getReciptAddress());//获取出所有收货地址，判断默认并显示
                            } else {
                                isaddress = false;
                            }
                        }
                    }
                }
            }, "");
            postage = productsBean.getPostage();
            if (productColor != null) {
                tv_color.setText(productColor.getColor());
            }
            if (size != null) {
                tv_size.setText(size.getSize());
            }
            if (postage == 0) {
                tv_postage.setText("快递 免邮");
            } else {
                tv_postage.setText("快递 " + productsBean.getPostage());
            }
        } else if (productsBean.getType() == 0) {
            Rellay_address.setVisibility(View.GONE);
            tv_consignee_name.setText("昵称：");
            tv_consignee.setText(userDetail.getNick_name());
            tv_phone.setText(userDetail.getPhone());
            Linlay_sizecolor.setVisibility(View.INVISIBLE);
            Linlay_xx.setVisibility(View.GONE);
        }

        Picasso.with(OrderActivity.this).load(productsBean.getPcs().get(0).getCover()).into(im);
        tv_price.setText(productsBean.getPrice() + "");//单价
        tv_number.setText(number + "");
        tv_number1.setText(number + "");
        tv_money.setText((number * productsBean.getPrice()) + "");//合计 不加邮费
        tv_money1.setText((number * productsBean.getPrice() + postage) + "");//合计 加邮费
        tv_name.setText(productsBean.getName());

    }

    /**
     * 判断默认收获地址，并显示
     */
    private void LoadingAddress(List<ResultAddressParam.ReciptAddressparam> reciptAddressparamList) {
        ResultAddressParam.ReciptAddressparam reciptAddress_Defaults = null;//默认收货地址
        for (ResultAddressParam.ReciptAddressparam param : reciptAddressparamList) {
            if (param.getDefaults()) {
                isaddress = true;
                reciptAddress_Defaults = param;
            }
        }

        tv_consignee.setText(reciptAddress_Defaults.getConsignee());
        tv_phone.setText(reciptAddress_Defaults.getPhone());
        tv_address.setText("收货地址：" + reciptAddress_Defaults.getAddress());
        mReciptAddressparam = reciptAddress_Defaults;
    }

    private void initView() {
        Linlay_Selectaddress = (LinearLayout) findViewById(R.id.linlay_activity_order_selectaddress);//收货地址
        Linlay_back = (LinearLayout) findViewById(R.id.linlay_activity_order_back);//返回按钮
        Linlay_sizecolor = (LinearLayout) findViewById(R.id.Linlay_activity_order_sizecolor);//颜色分类  尺码
        Linlay_xx = (LinearLayout) findViewById(R.id.linlay_activity_order_xx);
        Rellay_address = (RelativeLayout) findViewById(R.id.rellay_activity_order_address);
        tv_consignee = (TextView) findViewById(R.id.tv_activity_order_consignee);
        tv_consignee_name = (TextView) findViewById(R.id.tv_activity_order_consignee_name);
        tv_phone = (TextView) findViewById(R.id.tv_activity_order_phone);
        tv_address = (TextView) findViewById(R.id.tv_activity_order_address);
        tv_postage = (TextView) findViewById(R.id.tv_activity_order_postage);
        im = (SelectableRoundedImageView) findViewById(R.id.im_acityity_order);
        tv_price = (TextView) findViewById(R.id.tv_activity_order_price);
        tv_number = (TextView) findViewById(R.id.tv_activity_order_number);
        tv_minus = (TextView) findViewById(R.id.tv_activity_order_minus);
        tv_add = (TextView) findViewById(R.id.tv_activity_order_add);
        tv_money = (TextView) findViewById(R.id.tv_activity_order_money);
        tv_number1 = (TextView) findViewById(R.id.tv_activity_order_number1);
        tv_money1 = (TextView) findViewById(R.id.tv_activity_order_money1);
        tv_name = (TextView) findViewById(R.id.tv_activity_order_name);
        tv_color = (TextView) findViewById(R.id.tv_activity_order_color);
        tv_size = (TextView) findViewById(R.id.tv_activity_order_size);
        bt_order = (Button) findViewById(R.id.bt_activity_order);
        et_message = (EditText) findViewById(R.id.et_activity_order_message);

        Linlay_Selectaddress.setOnClickListener(this);
        Linlay_back.setOnClickListener(this);
        tv_minus.setOnClickListener(this);
        tv_add.setOnClickListener(this);
        bt_order.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.linlay_activity_order_selectaddress:
                if (productsBean.getType() == 1) {//实体商品才跳转到收货地址界面
                    Intent intent = new Intent(OrderActivity.this, ManageReciptAddressActivity.class);
                    intent.putExtra("tapy", "0");
                    intent.putExtra("store", "0");
                    startActivityForResult(intent, 1);
                }
                break;
            case R.id.linlay_activity_order_back://返回
                finish();
                break;
            case R.id.tv_activity_order_minus://商品个数减
                changenumber("-");
                break;
            case R.id.tv_activity_order_add://商品个数加
                changenumber("+");
                break;
            case R.id.bt_activity_order://确认购买
                ConfirmOrder();
                break;
        }
    }


    private RequestOrderParam requestOrderParam;

    /**
     * 确认订单  处理需要的数据 判断是否需要密码
     */
    private void ConfirmOrder() {
        requestOrderParam = new RequestOrderParam();//保存用户订单上传参数

        requestOrderParam.setPid(new Integer(productsBean.getId()));//商品ID
        requestOrderParam.setMessage(et_message.getText().toString().trim());//买家留言
        requestOrderParam.setNumber(number);//商品个数
        if (productsBean.getType() == 1) {
            requestOrderParam.setType(0);//送货方式
            if (productColor != null) {
                requestOrderParam.setClassify(productColor.getColor());//颜色
            }
            if (size != null) {
                requestOrderParam.setSize(size.getSize());//尺寸
            }

            if (isaddress) {
                requestOrderParam.setRid(mReciptAddressparam.getId());//收货地址id
            } else {
                Intent intent = new Intent(OrderActivity.this, ManageReciptAddressActivity.class);
                intent.putExtra("tapy", "0");
                intent.putExtra("store", "0");
                startActivityForResult(intent, 1);
                Toast.makeText(OrderActivity.this, "请选择收货地址", Toast.LENGTH_SHORT).show();
                return;
            }
        }

        String money = tv_money1.getText().toString().trim();
        UserController.isUsePwd(new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                if (data != null && (ResultisUsePwdParam) data != null) {
                    ResultisUsePwdParam resultisUsePwdParam = (ResultisUsePwdParam) data;
                    if (resultisUsePwdParam.getStatus().equals("200")) {//不需要返回true , 需要返回false
                        if (resultisUsePwdParam.isData()) {
                            uploadOrder();//不需要的话直接上传订单
                        } else {
                            isPassword();//需要的话先验证密码
                        }
                    }
                }
            }
        }, money.substring(0, money.length() - 2));
    }

    /**
     * @return 判断支付密码
     */
    private void isPassword() {
        LinearLayout relativeLayout = (LinearLayout) findViewById(R.id.relative);
        backgroundAlpha(0.6f);
        payPopupWindow.show(relativeLayout, new PayPopupWindow.CallbackListener() {
            @Override
            public void onFinish(String code) {
                payPopupWindow.CloseShow();
                backgroundAlpha(1f);
                isPassWord(code);
            }
        });
    }

    private void isPassWord(String userCode) {
        UserRequestParam userRequestParam = new UserRequestParam();
        userRequestParam.setPwd(userCode);
        UserController.set_check_paypasswd(userRequestParam, new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                if (data != null && (ResultParam) data != null) {
                    ResultParam resultParam = (ResultParam) data;
                    if (resultParam.getStatus().equals("200")) {
                        uploadOrder();
                    } else {
                    }
                }
            }
        });
    }

    /**
     * 上传订单
     */
    private void uploadOrder() {
        UserController.save_Order(new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                if (data != null && (ResultParam) data != null) {
                    ResultParam resultParam = (ResultParam) data;
                    if (resultParam.getStatus().equals("200")) {
                        backgroundAlpha(1f);
                        Toast.makeText(OrderActivity.this, "上传订单成功", Toast.LENGTH_SHORT).show();
                        OrderActivity.this.finish();
                        Intent intent = new Intent(OrderActivity.this, MyOrderActivity.class);
                        intent.putExtra("type", 0);
                        startActivity(intent);
                    }
                }
            }
        }, requestOrderParam);
    }

    /**
     * 设置添加屏幕的背景透明度
     */
    public void backgroundAlpha(float bgAlpha) {
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.alpha = bgAlpha; //0.0-1.0
        getWindow().setAttributes(lp);
    }

    /**
     * 商品个数加减
     */
    private void changenumber(String s) {
        //如果是虚拟商品type=0,或者虚拟商品不为次数的不能加减个数，Category=2为虚拟次数商品
//        if (productsBean.getType().intValue() == 0 && productsBean.getCategory().intValue() != 2) {
//            return;
//        }
        if (s.equals("+")) {
            //虚拟次数商品不需要判断库存，直接进行加，虚拟次数商品库存Stock返回为null
            if (productsBean.getCategory() != null || productsBean.getStock().intValue() > number) {
                number++;
            }
        } else if (number > 1) {
            number--;
        }
        tv_number.setText(number + "");
        tv_number1.setText(number + "");
        tv_money.setText((number * productsBean.getPrice()) + "");//合计 不加邮费
        tv_money1.setText((number * productsBean.getPrice() + postage) + "");//合计 加邮费
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            Bundle bundle = data.getExtras();
            ResultAddressParam.ReciptAddressparam reciptAddressparam = (ResultAddressParam.ReciptAddressparam) bundle.getSerializable("reciptAddress");
            tv_consignee.setText(reciptAddressparam.getConsignee());
            tv_phone.setText(reciptAddressparam.getPhone());
            tv_address.setText(reciptAddressparam.getAddress());
            mReciptAddressparam = reciptAddressparam;
            isaddress = true;
        }
    }

    @Override
    public void onBackPressed() {
        if (payPopupWindow.isShow()) {
            payPopupWindow.CloseShow();
            backgroundAlpha(1f);
        } else {
            finish();
        }
    }
}
