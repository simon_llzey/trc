package cn.trcfitness.view.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import cn.trcfitness.R;
import cn.trcfitness.controller.FriendController;
import cn.trcfitness.custom.LoadDialog;
import cn.trcfitness.model.Frends;
import cn.trcfitness.model.FriendResultParam;
import cn.trcfitness.model.UserRequestParam;
import cn.trcfitness.view.adapter.FriendAdapter;

/**
 * 搜索好友
 */
public class SearchFriendAct extends Activity implements View.OnClickListener {

    private Context mContext = this;
    private PullToRefreshListView mListView;
    private LinearLayout Linlay_break;
    private FriendAdapter adapter;
    private EditText et_search;
    private ImageView im_empty;
    private List<Frends> mList;

    private int pageOffset = 0;//页数
    private static final int REFRESH = 0;
    private static final int MORE = 1;
    private UserRequestParam userRequest;

    private LinearLayout Linlay_no_content;
    private LoadDialog loadDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_search_friend);
        loadDialog = new LoadDialog(this);
        loadDialog.show();

        initView();
    }

    private void initView() {
        mListView = (PullToRefreshListView) findViewById(R.id.search_list);
        et_search = (EditText) findViewById(R.id.et_activity_search_friend);//好友搜索
        im_empty = (ImageView) findViewById(R.id.im_activity_search_friend_empty);
        Linlay_break = (LinearLayout) findViewById(R.id.Linlay_activity_search_friend_break);
        Linlay_no_content = (LinearLayout) findViewById(R.id.Linlay_activity_friend_no_content);

        im_empty.setOnClickListener(this);
        Linlay_break.setOnClickListener(this);

        mList = new ArrayList<Frends>();
        adapter = new FriendAdapter(mContext, mList, true);
        mListView.setAdapter(adapter);
        mListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            //下拉刷新
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                pageOffset = 0;
                userRequest.setPageOffset(pageOffset);
                getFriendList(REFRESH);
            }

            //上拉加载更多
            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                if (mList != null && mList.size() > 0) {
                    pageOffset = mList.size();
                }
                userRequest.setPageOffset(pageOffset);
                getFriendList(MORE);
            }
        });
        userRequest = new UserRequestParam();
        userRequest.setPageSize(20);
        userRequest.setPageOffset(pageOffset);
        getFriendList(REFRESH);

        et_search.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    HideKeyboard(v);
                    pageOffset = 0;
                    userRequest.setPageOffset(pageOffset);
                    userRequest.setKey(et_search.getText().toString().trim());
                    getFriendList(REFRESH);
                }
                return false;
            }
        });
    }

    //隐藏虚拟键盘
    public static void HideKeyboard(View v) {
        InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm.isActive()) {
            imm.hideSoftInputFromWindow(v.getApplicationWindowToken(), 0);
        }
    }

    /**
     * @param state 获取所有用户列表
     */
    private void getFriendList(final int state) {
        FriendController.getFriendList(new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                loadDialog.cancel();
                if (data != null && (FriendResultParam) data != null) {
                    FriendResultParam resultParam = (FriendResultParam) data;
                    if (state == REFRESH) {
                        mList.clear();
                        mList = resultParam.getData().getFrends();
                        RefreshFriend();
                    } else if (state == MORE) {
                        mList.addAll(resultParam.getData().getFrends());
                        RefreshFriend();
                        if (resultParam.getData().getFrends().size() == 0) {
                            mToast("没有更多好友");
                        }
                    }
                } else {
//                    mToast("更新失败");
                    mListView.onRefreshComplete();
                }
            }
        }, userRequest);
    }

    /**
     * 获取我的用户列表
     */
    private void RefreshFriend() {
        if (mList != null && !mList.isEmpty()) {
            Linlay_no_content.setVisibility(View.GONE);
        } else {
            Linlay_no_content.setVisibility(View.VISIBLE);
        }
        adapter.setList(mList);
        mListView.onRefreshComplete();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.im_activity_search_friend_empty:
                et_search.setText("");
                break;
            case R.id.Linlay_activity_search_friend_break:
                finish();
                break;
        }
    }

    private void mToast(String text) {
        Toast.makeText(SearchFriendAct.this, text, Toast.LENGTH_SHORT).show();
    }
}
