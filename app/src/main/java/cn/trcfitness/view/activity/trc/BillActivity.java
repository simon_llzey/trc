package cn.trcfitness.view.activity.trc;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import cn.trcfitness.R;
import cn.trcfitness.controller.UserController;
import cn.trcfitness.model.ResultBillParam;
import cn.trcfitness.view.activity.trc.adapter.BillAdapter;

/**
 * trc-账单
 */
public class BillActivity extends Activity implements View.OnClickListener {

    private BillAdapter billAdapter;
    private Context mContext = this;
    private PullToRefreshListView mListView;
    //选择时间弹窗
    private PopupWindow popupWindow;
    private List<String> listTimes = new ArrayList<String>();
    private String[] times = {"全部", "两天内", "七天内", "一个月内"};

    //记录当前选择的天数
    private int currentDay = 0;

    private List<ResultBillParam.DataBean.ConsumeDetailsBean> mList;

    private Handler mHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case 0:
                    billAdapter.notifyDataSetChanged();
                    mListView.onRefreshComplete();
                    break;
            }


        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_bill);
        initView();
    }

    private void initView() {
        mListView = (PullToRefreshListView) findViewById(R.id.bill_list);
        mListView.setMode(PullToRefreshBase.Mode.PULL_FROM_START);

        mList = new ArrayList<ResultBillParam.DataBean.ConsumeDetailsBean>();
        billAdapter = new BillAdapter(mContext, mList);
        mListView.setAdapter(billAdapter);

        findViewById(R.id.bill_time).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showWindow(v);
            }
        });
        findViewById(R.id.bill_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BillActivity.this.finish();
            }
        });

        for (int i = 0; i < times.length; i++) {
            listTimes.add(times[i]);
        }

        mListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
            @Override
            public void onRefresh(PullToRefreshBase<ListView> refreshView) {
                RefreshBill(currentDay,false);
            }
        });
        RefreshBill(currentDay,false);
    }


    /**
     *
     * @param day 多久前的账单
     * @param isLoader 是否加载更多
     */
    private void RefreshBill(int day, boolean isLoader) {

        if (isLoader){
            if ((mList != null && mList.size() > 0)) {
            }
        }else {
            //清空本地列表
            mList.clear();
        }

        UserController.get_bills(new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                if (data != null && (ResultBillParam) data != null) {
                    ResultBillParam resultBillParam = (ResultBillParam) data;
                    if (resultBillParam.getStatus().equals("200")) {
                        List<ResultBillParam.DataBean.ConsumeDetailsBean> list = resultBillParam.getData().getConsumeDetails();

                        for (ResultBillParam.DataBean.ConsumeDetailsBean detailsBean : list){
                            mList.add(detailsBean);
                        }
                        mHandler.sendEmptyMessage(0);
//                        mListView.getRefreshableView().setSelection(0);
                    }
                }

            }
        }, day);

    }

    int x = -1;
    int y = -1;

    /**
     * 弹出选择框
     *
     * @param parent
     */
    private void showWindow(View parent) {

        if (popupWindow == null) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.popup_view, null);
            ((TextView) view.findViewById(R.id.tv_popup_0)).setText("全部");
            ((TextView) view.findViewById(R.id.tv_popup_500)).setText("两天内");
            ((TextView) view.findViewById(R.id.tv_popup_1000)).setText("七天内");
            ((TextView) view.findViewById(R.id.tv_popup_5000)).setText("一个月内");

            view.findViewById(R.id.tv_popup_0).setOnClickListener(this);
            view.findViewById(R.id.tv_popup_500).setOnClickListener(this);
            view.findViewById(R.id.tv_popup_1000).setOnClickListener(this);
            view.findViewById(R.id.tv_popup_5000).setOnClickListener(this);
            // 创建一个PopuWidow对象
            popupWindow = new PopupWindow(view, WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT, true);
        }

        // 使其聚集
        popupWindow.setFocusable(true);
        // 设置允许在外点击消失
        popupWindow.setOutsideTouchable(true);
        // 这个是为了点击“返回Back”也能使其消失，并且并不会影响你的背景
        popupWindow.setBackgroundDrawable(new BitmapDrawable());

        if (x == -1 && y == -1) {
            int[] location = new int[2];
            findViewById(R.id.tv_bill_time).getLocationOnScreen(location);
            x = location[0] - (location[0] / 7);
            y = location[1] + findViewById(R.id.tv_bill_time).getHeight() + (location[0] / 30);
        }
        popupWindow.showAtLocation(findViewById(R.id.tv_bill_time), Gravity.NO_GRAVITY, x, y);
    }




    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_popup_0:
                ((TextView) findViewById(R.id.tv_bill_time)).setText("全部");
                RefreshBill(0, false);
                currentDay = 0;
                popupWindow.dismiss();
                break;
            case R.id.tv_popup_500:
                ((TextView) findViewById(R.id.tv_bill_time)).setText("两天内");
                RefreshBill(1, false);
                currentDay = 1;
                popupWindow.dismiss();
                break;
            case R.id.tv_popup_1000:
                ((TextView) findViewById(R.id.tv_bill_time)).setText("七天内");
                RefreshBill(2, false);
                currentDay = 2;
                popupWindow.dismiss();
                break;
            case R.id.tv_popup_5000:
                ((TextView) findViewById(R.id.tv_bill_time)).setText("一个月内");
                RefreshBill(3, false);
                currentDay = 3;
                popupWindow.dismiss();
                break;
        }
    }
}
