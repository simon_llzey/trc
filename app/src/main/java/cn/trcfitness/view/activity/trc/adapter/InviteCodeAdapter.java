package cn.trcfitness.view.activity.trc.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import cn.trcfitness.R;
import cn.trcfitness.model.Codes;

/**
 * Created by Administrator on 2016/5/16.
 * 我的邀请码
 */
public class InviteCodeAdapter extends BaseAdapter {

    private Context mContext;
    private List<Codes> mList;

    public InviteCodeAdapter(Context context, List<Codes> list){
        this.mContext = context;
        this.mList = list;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null){
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.invitecode_item_list, null);
            viewHolder.code = (TextView)convertView.findViewById(R.id.invite_code);
            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder)convertView.getTag();
        }

        if (mList.get(position).isUsed()){
//            viewHolder.code.setTextColor(mContext.getResources().getColor(R.color.invite_code_sel));
            viewHolder.code.setTextColor(mContext.getResources().getColor(R.color.yellow));
        }else {
            viewHolder.code.setTextColor(mContext.getResources().getColor(R.color.invite_code));
        }
        viewHolder.code.setText(mList.get(position).getCode());
        return convertView;
    }

    private class ViewHolder{
        TextView code;
    }
}
