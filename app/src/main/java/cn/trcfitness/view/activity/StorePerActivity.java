package cn.trcfitness.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.joooonho.SelectableRoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import cn.trcfitness.R;
import cn.trcfitness.application.OverallApplication;
import cn.trcfitness.custom.CustomGridView;
import cn.trcfitness.model.TRCInfo;
import cn.trcfitness.model.UserData;
import cn.trcfitness.model.UserDetail;
import cn.trcfitness.view.adapter.GrvStorePerspnalAdapter;

/**
 * Created by ziv on 2016/5/16.
 * 个人中心
 */
public class StorePerActivity extends Activity implements View.OnClickListener {
    private CustomGridView mGridView;
    private String[] names = {"我的订单", "待发货", "待收货", "待评价", "退货/售后", "地址管理",};
    private int[] images = {R.drawable.grv_activity_storepersonal1, R.drawable.grv_activity_storepersonal3,
            R.drawable.grv_activity_storepersonal4, R.drawable.grv_activity_storepersonal5, R.drawable.grv_activity_storepersonal6,
            R.drawable.grv_activity_storepersonal7};
    private List<TRCInfo> mList = new ArrayList<TRCInfo>();

    private SelectableRoundedImageView im_storepersonal;
    private TextView tv_name;
    static UserDetail userDetail;//用户数据
    private LinearLayout linlay_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_storepersonal);
        initview();

        UserData userData = OverallApplication.userData;
        if (userData != null) {
            userDetail = userData.getData().getUser();
        }
        if (userDetail != null) {
            initData();
        } else {
            Toast.makeText(this, "null重新登录", Toast.LENGTH_SHORT).show();
        }
    }

    private void initData() {
        im_storepersonal.setImageResource(R.drawable.def_head);
        Picasso.with(StorePerActivity.this).load(userDetail.getImg()).into(im_storepersonal);
        tv_name.setText(userDetail.getNick_name());
        for (int i = 0; i < names.length; i++) {
            TRCInfo info = new TRCInfo();
            info.setName(names[i]);
            info.setImage(images[i]);
            mList.add(info);
        }
        mGridView.setAdapter(new GrvStorePerspnalAdapter(this.getApplicationContext(), mList));
    }

    private void initview() {
        mGridView = (CustomGridView) findViewById(R.id.grv_activity_storepersonal);
        im_storepersonal = (SelectableRoundedImageView) findViewById(R.id.im_activity_storepersonal);
        tv_name = (TextView) findViewById(R.id.activity_storepersonal_name);
        linlay_back = (LinearLayout) findViewById(R.id.linlay_activity_storepersonal_back);
        linlay_back.setOnClickListener(this);

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 4) {
                    Intent intent = new Intent(StorePerActivity.this, ExitOrderActivity.class);
                    startActivity(intent);
                    return;
                } else if (position == 5) {
                    Intent intent = new Intent(StorePerActivity.this, ManageReciptAddressActivity.class);
                    intent.putExtra("tapy", "1");
                    intent.putExtra("store", "1");
                    startActivity(intent);
                    return;
                } else if (position < 4) {
                    Intent intent = new Intent(StorePerActivity.this, MyOrderActivity.class);
                    intent.putExtra("type", position);
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.linlay_activity_storepersonal_back:
                finish();
                break;
        }
    }
}
