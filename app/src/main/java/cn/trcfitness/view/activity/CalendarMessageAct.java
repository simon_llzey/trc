package cn.trcfitness.view.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import cn.trcfitness.R;
import cn.trcfitness.controller.UserController;
import cn.trcfitness.model.ResultMessageParam;
import cn.trcfitness.model.UserRequestParam;
import cn.trcfitness.view.adapter.CalendarMessageAdapter;

/**
 * 日历-消息
 */
public class CalendarMessageAct extends Activity implements View.OnClickListener {

    private Context mContext = this;
    private PullToRefreshListView mListView;
    private CalendarMessageAdapter adapter;
    private List<ResultMessageParam.DataBean.MessageBean> mList = new ArrayList<ResultMessageParam.DataBean.MessageBean>();
    private LinearLayout Linlay_back, Linlay_delect;
    private TextView tv_delect;

    private int pageOffset = 0;//页数
    private static final int REFRESH = 0;
    private static final int MORE = 1;
    private UserRequestParam userRequest;

    private LinearLayout Linlay_no_content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_calendar_message);
        initView();
    }

    private void initView() {
        adapter = new CalendarMessageAdapter(mContext, mList);
        mListView = (PullToRefreshListView) findViewById(R.id.message_list);
        Linlay_back = (LinearLayout) findViewById(R.id.Linlay_activity_message_back);
        Linlay_delect = (LinearLayout) findViewById(R.id.Linlay_activity_message_delect);
        Linlay_no_content = (LinearLayout) findViewById(R.id.Linlay_activity_calendar_no_content);
        tv_delect = (TextView) findViewById(R.id.tv_activity_message_delect);
        Linlay_back.setOnClickListener(this);
        Linlay_delect.setOnClickListener(this);
        mListView.setAdapter(adapter);
        mListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            //下拉刷新
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                pageOffset = 0;
                userRequest.setPageOffset(pageOffset);
                getData(REFRESH);
            }

            //上拉加载更多
            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                if (mList != null && mList.size() > 0) {
                    pageOffset = mList.size();
                }
                userRequest.setPageOffset(pageOffset);
                getData(MORE);
            }
        });


//        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Intent detailIntent = new Intent(mContext, MessageDeatailAct.class);
//                Bundle bundle = new Bundle();
//                bundle.putSerializable("MessageBean", mList.get(position - 1));
//                detailIntent.putExtras(bundle);
//                startActivity(detailIntent);
//            }
//        });

        userRequest = new UserRequestParam();
        userRequest.setPageSize(20);
        userRequest.setPageOffset(pageOffset);
        getData(REFRESH);
    }

    private void getData(final int state) {
        UserController.Message(new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                if (data != null && (ResultMessageParam) data != null) {
                    ResultMessageParam resultMessageParam = (ResultMessageParam) data;
                    if (state == REFRESH) {
                        mList.clear();
                        mList = resultMessageParam.getData().getMessage();
                        RefreshFriend();
                    } else if (state == MORE) {
                        mList.addAll(resultMessageParam.getData().getMessage());
                        RefreshFriend();
                        if (resultMessageParam.getData().getMessage().size() == 0) {
                            mToast("没有更多数据");
                        }
                    }
                } else {
                    mListView.onRefreshComplete();
                }
            }
        },userRequest);
    }


    private void RefreshFriend() {
        if (mList != null && !mList.isEmpty()) {
            Linlay_no_content.setVisibility(View.GONE);
        } else {
            Linlay_no_content.setVisibility(View.VISIBLE);
        }
        adapter.setList(mList);
        mListView.onRefreshComplete();
        adapter.notifyDataSetChanged();
    }

    private void mToast(String text) {
        Toast.makeText(CalendarMessageAct.this, text, Toast.LENGTH_SHORT).show();
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.Linlay_activity_message_back:
                CalendarMessageAct.this.finish();
                break;
            case R.id.Linlay_activity_message_delect:
                if (adapter.getDelect()) {
                    adapter.setDelect(false);
                    tv_delect.setText("编辑");
                } else {
                    adapter.setDelect(true);
                    tv_delect.setText("取消");
                }
                adapter.notifyDataSetChanged();
                break;
        }
    }
}
