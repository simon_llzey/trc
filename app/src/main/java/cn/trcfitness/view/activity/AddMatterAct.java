package cn.trcfitness.view.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Observable;
import java.util.Observer;
import java.util.TimeZone;

import cn.trcfitness.R;
import cn.trcfitness.controller.UserController;
import cn.trcfitness.custom.CustomDatePickerDialog;
import cn.trcfitness.custom.CustomTimePickerDialog;
import cn.trcfitness.http.model.ResultParam;
import cn.trcfitness.model.AddCalendarEvent;
import cn.trcfitness.utils.DateUtils;
import cn.trcfitness.view.fragmet.CalendarFra;

/**
 * 日历新建事件
 */
public class AddMatterAct extends Activity implements View.OnClickListener {

    private Context mContext = AddMatterAct.this;
    private TextView alertTime, startTime, endTime;
    private EditText locationText, titleText, remarkText;


    private AddCalendarEvent calendarEvent;
    //日历控件
    private CustomDatePickerDialog mDateDialog;
    //时间控件
    private CustomTimePickerDialog mTimeDialog;

    //开始时间
    private String start_date = "";
    private String start_time = "";
    //结束时间
    private String end_date = "";
    private String end_time = "";
    //提醒时间
    private String alertStr = "";

    //传过来选择的日期
    private String yearStr, monthStr, dayStr;
    //记录开始时间和结束时间
    int start_hour = 0;
    int start_minute = 0;
    int end_hour = 0;
    int end_minute = 0;

    //当前时间
    int hour = 0;
    int minute = 0;

    //记录选择的开始年月日
    private int start_year;
    private int start_month;
    private int start_day;
    //记录选择的结束年月日
    private int end_year;
    private int end_month;
    private int end_day;

    //选中新增事件的日期
    private String currrent_time = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_add_matter);

        initView();
    }


    private void initView() {

        currrent_time = getIntent().getStringExtra("time");
        String[] date = currrent_time.split("/");
        yearStr = date[0];
        monthStr = date[1];
        dayStr = date[2];


        Log.i("AddMatterAct", "添加的选中的时间" + currrent_time);
        titleText = (EditText) findViewById(R.id.matter_title);
        locationText = (EditText) findViewById(R.id.matter_location);
        remarkText = (EditText) findViewById(R.id.matter_remark);

        alertTime = (TextView) findViewById(R.id.matter_alert_time);
        startTime = (TextView) findViewById(R.id.matter_start_time);
        endTime = (TextView) findViewById(R.id.matter_end_time);


        calendarEvent = new AddCalendarEvent();

        findViewById(R.id.matter_alert).setOnClickListener(this);
        findViewById(R.id.matter_start).setOnClickListener(this);
        findViewById(R.id.matter_end).setOnClickListener(this);
        findViewById(R.id.matter_back).setOnClickListener(this);
        findViewById(R.id.matter_save).setOnClickListener(this);
        findViewById(R.id.matter_alert).setOnClickListener(this);
    }


    /**
     * 保存事件
     */
    private void postData() {

        if (titleText.getText().toString() == null || titleText.getText().toString().equals("")) {
            Toast.makeText(mContext, "请填写标题", Toast.LENGTH_LONG).show();
            return;
        }
        if (locationText.getText().toString().equals("") || locationText.getText().toString() == null) {
            Toast.makeText(mContext, "请填写位置", Toast.LENGTH_LONG).show();
            return;
        }
        if (startTime.getText().toString().equals("") || startTime.getText().toString() == null) {
            Toast.makeText(mContext, "请填写开始时间", Toast.LENGTH_LONG).show();
            return;
        }
        if (endTime.getText().toString().equals("") || endTime.getText().toString() == null) {
            Toast.makeText(mContext, "请填写结束时间", Toast.LENGTH_LONG).show();
            return;
        }

        if (currrent_time.equals("")) {
            Toast.makeText(mContext, "未选择创建时间", Toast.LENGTH_LONG).show();
            return;
        }

        if (!remarkText.getText().equals("") && remarkText.getText() != null) {
            calendarEvent.setRemark(remarkText.getText().toString());
        }

        int alert = -1;
        if (!alertStr.equals("")) {
            alert = Integer.parseInt(alertStr);
            if (alert != -1) {
                calendarEvent.setRemind(alert);
            }

        }


        Log.i("AddMatter", "获取提醒时间" + alert + "和创建时间" + currrent_time);

        calendarEvent.setSpecific_time(currrent_time);
        calendarEvent.setTitle(titleText.getText().toString());
        calendarEvent.setLocation(locationText.getText().toString());
        calendarEvent.setBegin(startTime.getText().toString());
        calendarEvent.setEnd(endTime.getText().toString());

        long start = DateUtils.date(startTime.getText().toString());
        long end = DateUtils.date(endTime.getText().toString());
//        initCalendars();
        addEvent(titleText.getText().toString(), remarkText.getText().toString(), locationText.getText().toString(), alert, start, end);
        UserController.save_calendar_event(calendarEvent, new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                ResultParam resultParam = (ResultParam) data;
                if (resultParam != null) {
                    if (resultParam.isStatus()) {
                        Toast.makeText(mContext, "上传成功", Toast.LENGTH_LONG).show();
                        AddMatterAct.this.finish();
                        if (CalendarDetailAct.instance != null){
                            CalendarDetailAct.instance.finish();
                        }
                        CalendarFra.instance.refreshDate();
                    } else {
                        AddMatterAct.this.finish();
                        Toast.makeText(mContext, "上传失败", Toast.LENGTH_LONG).show();
                    }
                } else {
                    AddMatterAct.this.finish();
                    Toast.makeText(mContext, "上传失败2", Toast.LENGTH_LONG).show();
                }


            }
        });

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.matter_alert:
                Intent openIntent = new Intent(mContext, CalendarAlertAct.class);
                startActivityForResult(openIntent, 0);
                break;
            case R.id.matter_start:
                showDate(startTime);
                break;
            case R.id.matter_end:
                if (startTime.getText() != null && !startTime.getText().equals("无")){
                    showEndDate(endTime);
                }else {
                    Toast.makeText(mContext, "请先选择开始时间", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.matter_back:
                AddMatterAct.this.finish();
                break;
            case R.id.matter_save:
                postData();
                break;
        }
    }

    /**
     * 显示日历
     *
     * @param textView
     */
    private void showDate(final TextView textView) {

        final int cur_year = Integer.parseInt(yearStr);
        final int cur_month = Integer.parseInt(monthStr) - 1;
        final int cur_day = Integer.parseInt(dayStr);
        mDateDialog = new CustomDatePickerDialog(mContext,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker arg0, int arg1,
                                          int arg2, int arg3) {
                        int year = arg1;
                        int month = (arg2 + 1);
                        int day = arg3;

                        String months = "";
                        if (month < 10) {
                            months = "0" + month;
                        } else {
                            months = month + "";
                        }

                        String days = "";
                        if (day < 10) {
                            days = "0" + day;
                        } else {
                            days = day + "";
                        }
                        Log.i("Addmater", "选择时间" + (cur_month+1) + "/" + cur_day + "当前时间" + arg2 + "/" + arg3);

                        if (textView.equals(startTime)) {
                            start_date = year + "/" + months + "/" + days;

                            if (arg1 <cur_year){
                                Toast.makeText(mContext, "开始日期不能小于选择的日期", Toast.LENGTH_LONG).show();
                                return;
                            }
                            if (arg2 < cur_month){
                                Toast.makeText(mContext, "开始日期不能小于选择的日期", Toast.LENGTH_LONG).show();
                                return;
                            }
                            if (arg2 <= cur_month){
                                if (arg3 < cur_day){
                                    Toast.makeText(mContext, "开始日期不能小于选择的日期", Toast.LENGTH_LONG).show();
                                    return;
                                }
                            }


                        }

                        start_year = arg1;
                        start_month = arg2;
                        start_day = arg3;
                        showTime(textView, false);
                    }
                }, cur_year, cur_month, cur_day);
        mDateDialog.show();

    }


    /**
     * 显示结束时间
     * @param textView
     */
    private void showEndDate(final TextView textView) {

        mDateDialog = new CustomDatePickerDialog(mContext,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker arg0, int arg1,
                                          int arg2, int arg3) {
                        int year = arg1;
                        int month = (arg2 + 1);
                        int day = arg3;

                        String months = "";
                        if (month < 10) {
                            months = "0" + month;
                        } else {
                            months = month + "";
                        }

                        String days = "";
                        if (day < 10) {
                            days = "0" + day;
                        } else {
                            days = day + "";
                        }
                        Log.i("Addmater", "选择时间" + (start_month+1) + "/" + start_day + "当前时间" + arg2 + "/" + arg3);

                        if (textView.equals(endTime)) {
                            end_date = year + "/" + months + "/" + days;

                            if (arg1 <start_year){
                                Toast.makeText(mContext, "开始日期不能小于选择的日期", Toast.LENGTH_LONG).show();
                                return;
                            }
                            if (arg2 < start_month){
                                Toast.makeText(mContext, "开始日期不能小于选择的日期", Toast.LENGTH_LONG).show();
                                return;
                            }
                            if (arg2 <= start_month){
                                if (arg3 < start_day){
                                    Toast.makeText(mContext, "开始日期不能小于选择的日期", Toast.LENGTH_LONG).show();
                                    return;
                                }
                            }
                        }
                        end_year = arg1;
                        end_month = arg2;
                        end_day = arg3;

                        showTime(textView, true);
                    }
                }, start_year, start_month, start_day);
        mDateDialog.show();

    }

    /**
     * 显示时间
     *
     * @param textView
     */
    private void showTime(final TextView textView, boolean isEnd) {
        final Calendar cal = Calendar.getInstance();

        if (isEnd){
            hour = start_hour;
            minute = start_minute + 1;//结束时间比开始时间多一分钟
        }else {
            hour =  cal.get(Calendar.HOUR_OF_DAY);
            minute = cal.get(Calendar.MINUTE);
        }


        mTimeDialog = new CustomTimePickerDialog(mContext,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int curMinute) {
                        int hour = hourOfDay;
                        int min = curMinute;

                        String hours = "";
                        if (hour < 10) {
                            hours = "0" + hour;
                        } else {
                            hours = hour + "";
                        }

                        String mins = "";
                        if (min < 10) {
                            mins = "0" + min;
                        } else {
                            mins = min + "";
                        }
                        //开始时间
                        if (textView.equals(startTime)) {
                            if (hourOfDay < hour){
                                Toast.makeText(mContext, "时间不能小于当前时间", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            if (curMinute < minute){
                                Toast.makeText(mContext, "时间不能小于当前时间", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            start_hour = hourOfDay;
                            start_minute = curMinute;
                            start_time = start_date + " " + hours + ":" + mins + ":" + "00 ";
                            textView.setText(start_time);
                        }
                        //结束时间
                        if (textView.equals(endTime)) {

                            /*if (hourOfDay < start_hour){
                                Toast.makeText(mContext, "时间不能小于当前时间", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            if (curMinute < start_minute){
                                Toast.makeText(mContext, "时间不能小于当前时间", Toast.LENGTH_SHORT).show();
                                return;
                            }*/

                            end_hour = hourOfDay;
                            end_minute = curMinute;
                            if (end_date.equals(start_date)) {
                                if (end_hour < start_hour || end_hour == start_hour && end_minute <= start_minute) {
                                    Toast.makeText(mContext, "结束时间不能小于开始时间", Toast.LENGTH_LONG).show();
                                    return;
                                }
                            }
                            end_time = end_date + " " + hours + ":" + mins + ":" + "00 ";
                            textView.setText(end_time);
                        }

                    }
                }, hour, minute, true);
        mTimeDialog.show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 0:
                Bundle b = data.getExtras(); //data为B中回传的Intent
                alertStr = b.getString("time");//str即为回传的值
                if (!alertStr.equals("")) {
                    if (alertStr.equals("-1")) {
                        alertTime.setText("无");
                        return;
                    }
                    alertTime.setText(alertStr);
                }
                break;
            default:
                break;
        }

    }


    //添加日历账户
    private void initCalendars() {

        TimeZone timeZone = TimeZone.getDefault();
        ContentValues value = new ContentValues();
        value.put(CalendarContract.Calendars.NAME, "yy");

        value.put(CalendarContract.Calendars.ACCOUNT_NAME, "mygmailaddress@gmail.com");
        value.put(CalendarContract.Calendars.ACCOUNT_TYPE, "com.android.exchange");
        value.put(CalendarContract.Calendars.CALENDAR_DISPLAY_NAME, "myself");
        value.put(CalendarContract.Calendars.VISIBLE, 1);
        value.put(CalendarContract.Calendars.CALENDAR_COLOR, -9206951);
        value.put(CalendarContract.Calendars.CALENDAR_ACCESS_LEVEL, CalendarContract.Calendars.CAL_ACCESS_OWNER);
        value.put(CalendarContract.Calendars.SYNC_EVENTS, 1);
        value.put(CalendarContract.Calendars.CALENDAR_TIME_ZONE, timeZone.getID());
        value.put(CalendarContract.Calendars.OWNER_ACCOUNT, "mygmailaddress@gmail.com");
        value.put(CalendarContract.Calendars.CAN_ORGANIZER_RESPOND, 0);

        Uri calendarUri = CalendarContract.Calendars.CONTENT_URI;
        calendarUri = calendarUri.buildUpon()
                .appendQueryParameter(CalendarContract.CALLER_IS_SYNCADAPTER, "true")
                .appendQueryParameter(CalendarContract.Calendars.ACCOUNT_NAME, "mygmailaddress@gmail.com")
                .appendQueryParameter(CalendarContract.Calendars.ACCOUNT_TYPE, "com.android.exchange")
                .build();

        getContentResolver().insert(calendarUri, value);
    }

    /**
     * 添加日历事件
     *
     * @param title
     * @param content
     * @param address
     * @param alert
     * @param startTime
     * @param endTime
     */
    public void addEvent(String title, String content, String address, int alert, long startTime, long endTime) {
        String calId = "";
        Cursor userCursor = getContentResolver().query(Uri.parse("content://com.android.calendar/calendars"), null, null, null, null);
        if (userCursor.getCount() > 0) {
            userCursor.moveToLast();  //注意：是向最后一个账户添加，开发者可以根据需要改变添加事件 的账户
            calId = userCursor.getString(userCursor.getColumnIndex("_id"));
        } else {
            Toast.makeText(this, "没有账户，请先添加账户", Toast.LENGTH_LONG).show();
            return;
        }

        ContentValues event = new ContentValues();
        event.put("title", title);
        event.put("description", content);
        // 插入账户
        event.put("calendar_id", calId);
        System.out.println("calId: " + calId);
        event.put("eventLocation", address);

        event.put("dtstart", startTime);
        event.put("dtend", endTime);
        event.put("hasAlarm", 1);
        event.put("hasAttendeeData", 1);//我添加的
        event.put("eventStatus", 1);


        event.put(CalendarContract.Events.EVENT_TIMEZONE, "Asia/Shanghai");  //这个是时区，必须有，
        //添加事件
        Uri newEvent = getContentResolver().insert(Uri.parse("content://com.android.calendar/events"), event);
        //事件提醒的设定
        long id = Long.parseLong(newEvent.getLastPathSegment());
        ContentValues values = new ContentValues();

        values.put("method", 1);
        values.put("event_id", id);
        // 提前10分钟有提醒
        if (alert == -1) {
            values.put("minutes", "无提醒");
        } else {
            values.put("minutes", alert);
        }

        getContentResolver().insert(Uri.parse("content://com.android.calendar/reminders"), values);
    }

}
