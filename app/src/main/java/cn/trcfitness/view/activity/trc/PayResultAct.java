package cn.trcfitness.view.activity.trc;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.Observable;
import java.util.Observer;

import cn.trcfitness.R;
import cn.trcfitness.application.OverallApplication;
import cn.trcfitness.controller.UserController;
import cn.trcfitness.model.ResultBalanceParam;
import cn.trcfitness.model.UserData;
import cn.trcfitness.model.UserDetail;
import cn.trcfitness.utils.NumberUtils;
import cn.trcfitness.view.fragmet.TRCFra;

/**
 * 充值后返回结果
 */
public class PayResultAct extends Activity {

    private TextView rechargeMoneyText, allMoneyText;

    //充值数
    private String rechargeNum = "";
    private UserDetail userDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_pay_result);

        rechargeNum = getIntent().getStringExtra("recharge");

        UserData userData = OverallApplication.userData;
        if (userData != null) {
            userDetail = userData.getData().getUser();

        }

        rechargeMoneyText = (TextView)findViewById(R.id.recharge_money);
        allMoneyText = (TextView)findViewById(R.id.all_money);

        if (!rechargeNum.equals("")){
            rechargeMoneyText.setText(rechargeNum);
        }
        if (userDetail != null){
            getMoney();
        }

        findViewById(R.id.recharge_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PayResultAct.this.finish();
            }
        });
    }

    private void getMoney(){
        UserController.get_Balance(new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                if (data != null && (ResultBalanceParam) data != null) {
                    ResultBalanceParam resultBalanceParam = (ResultBalanceParam) data;
                    if (resultBalanceParam.getStatus().equals("200")) {
                        DecimalFormat fnum = new DecimalFormat("##0.00");
                        String dd=fnum.format(resultBalanceParam.getData());
                        allMoneyText.setText(dd);
                    }
                }
            }
        });
    }

}
