package cn.trcfitness.view.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Observable;
import java.util.Observer;

import cn.trcfitness.R;
import cn.trcfitness.controller.UserController;
import cn.trcfitness.custom.CustomDatePickerDialog;
import cn.trcfitness.custom.CustomTimePickerDialog;
import cn.trcfitness.http.model.ResultParam;
import cn.trcfitness.model.OrdersBean;
import cn.trcfitness.model.RequestoApplyAppointmentParam;

/**
 * Created by ziv on 2016/7/7.
 * 预约课程
 */
public class ApplyAppointmentActivity extends Activity implements View.OnClickListener {
    private OrdersBean ordersBean;//商品详情
    private TextView consumer_name, price, product_name, info, consumer_address, order_number, service_number;
    private ImageView cover;
    private EditText et_number, et_time0, et_time1, et_remark;
    private LinearLayout linlay_back, linlay_send;

    private String sDate = null;
    private String eDate = null;

    //日历控件
    private CustomDatePickerDialog mDateDialog;
    //时间控件
    private CustomTimePickerDialog mTimeDialog;
    //当前年月日
    private int yearStr, monthStr, dayStr;
    //开始年月日时分
    private int startyear = -1, startmonth = -1, startday = -1, starthour = -1, startminute = -1;

    //当前时间
    private Calendar cal = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_applyappointment);
        initView();
        initData();
    }

    private void initData() {
        Bundle bundle = getIntent().getExtras();
        ordersBean = (OrdersBean) bundle.getSerializable("OrdersBean");
        consumer_name.setText(ordersBean.getConsumer_name());
        DecimalFormat fnum = new DecimalFormat("##0.00");
        String dd = fnum.format(ordersBean.getPrice());
        price.setText(dd + "元");
        product_name.setText(ordersBean.getProduct().getName());
        info.setText(ordersBean.getProduct().getInfo());
        consumer_address.setText(ordersBean.getConsumer_address());
            service_number.setText(ordersBean.getService_number() + "");
        order_number.setText(ordersBean.getOrder_number() + "");
        String ImgUrl = ordersBean.getProduct().getPcs().get(0).getCover();
        if (ImgUrl != null && ImgUrl.length() > 0) {
            Picasso.with(ApplyAppointmentActivity.this).load(ImgUrl).into(cover);
        }
    }


    private void initView() {
        consumer_name = (TextView) findViewById(R.id.tv_applyappointment_name);
        price = (TextView) findViewById(R.id.tv_applyappointment_number_price);
        product_name = (TextView) findViewById(R.id.tv_applyappointment_product_name);
        info = (TextView) findViewById(R.id.tv_applyappointment_number_info);
        consumer_address = (TextView) findViewById(R.id.tv_applyappointment_number_consumer_address);
        order_number = (TextView) findViewById(R.id.tv_applyappointment_order_number);
        service_number = (TextView) findViewById(R.id.tv_applyappointment_number_service_number);
        cover = (ImageView) findViewById(R.id.im_applyappointment_cover);
        et_number = (EditText) findViewById(R.id.et_applyappointment_number);
        et_time0 = (EditText) findViewById(R.id.et_applyappointment_time0);//开始时间
        et_time1 = (EditText) findViewById(R.id.et_applyappointment_time1);//结束时间
        et_remark = (EditText) findViewById(R.id.et_applyappointment_remark);//备注
        linlay_back = (LinearLayout) findViewById(R.id.linlay_activity_applyappointment_back);
        linlay_send = (LinearLayout) findViewById(R.id.linlay_activity_applyappointment_send);
        linlay_back.setOnClickListener(this);
        linlay_send.setOnClickListener(this);
        et_time0.setOnClickListener(this);
        et_time1.setOnClickListener(this);

    }

    /**
     * 显示日历
     */
    private void showDate(final int state) {
        if (state == 1 && startyear == -1) {
            mToast("请先选择开始时间");
            return;
        }
        cal = Calendar.getInstance();
        yearStr = cal.get(Calendar.YEAR);
        monthStr = cal.get(Calendar.MONTH);
        dayStr = cal.get(Calendar.DAY_OF_MONTH);
        mDateDialog = new CustomDatePickerDialog(ApplyAppointmentActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker arg0, int arg1,
                                          int arg2, int arg3) {
                        String s = arg1 + "-" + arg2 + "-" + arg3 + " 00:00:00";//当前时间
                        if (state == 1) {
                            if (!CompareTime(startyear + "-" + startmonth + "-" + startday + " 00:00:00", s)) {//已选择开始时间与选择结束时间时间差
                                mToast("结束时间在开始时间之前");
                                return;
                            }
                        } else if (!CompareTime(yearStr + "-" + monthStr + "-" + dayStr + " 00:00:00", s)) {//系统时间与当前时间差
                            mToast("请选择有效时间");
                            return;
                        }
                        showTime(state, arg1, arg2, arg3);
                    }
                }, yearStr, monthStr, dayStr);
        mDateDialog.show();
    }

    /**
     * 显示时间
     *
     * @param arg1 年
     * @param arg2 月
     * @param arg3 日
     */
    private void showTime(final int state, final int arg1, final int arg2, final int arg3) {
        cal = Calendar.getInstance();
        final int hour1 = cal.get(Calendar.HOUR_OF_DAY);
        final int minute1 = cal.get(Calendar.MINUTE);
        yearStr = cal.get(Calendar.YEAR);
        monthStr = cal.get(Calendar.MONTH);
        dayStr = cal.get(Calendar.DAY_OF_MONTH);
        mTimeDialog = new CustomTimePickerDialog(ApplyAppointmentActivity.this,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        //小时
                        String hours = "";
                        if (hourOfDay < 10) {
                            hours = "0" + hourOfDay;
                        } else {
                            hours = hourOfDay + "";
                        }
                        //分钟
                        String mins = "";
                        if (minute < 10) {
                            mins = "0" + minute;
                        } else {
                            mins = minute + "";
                        }

                        int month = (arg2 + 1);
                        int day = arg3;
                        //月
                        String months = "";
                        if (month < 10) {
                            months = "0" + month;
                        } else {
                            months = month + "";
                        }
                        //日
                        String days = "";
                        if (day < 10) {
                            days = "0" + day;
                        } else {
                            days = day + "";
                        }

                        String s = arg1 + "-" + arg2 + "-" + arg3 + " " + hourOfDay + ":" + minute + ":00";//当前选择时间
                        if (state == 1) {
                            if (!CompareTime(startyear + "-" + startmonth + "-" + startday + " " + starthour + ":" + startminute + ":00", s)) {//已选择开始时间与选择结束时间时间差
                                mToast("请选择有效时间");
                                return;
                            } else {
                                eDate = arg1 + "-" + months + "-" + days + " " + hours + ":" + mins + ":00";
                                et_time1.setText(arg1 + "年" + months + "月" + days + "日 " + hours + ":" + mins);
                                return;
                            }
                        } else if (!CompareTime(yearStr + "-" + monthStr + "-" + dayStr + " " + hour1 + ":" + minute1 + ":00", s)) {//系统时间与当前选择时间
                            mToast("请选择有效时间");
                            return;
                        } else {
                            startyear = arg1;
                            startmonth = arg2;
                            startday = arg3;
                            starthour = hourOfDay;
                            startminute = minute;
                            sDate = arg1 + "-" + months + "-" + days + " " + hours + ":" + mins + ":00";
                            et_time0.setText(arg1 + "年" + months + "月" + days + "日 " + hours + ":" + mins);
                        }
                    }
                }, hour1, minute1, true);
        mTimeDialog.show();
    }

    private void send() {
        if (et_time0.getText().toString().trim().equals("") && et_time0.getText().toString().length() == 0) {
            mToast("请选择开始时间");
            return;
        }

        if (et_time1.getText().toString().trim().equals("") && et_time1.getText().toString().length() == 0) {
            mToast("请选择结束时间");
            return;
        }

        if (et_remark.getText().toString().trim().equals("") && et_remark.getText().toString().length() == 0) {
            mToast("请填写备注");
            return;
        }
        RequestoApplyAppointmentParam requestParam = new RequestoApplyAppointmentParam();
        requestParam.setOid(ordersBean.getId());
        requestParam.setSDate(sDate);
        requestParam.setEDate(eDate);
        requestParam.setRemark(et_remark.getText().toString());

        UserController.applyAppointment(new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                if (data != null && (ResultParam) data != null) {
                    ResultParam resultParam = (ResultParam) data;
                    if (resultParam.getStatus().equals("200")) {
                        mToast("预约请求上传成功");
                        ApplyAppointmentActivity.this.finish();
                    }
                }
            }
        }, requestParam);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.linlay_activity_applyappointment_back:
                finish();
                break;
            case R.id.et_applyappointment_time0:
                showDate(0);
                break;
            case R.id.et_applyappointment_time1:
                showDate(1);
                break;
            case R.id.linlay_activity_applyappointment_send:
                send();
                break;
        }
    }


    private void mToast(String s) {
        Toast.makeText(ApplyAppointmentActivity.this, s, Toast.LENGTH_LONG).show();
    }

    /**
     * 比较两个时间大小
     *
     * @param s1 开始时间
     * @param s2 结束时间
     * @return
     */
    private Boolean CompareTime(String s1, String s2) {
//        String s1 = "2008-01-25 09:12:09";
//        String s2 = "2008-01-29 09:12:11";
        java.text.DateFormat df = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        java.util.Calendar c1 = java.util.Calendar.getInstance();
        java.util.Calendar c2 = java.util.Calendar.getInstance();

        try {
            c1.setTime(df.parse(s1));
            c2.setTime(df.parse(s2));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        int result = c1.compareTo(c2);
        if (result == 0) {
            //c1相等c2
            return true;
        } else if (result < 0) {
            //c1小于c2
            return true;
        } else {
            return false;
            //c1大于c2
        }
    }
}
