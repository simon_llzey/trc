package cn.trcfitness.view.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import cn.trcfitness.R;
import cn.trcfitness.controller.UserController;
import cn.trcfitness.http.model.ResultParam;
import cn.trcfitness.model.AddCalendarEvent;
import cn.trcfitness.model.CalendarEvent;
import cn.trcfitness.model.CalendarResultParam;
import cn.trcfitness.model.UserRequestParam;
import cn.trcfitness.view.adapter.CalendarDetailAdapter;

/**
 * 日历详细界面
 */
public class CalendarDetailAct extends Activity implements View.OnClickListener {
    private Context mContext = this;
    private PullToRefreshListView mListView;
    private CalendarDetailAdapter adapter;
    private TextView mEdit, mAdd;

    private LinearLayout goRunLin;
    private TextView goRun;

    //判断是否选择编辑
    private boolean isShare = false;
    //选中的那一天
    private String currrent_time = "";
    //日历所有事件
    private AddCalendarEvent calendarEvent;
    private List<CalendarEvent> eventList;

    public static CalendarDetailAct instance;

    private UserRequestParam requestParam;//分享上传参数

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case 0:
                    adapter.notifyDataSetChanged();
                    if (eventList != null && eventList.size() > 0)
                        goRunLin.setVisibility(View.GONE);
                    break;
            }
            if (mListView != null)
                mListView.onRefreshComplete();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_calendar_detail);
        instance = this;
        initView();
    }

    private void initView() {

        currrent_time = getIntent().getStringExtra("date");
        Log.i("Calenday", "选中的时间" + currrent_time);
        mEdit = (TextView) findViewById(R.id.detail_calendar_edit);
        mAdd = (TextView) findViewById(R.id.detail_calendar_add);

        goRunLin = (LinearLayout) findViewById(R.id.no_event_lin);
        goRun = (TextView) findViewById(R.id.btn_go_run);

        calendarEvent = new AddCalendarEvent();
        calendarEvent.setSpecific_time(currrent_time);

        eventList = new ArrayList<CalendarEvent>();

        mListView = (PullToRefreshListView) findViewById(R.id.detail_calendar_list);
        mListView.setMode(PullToRefreshBase.Mode.PULL_FROM_START);

        adapter = new CalendarDetailAdapter(mContext, eventList);
        mListView.setAdapter(adapter);

        mEdit.setOnClickListener(this);
        mAdd.setOnClickListener(this);
        goRun.setOnClickListener(this);
        findViewById(R.id.detail_back).setOnClickListener(this);

        mListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
            @Override
            public void onRefresh(PullToRefreshBase<ListView> refreshView) {
                getData();
            }
        });


        getData();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.detail_calendar_edit:
                if (!isShare) {
                    adapter.isEdit = true;
                    adapter.notifyDataSetChanged();
                    isShare = true;
                    mEdit.setText("取消");
                } else {
                    adapter.isEdit = false;
                    adapter.notifyDataSetChanged();
                    isShare = false;
                    mEdit.setText("分享");
                }

                break;

            case R.id.detail_calendar_add:
                Intent addIntent = new Intent(mContext, AddMatterAct.class);
                addIntent.putExtra("time", currrent_time);
                startActivity(addIntent);
                break;

            case R.id.detail_back:
                CalendarDetailAct.this.finish();
                break;

            case R.id.btn_go_run:
                Intent intent = new Intent(mContext, AddMatterAct.class);
                intent.putExtra("time", currrent_time);
                startActivity(intent);
                break;
        }
    }


    /**
     * 获取当月事件
     */
    public void getData() {

        eventList.clear();
        Log.i("Calendar", "当前日期" + currrent_time);

        UserController.get_calendar_event(calendarEvent, new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                CalendarResultParam resultParam = (CalendarResultParam) data;
                if (resultParam != null) {
                    List<CalendarEvent> list = resultParam.getData().getCalendarEvents();
                    if (list != null && list.size() > 0) {
                        for (CalendarEvent event : list) {
                            if (!event.isDeleted()) {
                                eventList.add(event);
                            }
                        }
                        Collections.reverse(eventList);
                        mHandler.sendEmptyMessage(0);
                    }
                } else {
                    Toast.makeText(mContext, "没有数据", Toast.LENGTH_LONG).show();
                }


            }
        });

    }


    /**
     * @param pid 分享日历id
     *            好友分享日历
     */
    public void Share(int pid) {
        requestParam = new UserRequestParam();
        Intent intent = new Intent(mContext, SelectFriendActivity.class);
        startActivityForResult(intent, 0);
        requestParam.setPid(pid);

    }

    private void SendShare() {
        UserController.Share(new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                if (data != null && (ResultParam) data != null) {
                    ResultParam resultParam = (ResultParam) data;
                    if (resultParam.getStatus().equals("200")) {
                        Toast.makeText(mContext, "日历分享成功", Toast.LENGTH_LONG).show();
                    }
                }

            }
        }, requestParam);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            int uid = data.getIntExtra("uid", -1);
            if (uid != -1) {
                requestParam.setUid(uid);
                SendShare();
            } else {
                Toast.makeText(mContext, "UID错误", Toast.LENGTH_LONG).show();
            }
        }
    }
}
