package cn.trcfitness.view.activity.trc;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Observable;
import java.util.Observer;

import cn.trcfitness.R;
import cn.trcfitness.controller.UserController;
import cn.trcfitness.model.UserDetail;
import cn.trcfitness.model.UserResultParam;

/**
 * 修改昵称和姓名
 */
public class NickNameAct extends Activity {

    private Context mContext = this;

    private EditText mEdit;
    //记录是哪一个界面
    private String intentName;
    //设置名称
    private String setName = "";
    private UserDetail userDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_nick_name);

        mEdit = (EditText)findViewById(R.id.modification_edit);

        intentName = getIntent().getStringExtra("name");
        userDetail = new UserDetail();
        //如果进来的是姓名修改标题
        if (intentName.equals("name")){
            ((TextView)findViewById(R.id.name_title)).setText("我的姓名");
            ((TextView)findViewById(R.id.name)).setText("姓名");
        }

        findViewById(R.id.modification_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backResult();
            }
        });


        findViewById(R.id.modification_finish).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //设置姓名
                setName = mEdit.getText().toString();
                if (intentName.equals("name")){
                    if (setName!= null && !setName.equals("")){
                        userDetail.setName(setName);
                        setInfo(userDetail);
                    }else {
                        Toast.makeText(mContext, "请先输入名称", Toast.LENGTH_LONG).show();
                    }
                }else {
                    //设置昵称
                    if (setName!= null && !setName.equals("")){
                        userDetail.setNick_name(setName);
                        setInfo(userDetail);
                    }else {
                        Toast.makeText(mContext, "请先输入名称", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
        findViewById(R.id.modification_delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEdit.setText("");
            }
        });
    }


    /**
     * 设置用户信息
     * @param detail
     */
    private void setInfo(UserDetail detail){
        UserController.set_user_info(detail, new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                UserResultParam resultParam = (UserResultParam) data;
                if (resultParam != null) {
                    Log.i("Nick", "设置用户名" + resultParam.toString());
                    if (resultParam.isStatus()) {
                        Toast.makeText(mContext, "设置成功", Toast.LENGTH_LONG).show();
                        if (intentName.equals("name")) {
                            Intent nameIntent = new Intent();
                            nameIntent.putExtra("name", userDetail.getName());
                            setResult(3, nameIntent);
                        } else {
                            Intent nickIntent = new Intent();
                            nickIntent.putExtra("name", userDetail.getNick_name());
                            setResult(2, nickIntent);
                        }
                    } else {
                        Toast.makeText(mContext, "设置失败", Toast.LENGTH_LONG).show();
                    }

                } else {
                    Toast.makeText(mContext, "设置失败", Toast.LENGTH_LONG).show();
                }

                NickNameAct.this.finish();
            }
        });
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            backResult();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


    /**
     * 返回时传值，不然报错
     */
    private void backResult(){
        if (intentName.equals("name")) {
            Intent nameIntent = new Intent();
            nameIntent.putExtra("name", "");
            setResult(3, nameIntent);
        } else {
            Intent nickIntent = new Intent();
            nickIntent.putExtra("name", "");
            setResult(2, nickIntent);
        }
        NickNameAct.this.finish();
    }


}
