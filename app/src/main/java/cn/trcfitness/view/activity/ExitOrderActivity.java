package cn.trcfitness.view.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import cn.trcfitness.R;
import cn.trcfitness.controller.UserController;
import cn.trcfitness.model.OrdersBean;
import cn.trcfitness.model.RequestOrders;
import cn.trcfitness.model.ResultMyOrderPaam;
import cn.trcfitness.view.adapter.MyOrderAdapter;

/**
 * Created by ziv on 2016-8-30.
 */

public class ExitOrderActivity extends Activity implements View.OnClickListener {
    private LinearLayout linlay_back;

    private PullToRefreshListView listview;
    private List<OrdersBean> mlist;
    private MyOrderAdapter adapter;
    private RequestOrders requestOrders;

    private int pageOffset = 0;//页数
    private static final int REFRESH = 0;
    private static final int MORE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_exitorder);
        initView();
        requestOrders = new RequestOrders();
        requestOrders.setPageSize(20);
        requestOrders.setStatus(4);//退货
        getData(REFRESH);
    }

    private void initView() {
        mlist = new ArrayList<OrdersBean>();
        linlay_back = (LinearLayout) findViewById(R.id.linlay_activity_exitorder_back);
        listview = (PullToRefreshListView) findViewById(R.id.lv_exitmyorder);

        linlay_back.setOnClickListener(this);

        adapter = new MyOrderAdapter(this, mlist, "1");
        listview.setAdapter(adapter);
        listview.setMode(PullToRefreshBase.Mode.BOTH);
        listview.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            //下拉刷新
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                pageOffset = 0;
                requestOrders.setPageOffset(pageOffset);
                getData(REFRESH);
            }

            //上拉加载更多
            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                if (mlist != null && mlist.size() > 0) {
                    pageOffset = mlist.size();
                }
                requestOrders.setPageOffset(pageOffset);
                getData(MORE);
            }
        });
    }

    private void getData(final int state) {
        UserController.get_Orders(new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                if (data != null && (ResultMyOrderPaam) data != null) {
                    ResultMyOrderPaam resultMyOrderPaam = (ResultMyOrderPaam) data;
                    if (state == REFRESH) {
                        mlist.clear();
                        mlist = resultMyOrderPaam.getData().getOrders();
                        RefreshList();
                    } else if (state == MORE) {
                        mlist.addAll(resultMyOrderPaam.getData().getOrders());
                        RefreshList();
                        if (resultMyOrderPaam.getData().getOrders().size() == 0) {
                            Toast.makeText(ExitOrderActivity.this, "没有更多数据", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        }, requestOrders);
    }

    private void RefreshList() {
        adapter.setList(mlist);
        listview.onRefreshComplete();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.linlay_activity_exitorder_back:
                finish();
                break;
        }

    }
}
