package cn.trcfitness.view.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import java.util.ArrayList;
import java.util.List;

import cn.trcfitness.R;
import cn.trcfitness.view.adapter.MyOrderPageAdapter;
import cn.trcfitness.view.fragmet.MyOrderFra;

/**
 * Created by ziv on 2016/5/17.
 * 我的订单
 */
public class MyOrderActivity extends FragmentActivity implements ViewPager.OnPageChangeListener, View.OnClickListener {
    private ViewPager mViewPager;
    private CheckedTextView[] mTabs;
    private ImageView imageView;// 页卡标题动画图片
    private LinearLayout Linlay_break;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_myorder);

        Intent intent = getIntent();
        int type = intent.getIntExtra("type", 100);

        initView();
        initTabs(type);
        switchTab(type);
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    private void initTabs(final int type) {
        int tabID[] = {R.id.cktv_activity_myorder_0, R.id.cktv_activity_myorder_2, R.id.cktv_activity_myorder_3, R.id.cktv_activity_myorder_4};
        mTabs = new CheckedTextView[tabID.length];
        for (int i = 0; i < tabID.length; i++) {
            mTabs[i] = (CheckedTextView) findViewById(tabID[i]);
            mTabs[i].setOnClickListener(this);
        }

        //创建出实例后马上获取宽度
        mTabs[type].post(new Runnable() {
            @Override
            public void run() {
                //初始化游标宽度
                int width = mTabs[type].getWidth();
                LayoutParams laParams = (LayoutParams) imageView.getLayoutParams();
                laParams.width = width;
                imageView.setLayoutParams(laParams);

                //初始化游标位置
                int[] location = new int[2];//location [0]--->x坐标,location [1]--->y坐标
                mTabs[type].getLocationOnScreen(location);//获取在整个屏幕内的绝对坐标
                imgIndex = location[0];
                switchTab(type);
            }
        });
    }

    private void initView() {
        imageView = (ImageView) findViewById(R.id.cursor);
        mViewPager = (ViewPager) findViewById(R.id.vp_myorder);
        Linlay_break= (LinearLayout) findViewById(R.id.Linlay_activity_myorder_break);
        Linlay_break.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        List<android.support.v4.app.Fragment> fragments = new ArrayList<android.support.v4.app.Fragment>();

        MyOrderFra myOrderFra0 = new MyOrderFra();
        myOrderFra0.setType("0");
        fragments.add(myOrderFra0);

        MyOrderFra myOrderFra1 = new MyOrderFra();
        myOrderFra1.setType("1");
        fragments.add(myOrderFra1);

        MyOrderFra myOrderFra2 = new MyOrderFra();
        myOrderFra2.setType("2");
        fragments.add(myOrderFra2);

        MyOrderFra myOrderFra3 = new MyOrderFra();
        myOrderFra3.setType("3");
        fragments.add(myOrderFra3);


        MyOrderPageAdapter adapter = new MyOrderPageAdapter(getSupportFragmentManager(), fragments);
        mViewPager.setAdapter(adapter);
        mViewPager.setOnPageChangeListener(this);//接口，需要实现三个方法
    }

    /**
     * 当前所在页面
     **/
    private int mCurTabIndex = 100;

    /**
     * @param v
     * 点击头部切换fragment
     */
    @Override
    public void onClick(View v) {
        if (v == mTabs[mCurTabIndex]) {
            return;
        }
        for (int i = 0; i < mTabs.length; i++) {
            if (v == mTabs[i]) {
                switchTab(i);
                return;
            }
        }

    }

    private int imgIndex = 0;//当前游标位置

    /**
     * @param i
     * 切换fragment
     */
    private void switchTab(int i) {
        mTabs[i].setTextColor(Color.parseColor("#d9b96b"));
        if (mCurTabIndex != 100) {
            mTabs[mCurTabIndex].setTextColor(Color.parseColor("#000000"));
            mTabs[mCurTabIndex].setChecked(false);
        }
        mViewPager.setCurrentItem(i, true);// 设置在哪个页面
        mTabs[i].setChecked(true);
        mCurTabIndex = i;


        //游标切换动画
        int[] location = new int[2];//location [0]--->x坐标,location [1]--->y坐标
//        mTabs[i].getLocationInWindow(location); //获取在当前窗口内的绝对坐标
        mTabs[i].getLocationOnScreen(location);//获取在整个屏幕内的绝对坐标

        Animation animation = new TranslateAnimation(imgIndex,
                location[0], 0, 0);
        animation.setFillAfter(true);/* True:图片停在动画结束位置 */
        animation.setDuration(300);
        imageView.startAnimation(animation);

        if (mTabs[i].getWidth() != imageView.getWidth()) {
            LayoutParams laParams = (LayoutParams) imageView.getLayoutParams();
            laParams.width = mTabs[i].getWidth();
            imageView.setLayoutParams(laParams);
        }
        imgIndex = location[0];

    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        switchTab(position);

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
