package cn.trcfitness.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Observable;
import java.util.Observer;

import cn.trcfitness.R;
import cn.trcfitness.application.OverallApplication;
import cn.trcfitness.controller.UserController;
import cn.trcfitness.custom.LoadDialog;
import cn.trcfitness.model.LoginParam;
import cn.trcfitness.model.UserData;
import cn.trcfitness.utils.SPUtils;
import cn.trcfitness.view.MainActivity;
import cn.trcfitness.view.fragmet.MainFragment;

/**
 * create by Eric
 * create date 2016/05/10
 * 登录
 */
public class LoginActivity extends Activity implements View.OnClickListener {
    private EditText etUsername;
    private EditText etPassword;
    private TextView tv_newuser, tv_changepw;
    private Button bt_login;
    private LoadDialog loadDialog;
    private String userName, passWord;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_login);
        initView();
        initdata();

    }

    private void initdata() {
        userName = (SPUtils.get(this, "username", "")).toString();
        passWord = (SPUtils.get(this, "password", "")).toString();
        etUsername.setText(userName);
        etPassword.setText(passWord);
        if (!(userName.equals("") || passWord.equals(""))) {
            LoginParam lp = new LoginParam();
            lp.setUsername(userName);
            lp.setPassword(passWord);
            loginClick(lp);
        }


    }


    /**
     * 初始化
     */
    private void initView() {
        etUsername = (EditText) findViewById(R.id.et_activity_login_phone);
        etPassword = (EditText) findViewById(R.id.et_activity_login_password);

        bt_login = (Button) findViewById(R.id.bt_activity_login_login);
        tv_changepw = (TextView) findViewById(R.id.tv_activity_login_changepw);
        tv_newuser = (TextView) findViewById(R.id.tv_activity_login_newuser);

        bt_login.setOnClickListener(this);
        tv_changepw.setOnClickListener(this);
        tv_newuser.setOnClickListener(this);

        loadDialog = new LoadDialog(this);
    }

    private void loginClick(LoginParam lp) {
        loadDialog.show();
        UserController.loginToMain(lp, new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                loadDialog.cancel();
                if (data != null && (UserData) data != null) {
                    //赋值用户信息
                    OverallApplication.userData = (UserData) data;
                    OverallApplication.exempt_money = ((UserData) data).getData().getUser().getExempt_money() + "";
                    if (OverallApplication.userData.getStatus().equals("200")) {
                        SPUtils.put(LoginActivity.this, "username", etUsername.getText().toString().trim());
                        SPUtils.put(LoginActivity.this, "password", etPassword.getText().toString().trim());
                        Intent intent = new Intent(getApplicationContext(), MainFragment.class);
                        OverallApplication.tourists = false;
                        startActivity(intent);
                        Toast.makeText(LoginActivity.this, "登录成功", Toast.LENGTH_SHORT).show();
                        MainActivity.instance.finish();
                        finish();
                    } else {
                        etPassword.setText("");
                    }
                } else {
                }
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_activity_login_login:
                String name = etUsername.getText().toString().trim();
                String password = etPassword.getText().toString().trim();
                if (name == null || name.equals("")) {
                    mToast("请输入用户名");
                    return;
                }
                if (password == null || password.equals("")) {
                    mToast("请输入密码");
                    return;
                }
                LoginParam lp = new LoginParam();
                lp.setUsername(name);
                lp.setPassword(password);
                loginClick(lp);
                break;

            case R.id.tv_activity_login_newuser:
                Intent intent1 = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent1);
                finish();
                break;

            case R.id.tv_activity_login_changepw:
                Intent intent3 = new Intent(LoginActivity.this, ChangePassWordActivity.class);
                startActivity(intent3);
                finish();
                break;
        }
    }

    private void mToast(String s) {
        Toast.makeText(LoginActivity.this, s, Toast.LENGTH_SHORT).show();
    }

}
