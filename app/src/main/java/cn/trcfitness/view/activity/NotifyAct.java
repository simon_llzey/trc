package cn.trcfitness.view.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;

import cn.trcfitness.R;

/**
 * 通知栏点击进来的界面
 */
public class NotifyAct extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_notify);
    }
}
