package cn.trcfitness.view.activity.trc.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import cn.trcfitness.R;
import cn.trcfitness.model.ResultBillParam;
import cn.trcfitness.utils.DateUtils;
import cn.trcfitness.utils.NumberUtils;

/**
 * Created by Administrator on 2016/5/13.
 */
public class BillAdapter extends BaseAdapter {
    private Context mContext;
    List<ResultBillParam.DataBean.ConsumeDetailsBean> list;

    public BillAdapter(Context context, List<ResultBillParam.DataBean.ConsumeDetailsBean> list) {
        this.mContext = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null){
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.bill_item_list, null);
            viewHolder.name= (TextView) convertView.findViewById(R.id.tv_bill_list_name);
            viewHolder.money= (TextView) convertView.findViewById(R.id.tv_bill_list_money);
            viewHolder.balance= (TextView) convertView.findViewById(R.id.tv_bill_list_balance);
            viewHolder.create_time= (TextView) convertView.findViewById(R.id.tv_bill_list_create_time);
            viewHolder.head = (ImageView) convertView.findViewById(R.id.tv_bill_list_head);


            convertView.setTag(viewHolder);
        }else {
            viewHolder = (ViewHolder)convertView.getTag();
        }

        viewHolder.name.setText(list.get(position).getName());
        viewHolder.money.setText(NumberUtils.getDecimalpoint2(list.get(position).getMoney()));
        viewHolder.balance.setText(NumberUtils.getDecimalpoint2(list.get(position).getBalance()));
        viewHolder.create_time.setText(DateUtils.date2(list.get(position).getCreate_time()));
        if (list.get(position).getType() == 0){
            viewHolder.head.setImageResource(R.drawable.gym);
        }else{
            viewHolder.head.setImageResource(R.drawable.spa);
        }

        return convertView;
    }

    private class ViewHolder{
        TextView name;
        TextView money;
        TextView balance;
        TextView create_time;
        ImageView head;
    }

    public void setList(List<ResultBillParam.DataBean.ConsumeDetailsBean> list) {
        this.list = list;
    }
}
