package cn.trcfitness.view.activity;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Observable;
import java.util.Observer;

import cn.trcfitness.R;
import cn.trcfitness.controller.UserController;
import cn.trcfitness.http.model.ResultParam;
import cn.trcfitness.model.CalendarEvent;
import cn.trcfitness.model.UserRequestParam;
import cn.trcfitness.utils.DateUtils;
import cn.trcfitness.view.fragmet.CalendarFra;

/**
 * 删除日历事件
 */
public class DeleteCalendarAct extends Activity {

    private EditText title, location, remark;
    private TextView alertTime, startTime, endTime;

    //日历事件
    private CalendarEvent event;

    //删除请求事件参数
    private UserRequestParam requestParam = new UserRequestParam();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_delete_calendar);
        event = (CalendarEvent)getIntent().getSerializableExtra("calendar_details");
        if (event != null){
            initView();
        }else {
            Toast.makeText(this, "没有数据", Toast.LENGTH_LONG).show();
        }

    }
    private void initView(){

        title = (EditText)findViewById(R.id.calendar_del_title);
        location = (EditText)findViewById(R.id.calendar_del_location);
        remark = (EditText)findViewById(R.id.calendar_del_remark);
        alertTime = (TextView)findViewById(R.id.calendar_del_alert_time);
        startTime = (TextView)findViewById(R.id.calendar_del_start_time);
        endTime = (TextView)findViewById(R.id.calendar_del_end_time);

        requestParam.setId(event.getId());
        Log.i("DelCalendar", "获取删除事件信息" + event.toString());

        title.setText(event.getTitle());
        location.setText(event.getLocation());

        if (event.getRemark() != null && !event.getRemark().equals("")){
            remark.setText(event.getRemark());
        }
        if (event.getRemind() != null && event.getRemind() > -1){
            alertTime.setText(event.getRemind()+"");
        }

        startTime.setText(DateUtils.date2(event.getBegin()));
        endTime.setText(DateUtils.date2(event.getEnd()));

        findViewById(R.id.calendar_del_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delEvent();
            }
        });
        findViewById(R.id.calendar_del_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DeleteCalendarAct.this.finish();
            }
        });
    }


    /**
     * 删除事件
     */
    private void delEvent(){
        UserController.del_calendar_event(requestParam, new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                ResultParam resultParam = (ResultParam)data;
                if (resultParam != null){
                    if (resultParam.isStatus()){
                        Toast.makeText(DeleteCalendarAct.this, "删除成功", Toast.LENGTH_LONG).show();
                        CalendarDetailAct.instance.getData();
                        CalendarFra.instance.refreshDate();
                    }else {
                        Toast.makeText(DeleteCalendarAct.this, "删除失败", Toast.LENGTH_LONG).show();
                    }
                }else {
                    Toast.makeText(DeleteCalendarAct.this, "删除失败", Toast.LENGTH_LONG).show();
                }
            DeleteCalendarAct.this.finish();
            }
        });
    }
}
