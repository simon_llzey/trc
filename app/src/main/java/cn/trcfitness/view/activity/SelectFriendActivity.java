package cn.trcfitness.view.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import cn.trcfitness.R;
import cn.trcfitness.controller.FriendController;
import cn.trcfitness.model.Frends;
import cn.trcfitness.model.FriendResultParam;
import cn.trcfitness.model.UserRequestParam;
import cn.trcfitness.view.adapter.FriendAdapter;

/**
 * Created by ziv on 2016/7/11.
 * 选择好友分享日历
 */
public class SelectFriendActivity extends Activity {
    private LinearLayout Linlay_back;
    private PullToRefreshListView mListView;
    private FriendAdapter adapter;
    private List<Frends> mList;
    private int pageOffset = 0;//页数
    private static final int REFRESH = 0;
    private static final int MORE = 1;
    private UserRequestParam userRequest;
    private Dialog dialog;
    private LinearLayout Linlay_no_content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_selectfriend);
        initView();
    }

    private void initView() {
        Linlay_back = (LinearLayout) findViewById(R.id.linlay_activity_selectfriend_back);
        mListView = (PullToRefreshListView) findViewById(R.id.lv_activity_selectfriend);
        Linlay_no_content = (LinearLayout) findViewById(R.id.Linlay_activity_selectfriend_no_content);
        mList = new ArrayList<Frends>();
        adapter = new FriendAdapter(SelectFriendActivity.this, mList, false);
        mListView.setAdapter(adapter);
        mListView.setMode(PullToRefreshBase.Mode.BOTH);
        mListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            //下拉刷新
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                pageOffset = 0;
                userRequest.setPageOffset(pageOffset);
                getFriendList(REFRESH);
            }

            //上拉加载更多
            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                if (mList != null && mList.size() > 0) {
                    pageOffset = mList.size();
                }
                userRequest.setPageOffset(pageOffset);
                getFriendList(MORE);
            }
        });

        userRequest = new UserRequestParam();
        userRequest.setPageSize(20);
        userRequest.setPageOffset(pageOffset);

        getFriendList(REFRESH);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
//                AlertDialog.Builder builder = new AlertDialog.Builder(SelectFriendActivity.this);
                View builderview = LayoutInflater.from(SelectFriendActivity.this).inflate(R.layout.dialog_confirm, null);
                builderview.findViewById(R.id.tv_dialog_cancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        backgroundAlpha(1f);
                    }
                });

                builderview.findViewById(R.id.tv_dialog_confirm).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //获取出当前选择好友的ID 返回上一个activity
                        Intent intent = new Intent();
                        int uid = mList.get(position - 1).getFid();
                        intent.putExtra("uid", uid);
                        SelectFriendActivity.this.setResult(RESULT_OK, intent);
                        SelectFriendActivity.this.finish();
                        dialog.dismiss();
                        backgroundAlpha(1f);
                    }
                });
//                builder.setView(builderview);
//                dialog = builder.show();
                dialog = new Dialog(SelectFriendActivity.this, R.style.selectorDialog);
                dialog.setContentView(builderview);
                dialog.show();
                backgroundAlpha(0.6f);
            }
        });

        Linlay_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    /**
     * 获取我的好友
     */
    private void getFriendList(final int state) {
        FriendController.getMyFriendList(new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                if (data != null && (FriendResultParam) data != null) {
                    FriendResultParam resultParam = (FriendResultParam) data;
                    if (state == REFRESH) {
                        mList.clear();
                        mList = resultParam.getData().getFrends();
                        RefreshFriend();
                    } else if (state == MORE) {
                        mList.addAll(resultParam.getData().getFrends());
                        RefreshFriend();
                        if (resultParam.getData().getFrends().size() == 0) {
                            mToast("没有更多数据");
                        }
                    }
                } else {
                    mListView.onRefreshComplete();
//                    mToast("更新失败");
                }
            }
        }, userRequest);
    }

    private void mToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }
    /**
     * 设置添加屏幕的背景透明度
     */
    public void backgroundAlpha(float bgAlpha) {
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.alpha = bgAlpha; //0.0-1.0
        getWindow().setAttributes(lp);
    }

    private void RefreshFriend() {
        if (mList != null && !mList.isEmpty()) {
            Linlay_no_content.setVisibility(View.GONE);
        } else {
            Linlay_no_content.setVisibility(View.VISIBLE);
        }
        adapter.setList(mList);
        mListView.onRefreshComplete();
        adapter.notifyDataSetChanged();
    }
}
