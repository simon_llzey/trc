package cn.trcfitness.view.activity.trc;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import cn.trcfitness.R;
import cn.trcfitness.controller.UserController;
import cn.trcfitness.custom.LoadDialog;
import cn.trcfitness.model.ResultAppointmentParam;
import cn.trcfitness.model.UserRequestParam;
import cn.trcfitness.view.activity.trc.adapter.ReservationAdapter;

/**
 * trc-预约管理
 */
public class ReservationManagerAct extends Activity {

    private Context mContext = this;
    private PullToRefreshListView mListView;
    private List<ResultAppointmentParam.DataBean.CesBean> datas;
    private ReservationAdapter adapter;
    private LinearLayout Linlay_no_content;
    private LoadDialog loadDialog;


    //分页参数
    private UserRequestParam params = new UserRequestParam();

    private Handler mHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case 0:
//                    if (datas != null && !datas.isEmpty()) {
//                        Linlay_no_content.setVisibility(View.GONE);
//                    } else {
//                        Linlay_no_content.setVisibility(View.VISIBLE);
//                    }
                    adapter.setList(datas);
                    adapter.notifyDataSetChanged();
                    break;
            }
            mListView.onRefreshComplete();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_reservation_manager);

        loadDialog = new LoadDialog(this);
        loadDialog.show();
        initView();
    }

    private void initView() {
        Linlay_no_content= (LinearLayout) findViewById(R.id.Linlay_activity_reservation_manager_no_content);
        mListView = (PullToRefreshListView) findViewById(R.id.reservation_list);
        mListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                getData(false);
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                getData(true);
            }
        });
        datas = new ArrayList<ResultAppointmentParam.DataBean.CesBean>();
        adapter = new ReservationAdapter(mContext, datas);
        mListView.setAdapter(adapter);

        getData(false);
        findViewById(R.id.reservation_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ReservationManagerAct.this.finish();
            }
        });
    }


    /**
     * 获取预约管理数据
     */
    public void getData(boolean isLoader){
        if (isLoader){
            int pageOffset = 0;
            if ((datas != null && datas.size() > 0)) {
                pageOffset += datas.size();
            }
            params.setPageOffset(pageOffset);
        }else {
            params.setPageOffset(0);
            //清空本地列表
            datas.clear();
        }

        UserController.get_reservation(params, new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                loadDialog.cancel();
                ResultAppointmentParam resultMyOrderPaam = (ResultAppointmentParam)data;
                if (resultMyOrderPaam != null){
                    List<ResultAppointmentParam.DataBean.CesBean> cesBeenList = resultMyOrderPaam.getData().getCes();
                    if (cesBeenList != null && cesBeenList.size() > 0){
                        datas = cesBeenList;
                    }else {
                    }
                }
                mHandler.sendEmptyMessage(0);
            }
        });
    }


}
