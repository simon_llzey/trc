package cn.trcfitness.view.activity.trc;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.Observable;
import java.util.Observer;

import cn.trcfitness.R;
import cn.trcfitness.application.OverallApplication;
import cn.trcfitness.controller.UserController;
import cn.trcfitness.model.ResultBalanceParam;
import cn.trcfitness.model.UserData;
import cn.trcfitness.model.UserDetail;
import cn.trcfitness.utils.NumberUtils;
import cn.trcfitness.view.activity.TimeBalanceActivity;
import cn.trcfitness.view.pay.PayKeyboardAct;

/**
 * trc-余额管理
 */
public class BalanceManageAct extends Activity implements View.OnClickListener {

    private Context mContext = this;
    private LinearLayout rechargeLin, setLin, numberLin, moneyLin, Linlay_timebalance;

    private TextView moneyNum;//剩余的金额

    private UserDetail userDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_balance_manage);

        UserData userData = OverallApplication.userData;
        if (userData != null) {
            userDetail = userData.getData().getUser();
            if (userDetail != null) {
                initView();
                initData();
            } else {
                Toast.makeText(mContext, "请重新登录", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(mContext, "请重新登录", Toast.LENGTH_LONG).show();
        }



    }

    private void initData() {
        UserController.get_Balance(new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                if (data != null && (ResultBalanceParam) data != null) {
                    ResultBalanceParam resultBalanceParam = (ResultBalanceParam) data;
                    if (resultBalanceParam.getStatus().equals("200")) {
                        DecimalFormat fnum = new DecimalFormat("##0.00");
                        String dd=fnum.format(resultBalanceParam.getData());
                        moneyNum.setText(dd);
                    }
                }
            }
        });
    }

    private void initView() {
        rechargeLin = (LinearLayout) findViewById(R.id.balance_recharge);
        setLin = (LinearLayout) findViewById(R.id.balance_set);
        numberLin = (LinearLayout) findViewById(R.id.balance_number);
        moneyLin = (LinearLayout) findViewById(R.id.balance_money);
        Linlay_timebalance = (LinearLayout) findViewById(R.id.Linlay_activity_balance_manage_timebalance);

        moneyNum = (TextView) findViewById(R.id.balance_money_number);
        if (userDetail.getReal_money() != null) {
            String dd = NumberUtils.getDecimalpoint2(userDetail.getReal_money());
            moneyNum.setText(dd);
        }

        rechargeLin.setOnClickListener(this);
        setLin.setOnClickListener(this);
        numberLin.setOnClickListener(this);
        Linlay_timebalance.setOnClickListener(this);
        findViewById(R.id.balance_back).setOnClickListener(this);
    }


    @Override
    protected void onResume() {
        super.onResume();
        UserData userData = OverallApplication.userData;
        if (userData != null) {
            userDetail = userData.getData().getUser();
            if (userDetail != null) {
                initData();
            } else {
                Toast.makeText(mContext, "请重新登录", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(mContext, "请重新登录", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.balance_recharge:
                Intent rechargeIntent = new Intent(mContext, RechargeMoneyAct.class);
                startActivity(rechargeIntent);
                break;

            case R.id.balance_set:
                Intent setIntent = new Intent(mContext, SetActvity.class);
                startActivity(setIntent);
                break;

            case R.id.balance_number:
                Intent numberIntent = new Intent(mContext, NumberManagerAct.class);
                startActivity(numberIntent);
                break;

            case R.id.balance_back:
                BalanceManageAct.this.finish();
                break;

            case R.id.Linlay_activity_balance_manage_timebalance:
                Intent intent = new Intent(BalanceManageAct.this, TimeBalanceActivity.class);
                startActivity(intent);
                break;
        }
    }
}
