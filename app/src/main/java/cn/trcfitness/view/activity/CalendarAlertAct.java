package cn.trcfitness.view.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import cn.trcfitness.R;

/**
 * 新建事件-提醒
 */
public class   CalendarAlertAct extends Activity {

    private Context mContext = this;

    private ListView mListView;
    private String[] times = {"事件发生时", "5分钟前", "15分钟前", "30分钟前", "1小时前", "2小时前", "1天前", "2天前", "1周前"};

    //选中的时间
    private String selectTime = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_calendar_alert);
        init();
    }

    private void init(){
        mListView = (ListView)findViewById(R.id.alert_list);
        mListView.setAdapter(new AlertAdapter());
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {
                    selectTime = 0 + "";
                } else if (position == 1) {
                    selectTime = 5 + "";
                } else if (position == 2) {
                    selectTime = 15 + "";
                } else if (position == 3) {
                    selectTime = 30 + "";
                } else if (position == 4) {
                    selectTime = 60 + "";
                } else if (position == 5) {
                    selectTime = 120 + "";
                } else if (position == 6) {
                    selectTime = (24 * 60) + "";
                } else if (position == 7) {
                    selectTime = (2 * 24 * 60) + "";
                } else if (position == 8) {
                    selectTime = (7 * 24 * 60) + "";
                }

                Intent alertIntent = new Intent();
                alertIntent.putExtra("time", selectTime);
                setResult(0, alertIntent);
                CalendarAlertAct.this.finish();
            }
        });

        findViewById(R.id.alert_no).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectTime = "-1";
                Intent alertIntent = new Intent();
                alertIntent.putExtra("time", selectTime);
                setResult(0, alertIntent);
                CalendarAlertAct.this.finish();
            }
        });

        findViewById(R.id.alert_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backResult();
            }
        });
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            backResult();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


    /**
     * 返回传值,不然报错
     */
    public void backResult(){
        selectTime = "-1";
        Intent alertIntent = new Intent();
        alertIntent.putExtra("time", selectTime);
        setResult(0, alertIntent);
        CalendarAlertAct.this.finish();
    }
    private class AlertAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return times.length;
        }

        @Override
        public Object getItem(int position) {
            return times[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.calendar_alert_list_item, null);
            TextView text = (TextView)convertView.findViewById(R.id.alert_time);
            text.setText(times[position]);
            return convertView;
        }
    }
}
