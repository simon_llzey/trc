package cn.trcfitness.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import cn.trcfitness.R;
import cn.trcfitness.model.CalendarEvent;
import cn.trcfitness.utils.DateUtils;
import cn.trcfitness.view.activity.CalendarDetailAct;
import cn.trcfitness.view.activity.DeleteCalendarAct;

/**
 * Created by Administrator on 2016/5/20.
 * 日历详细
 */
public class CalendarDetailAdapter extends BaseAdapter {

    private Context mContext;

    private List<CalendarEvent> mList;

    //判断是否编辑
    public boolean isEdit = false;

    public CalendarDetailAdapter(Context context, List<CalendarEvent> list) {
        this.mContext = context;
        this.mList = list;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.calendar_detail_list_item, null);
            viewHolder.mDetail = (LinearLayout) convertView.findViewById(R.id.detail_data);
            viewHolder.mShare = (LinearLayout) convertView.findViewById(R.id.item_share);
            viewHolder.title = (TextView)convertView.findViewById(R.id.event_title);
            viewHolder.reservation = (TextView)convertView.findViewById(R.id.event_remark);
            viewHolder.create_time = (TextView)convertView.findViewById(R.id.event_reservation_time);
            viewHolder.mType = (ImageView)convertView.findViewById(R.id.event_type);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }


        viewHolder.title.setText(mList.get(position).getTitle());
        viewHolder.reservation.setText(mList.get(position).getRemark());
        int type = mList.get(position).getType();
        if(type == 1){
            viewHolder.mType.setImageResource(R.drawable.user);
            viewHolder.create_time.setText(DateUtils.timesTurnDate(mList.get(position).getBegin()));
        }else {
            viewHolder.mType.setImageResource(R.drawable.activity);
            viewHolder.create_time.setText(DateUtils.date4(mList.get(position).getBegin())+"~"+DateUtils.date4(mList.get(position).getEnd()));
        }
        viewHolder.mDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent catIntent = new Intent(mContext, DeleteCalendarAct.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("calendar_details", mList.get(position));
                catIntent.putExtras(bundle);
                mContext.startActivity(catIntent);
            }
        });

        if (isEdit) {
            viewHolder.mShare.setVisibility(View.VISIBLE);
            viewHolder.mShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //当前日历id，传到activity
                    ((CalendarDetailAct) mContext).Share(mList.get(position).getId());
                }
            });
        } else {
            viewHolder.mShare.setVisibility(View.GONE);
        }
        return convertView;
    }

    private class ViewHolder {
        TextView title, create_time, reservation;
        private ImageView mType;
        private LinearLayout mShare, mDetail;

    }


}
