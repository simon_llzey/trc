package cn.trcfitness.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import cn.trcfitness.R;
import cn.trcfitness.model.OrdersBean;
import cn.trcfitness.utils.DateUtils;

/**
 * Created by ziv on 2016/7/8.
 */
public class TimeBalanceAdapter extends BaseAdapter {
    private Context context;
    private List<OrdersBean> mList;

    public TimeBalanceAdapter(Context context, List<OrdersBean> mList) {
        this.context = context;
        this.mList = mList;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.item_timebalance, null);
            holder.name = (TextView) convertView.findViewById(R.id.tv_item_timebalance_name);
            holder.time = (TextView) convertView.findViewById(R.id.tv_item_timebalance_time);
            holder.validity = (TextView) convertView.findViewById(R.id.tv_item_timebalance_validity);
            holder.im = (ImageView) convertView.findViewById(R.id.im_itme_timebalance);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        OrdersBean ordersBean = mList.get(position);
        holder.name.setText(ordersBean.getProduct().getName());
        String stime = countDate(ordersBean.getService_date(), ordersBean.getService_day());
        holder.validity.setText(stime);
        int day = ordersBean.getService_day() - countday(ordersBean.getService_date());
        holder.time.setText(day + "天");

        if (ordersBean.getProduct().getCategory() == 0) {
            holder.im.setBackgroundResource(R.drawable.year_card);//包年
        } else {
            holder.im.setBackgroundResource(R.drawable.private_card);//包月
        }

        return convertView;
    }

    public void setList(List<OrdersBean> list) {
        this.mList = list;
    }

    /**
     * 计算到期时间
     *
     * @param time 时间戳
     * @param i    时间间隔
     * @return
     */
    public String countDate(Long time, int i) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = DateUtils.timesTurnDate1(time);//购买时间
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) + i);//让日期加上购买的时间
        String dateStr = sdf.format(calendar.getTime());
        return dateStr;
    }

    /**
     * @param time
     * @return 计算当前时间与time的间隔天数
     */
    public int countday(Long time) {
        Date dt = new Date();//当前时间
        Date date = DateUtils.timesTurnDate1(time);//购买时间
        Long day = (dt.getTime() - date.getTime()) / (1000 * 60 * 60 * 24);
        int i = new Long(day).intValue();
        return i;
    }

    private class ViewHolder {
        TextView name;
        TextView time;
        TextView validity;
        ImageView im;
    }
}
