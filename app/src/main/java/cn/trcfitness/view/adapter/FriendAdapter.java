package cn.trcfitness.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.joooonho.SelectableRoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

import cn.trcfitness.R;
import cn.trcfitness.application.OverallApplication;
import cn.trcfitness.controller.UserController;
import cn.trcfitness.http.model.ResultParam;
import cn.trcfitness.model.Frends;

/**
 * Created by Administrator on 2016/5/16.
 * 好友
 */
public class FriendAdapter extends BaseAdapter {

    private List<Frends> mList;//所有用户
    private Context mContext;
    //判断是否为查找，如果是查找就显示添加按钮
    private boolean isSearch = false;
    private ViewHolder viewHolder = null;

    public FriendAdapter(Context contex, List<Frends> list, boolean search) {
        this.mContext = contex;
        this.mList = list;
        this.isSearch = search;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
//        if (convertView == null) {
        viewHolder = new ViewHolder();
        convertView = LayoutInflater.from(mContext).inflate(R.layout.friend_item_list, null);
        viewHolder.mHead = (SelectableRoundedImageView) convertView.findViewById(R.id.friend_head);
        viewHolder.mName = (TextView) convertView.findViewById(R.id.friend_name);
        viewHolder.mAddPerson = (Button) convertView.findViewById(R.id.friend_add);
        convertView.setTag(viewHolder);
//        } else {
//            viewHolder = (ViewHolder) convertView.getTag();
//        }
        if (isSearch) {
            viewHolder.mAddPerson.setVisibility(View.VISIBLE);
            if (mList.get(position).getAgree() != null) {
                if (mList.get(position).getAgree().intValue() == 0) {
                    viewHolder.mAddPerson.setText("等待确认");
                    viewHolder.mAddPerson.setBackgroundResource(R.drawable.btn_add_person2);
                    viewHolder.mAddPerson.setEnabled(false);
                } else if (mList.get(position).getAgree().intValue() == 1) {
                    viewHolder.mAddPerson.setText("已添加");
                    viewHolder.mAddPerson.setBackgroundResource(R.drawable.btn_add_person2);
                    viewHolder.mAddPerson.setEnabled(false);
                } else if (mList.get(position).getAgree().intValue() == 2) {
                    viewHolder.mAddPerson.setText("添加");
                    viewHolder.mAddPerson.setBackgroundResource(R.drawable.btn_add_person);
                    viewHolder.mAddPerson.setEnabled(true);
                }
            }

            if (OverallApplication.userData.getData().getUser().getUser_id() == mList.get(position).getU_userDetail().getUser_id()) {
                viewHolder.mAddPerson.setVisibility(View.GONE);
            }

            viewHolder.mAddPerson.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UserController.add_frend(new Observer() {
                        @Override
                        public void update(Observable observable, Object data) {
                            if (data != null && (ResultParam) data != null) {
                                if (((ResultParam) data).getStatus().equals("200")) {
                                    Toast.makeText(mContext, "好友请求发送成功", Toast.LENGTH_SHORT).show();
                                    mList.get(position).setAgree(0);
                                    notifyDataSetChanged();
                                }
                            }
                        }
                    }, mList.get(position).getU_userDetail().getUser_id() + "");
                }
            });
        } else {
            viewHolder.mAddPerson.setVisibility(View.GONE);
        }
        String ImgUrl = null;
        if (isSearch) {
            viewHolder.mName.setText(mList.get(position).getU_userDetail().getNick_name());
            ImgUrl = mList.get(position).getU_userDetail().getImg();
        } else {
            viewHolder.mName.setText(mList.get(position).getF_userDetail().getNick_name());
            ImgUrl = mList.get(position).getF_userDetail().getImg();
        }

        if (ImgUrl != null && ImgUrl.length() > 0) {
            Picasso.with(mContext).load(ImgUrl).into(viewHolder.mHead);
        }

        return convertView;
    }

    public void setList(List<Frends> list) {
        this.mList = list;
    }

    private class ViewHolder {
        TextView mName;
        SelectableRoundedImageView mHead;
        Button mAddPerson;
    }

}