package cn.trcfitness.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import cn.trcfitness.R;
import cn.trcfitness.model.TRCParam;

/**
 * Created by Administrator on 2016/5/27.
 * TRC健身列表
 */
public class TRCListAdapter extends BaseAdapter {

    private List<TRCParam.DataBean.PostsBean> mList;
    private Context mContext;

    public TRCListAdapter(Context context, List<TRCParam.DataBean.PostsBean> list) {
        this.mContext = context;
        this.mList = list;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
//        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.trc_list_item, null);
            viewHolder.img = (ImageView) convertView.findViewById(R.id.trc_image);
            viewHolder.title = (TextView) convertView.findViewById(R.id.trc_title);
            viewHolder.info = (TextView) convertView.findViewById(R.id.trc_content);
//            convertView.setTag(viewHolder);
//        } else {
//            viewHolder = (ViewHolder) convertView.getTag();
//        }

        viewHolder.title.setText(mList.get(position).getTitle());
        Picasso.with(mContext).load(mList.get(position).getCover()).into(viewHolder.img);
        return convertView;
    }


    private class ViewHolder {
        ImageView img;
        TextView info;
        TextView title;
        LinearLayout lin;
    }
}
