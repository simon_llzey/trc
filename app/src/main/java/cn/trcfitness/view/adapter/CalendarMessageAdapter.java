package cn.trcfitness.view.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.joooonho.SelectableRoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

import cn.trcfitness.R;
import cn.trcfitness.application.OverallApplication;
import cn.trcfitness.controller.UserController;
import cn.trcfitness.http.model.ResultParam;
import cn.trcfitness.model.ResultMessageParam;
import cn.trcfitness.model.UserRequestParam;
import cn.trcfitness.utils.DateUtils;
import cn.trcfitness.view.fragmet.CalendarFra;

/**
 * Created by Administrator on 2016/5/20.
 * 日历消息
 */
public class CalendarMessageAdapter extends BaseAdapter {

    private Context mContext;
    private List<ResultMessageParam.DataBean.MessageBean> list;
    private Boolean delect = false;

    public CalendarMessageAdapter(Context context, List<ResultMessageParam.DataBean.MessageBean> mList) {
        this.mContext = context;
        this.list = mList;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
//        if (convertView == null) {
        convertView = LayoutInflater.from(mContext).inflate(R.layout.calendar_message_list_item, null);
        holder = new ViewHolder();
        holder.iv = (SelectableRoundedImageView) convertView.findViewById(R.id.iv_item_calender_message);
        holder.tv_title = (TextView) convertView.findViewById(R.id.tv_item_calender_message_title);
        holder.tv_time = (TextView) convertView.findViewById(R.id.tv_item_calender_message_time);
        holder.tv_content = (TextView) convertView.findViewById(R.id.tv_item_calender_message_content);
        holder.tv_bt = (Button) convertView.findViewById(R.id.tv_item_calender_message_bt);
        holder.Linlay = (LinearLayout) convertView.findViewById(R.id.Linlay_item_aclendar_message);
        holder.tv_delect = (TextView) convertView.findViewById(R.id.tv_item_aclendar_message_delect);
//            convertView.setTag(holder);
//        } else {
//            holder = (ViewHolder) convertView.getTag();
//        }
        final ResultMessageParam.DataBean.MessageBean messageBean = list.get(position);
        Picasso.with(mContext).load(messageBean.getImg()).into(holder.iv);
        holder.tv_title.setText(messageBean.getTitle());
        holder.tv_time.setText(DateUtils.date2(messageBean.getCreate_time()));
        holder.tv_content.setText(messageBean.getContent());

        if (messageBean.getType() == 0) {//0文本消息  1日历消息   2好友申请
            holder.tv_bt.setVisibility(View.GONE);
        } else if (messageBean.getType() == 1) {
            holder.tv_bt.setVisibility(View.VISIBLE);
            switch (messageBean.getRecevie()) {
                case 0:
                    holder.tv_bt.setText("接受");
                    break;
                case 1:
                    holder.tv_bt.setText("已接受");
                    holder.tv_bt.setEnabled(false);
                    holder.tv_bt.setBackgroundResource(R.color.white);
                    holder.tv_bt.setTextColor(Color.parseColor("#7f7f7f"));
                    break;
                case 2:
                    holder.tv_bt.setText("已拒绝");
                    holder.tv_bt.setBackgroundResource(R.color.white);
                    holder.tv_bt.setTextColor(Color.parseColor("#7f7f7f"));
                    holder.tv_bt.setEnabled(false);
                    break;
            }
            if(messageBean.getSid()==0){
                holder.tv_bt.setVisibility(View.GONE);
            }
        } else {
            if (OverallApplication.userData.getData().getUser().getUser_id() == messageBean.getSid()) {
                holder.tv_bt.setVisibility(View.GONE);
            } else {
                holder.tv_bt.setVisibility(View.VISIBLE);
                switch (messageBean.getRecevie()) {
                    case 0:
                        holder.tv_bt.setText("接受");
                        break;
                    case 1:
                        holder.tv_bt.setText("已接受");
                        holder.tv_bt.setEnabled(false);
                        holder.tv_bt.setBackgroundResource(R.color.white);
                        holder.tv_bt.setTextColor(Color.parseColor("#7f7f7f"));
                        break;
                    case 2:
                        holder.tv_bt.setText("已拒绝");
                        holder.tv_bt.setBackgroundResource(R.color.white);
                        holder.tv_bt.setTextColor(Color.parseColor("#7f7f7f"));
                        holder.tv_bt.setEnabled(false);
                        break;
                }
            }
        }
        holder.tv_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (messageBean.getType() == 1) {
                    calendar(position, messageBean.getId());
                } else {
                    friend(position, messageBean.getId(), messageBean.getSid());
                }
            }
        });

        //删除消息
        holder.tv_delect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserRequestParam userRequestParam = new UserRequestParam();
                userRequestParam.setId(messageBean.getId());
                UserController.delMessage(new Observer() {
                    @Override
                    public void update(Observable observable, Object data) {
                        if (data != null && (ResultParam) data != null) {
                            ResultParam resultParam = (ResultParam) data;
                            if (resultParam.getStatus().equals("200")) {
                                Toast.makeText(mContext, "删除成功", Toast.LENGTH_SHORT).show();
                                list.remove(position);
                                notifyDataSetChanged();
                            }
                        }
                    }
                }, userRequestParam);
            }
        });

        if (delect) {
            holder.Linlay.setVisibility(View.VISIBLE);
        } else {
            holder.Linlay.setVisibility(View.GONE);
        }

        return convertView;
    }

    /**
     * 好友申请确认
     *
     * @param position
     * @param id       该条消息id
     * @param sid      申请好友的id
     */
    private void friend(final int position, final int id, final int sid) {
        UserRequestParam userRequestParam = new UserRequestParam();
        userRequestParam.setFid(sid);
        userRequestParam.setMid(id);
        userRequestParam.setAgree(1);
        UserController.friendagree(new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                ResultParam resultParam = (ResultParam) data;
                if (resultParam.getStatus().equals("200")) {
                    Toast.makeText(mContext, "成功", Toast.LENGTH_SHORT).show();
                    list.get(position).setRecevie(1);
                    notifyDataSetChanged();
                }

            }
        }, userRequestParam);
    }

    /**
     * 日历分享确认
     *
     * @param position
     * @param id       该条消息id
     */
    private void calendar(final int position, final int id) {
        UserRequestParam userRequestParam = new UserRequestParam();
        userRequestParam.setId(id);
        userRequestParam.setAgree(1);
        UserController.agree(new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                if (data != null && (ResultParam) data != null) {
                    ResultParam resultParam = (ResultParam) data;
                    if (resultParam.getStatus().equals("200")) {
                        Toast.makeText(mContext, "成功", Toast.LENGTH_SHORT).show();
                        CalendarFra.instance.refreshDate();
                        list.get(position).setRecevie(1);
                        notifyDataSetChanged();
                    }
                }
            }
        }, userRequestParam);
    }

    public void setList(List<ResultMessageParam.DataBean.MessageBean> list) {
        this.list = list;
    }

    public Boolean getDelect() {
        return delect;
    }

    public void setDelect(Boolean delect) {
        this.delect = delect;
    }

    private class ViewHolder {
        SelectableRoundedImageView iv;
        TextView tv_title;
        TextView tv_time;
        TextView tv_content;
        Button tv_bt;
        LinearLayout Linlay;
        TextView tv_delect;
    }
}
