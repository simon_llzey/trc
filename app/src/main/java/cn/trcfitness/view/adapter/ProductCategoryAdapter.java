package cn.trcfitness.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import cn.trcfitness.R;
import cn.trcfitness.model.ResultProductCategoryParam;

/**
 * Created by ziv on 2016/5/25.
 */
public class ProductCategoryAdapter extends BaseAdapter {
    List<ResultProductCategoryParam.DataBean.ClassifyBean> list_category;
    Context context;
    private int currentItem = -1;

    public ProductCategoryAdapter(List<ResultProductCategoryParam.DataBean.ClassifyBean> list_category, Context context) {
        this.list_category = list_category;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list_category.size();
    }

    @Override
    public Object getItem(int position) {
        return list_category.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_storefra_list, null);
            holder = new ViewHolder();
            holder.name = (TextView) convertView.findViewById(R.id.tv_item_storefra_list);
            holder.Linlay = (LinearLayout) convertView.findViewById(R.id.Linlay_item_storefra);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
            if (currentItem != -1 && currentItem == position) {
                holder.Linlay.setBackgroundResource(R.color.trc_backgr);
            } else {
                holder.Linlay.setBackgroundResource(R.color.white);
            }
        holder.name.setText(list_category.get(position).getName());
        return convertView;
    }

    public void setCurrentItem(int currentItem) {
        this.currentItem = currentItem;
    }

    private class ViewHolder {
        TextView name;
        LinearLayout Linlay;
    }
}
