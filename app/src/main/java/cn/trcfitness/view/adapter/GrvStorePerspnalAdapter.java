package cn.trcfitness.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import cn.trcfitness.R;
import cn.trcfitness.model.TRCInfo;

/**
 * Created by ziv on 2016/5/16.
 */
public class GrvStorePerspnalAdapter extends BaseAdapter {

    private Context mContext;
    private List<TRCInfo> mList;

    public GrvStorePerspnalAdapter(Context context, List<TRCInfo> list) {
        this.mContext = context;
        this.mList = list;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder vHolder = null;
        if (convertView == null) {
            vHolder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.grv_storepersonal_item, null);
            vHolder.name = (TextView) convertView.findViewById(R.id.grv_storepersonal_name);
            vHolder.image = (ImageView) convertView.findViewById(R.id.grv_storepersonal_image);
            convertView.setTag(vHolder);
        } else {
            vHolder = (ViewHolder) convertView.getTag();
        }
        vHolder.image.setImageResource(mList.get(position).getImage());
        vHolder.name.setText(mList.get(position).getName());

        return convertView;
    }

    private class ViewHolder {
        TextView name;
        ImageView image;
    }

}
