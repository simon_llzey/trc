package cn.trcfitness.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import cn.trcfitness.R;
import cn.trcfitness.model.ResultCity_ProvincesParam;

/**
 * Created by ziv on 2016/5/19.
 */
public class City_Provinces_Adapter extends BaseAdapter {
    Context context;
    List<ResultCity_ProvincesParam.Provincesparpm> data;
    private LayoutInflater inflater;

    public City_Provinces_Adapter(Context context, List<ResultCity_ProvincesParam.Provincesparpm> data) {
        this.context = context;
        this.data = data;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public ResultCity_ProvincesParam.Provincesparpm getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_city, null);
            holder = new ViewHolder();
            holder.name = (TextView) convertView.findViewById(R.id.tv_city_item);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        ResultCity_ProvincesParam.Provincesparpm provincesparpm = getItem(position);
        holder.name.setText(provincesparpm.getName());
        return convertView;
    }

    private class ViewHolder {
        TextView name;
    }
}
