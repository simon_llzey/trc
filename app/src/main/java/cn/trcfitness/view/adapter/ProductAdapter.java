package cn.trcfitness.view.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import cn.trcfitness.R;
import cn.trcfitness.model.ProductsBean;

/**
 * Created by ziv on 2016/5/25.
 */
public class ProductAdapter extends BaseAdapter {
    List<ProductsBean> list_product;
    Context context;

    public ProductAdapter(List<ProductsBean> list_product, Context context) {
        this.list_product = list_product;
        this.context = context;
    }

    public void setList(List<ProductsBean> list_product) {
        this.list_product = list_product;
    }

    @Override
    public int getCount() {
        return list_product.size();
    }

    @Override
    public ProductsBean getItem(int position) {
        return list_product.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_storefra_grid, null);
            holder = new ViewHolder();
            holder.im = (ImageView) convertView.findViewById(R.id.im_item_storefra_grid);
            holder.name = (TextView) convertView.findViewById(R.id.tv_item_storefra_name);
            holder.price = (TextView) convertView.findViewById(R.id.tv_item_storefra_price);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        ProductsBean productsBean = list_product.get(position);

        holder.name.setText(productsBean.getName());
        holder.price.setText("￥" + productsBean.getPrice());

        if (productsBean.getPcs().size() > 0) {
            String ImgUrl = productsBean.getPcs().get(0).getCover();
            if (ImgUrl != null && ImgUrl.length() > 0) {
                Picasso.with(context).load(ImgUrl).into(holder.im);
            }
        }
        return convertView;
    }

    private class ViewHolder {
        ImageView im;
        TextView name;
        TextView price;
    }
}
