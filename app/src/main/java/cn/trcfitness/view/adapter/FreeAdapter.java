package cn.trcfitness.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import cn.trcfitness.R;
import cn.trcfitness.model.FreeInfo;

/**
 * Created by Administrator on 2016/7/8.
 * 免费活动侧滑列表选项
 */
public class FreeAdapter extends BaseAdapter {

    private List<FreeInfo> list;
    private Context mContext;

    public FreeAdapter(Context context, List<FreeInfo> l){
        this.mContext = context;
        this.list = l;
        isSelected = new HashMap<Integer, Boolean>();
        initDate();
    }


    public HashMap<Integer, Boolean> getIsSelected() {
        return isSelected;
    }

    public void setIsSelected(HashMap<Integer, Boolean> isSelected) {
        this.isSelected = isSelected;
    }

    // 用来控制CheckBox的选中状况
    private HashMap<Integer, Boolean> isSelected;

    // 初始化isSelected的数据
    private void initDate() {
        for (int i = 0; i < list.size(); i++) {
            getIsSelected().put(i, false);
        }
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder vHolder = null;
        if (convertView == null) {
            vHolder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.calendar_free_list_item, null);
            vHolder.name = (TextView) convertView.findViewById(R.id.calendar_free_name);
            vHolder.image = (ImageView) convertView.findViewById(R.id.calendar_free_img);
            vHolder.select = (CheckBox) convertView.findViewById(R.id.calendar_free_sel);
            convertView.setTag(vHolder);
        } else {
            vHolder = (ViewHolder) convertView.getTag();
        }

        vHolder.name.setText(list.get(position).getName());
        vHolder.image.setImageResource(list.get(position).getImage());
        vHolder.select.setChecked(getIsSelected().get(position));


        return convertView;
    }
    public class ViewHolder {
        public TextView name;
        public ImageView image;
        public CheckBox select;
    }
}

