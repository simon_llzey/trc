package cn.trcfitness.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import cn.trcfitness.R;
import cn.trcfitness.model.ResultAddressParam;

/**
 * Created by ziv on 2016/5/18.
 */
public class ReciptAddressAdapter extends BaseAdapter {
    private Context context;
    private List<ResultAddressParam.ReciptAddressparam> list;
    private LayoutInflater inflater;

    public ReciptAddressAdapter(Context context, List<ResultAddressParam.ReciptAddressparam> list) {
        this.context = context;
        this.list = list;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public ResultAddressParam.ReciptAddressparam getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_reciptaddress, null);
            holder = new ViewHolder();
            holder.name = (TextView) convertView.findViewById(R.id.tv_item_reciptaddress_name);
            holder.phone = (TextView) convertView.findViewById(R.id.tv_item_reciptaddress_phone);
            holder.address = (TextView) convertView.findViewById(R.id.tv_item_reciptaddress_address);
            holder.im= (ImageView) convertView.findViewById(R.id.im_item_reciptaddress);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        ResultAddressParam.ReciptAddressparam reciptAddressparam=list.get(position);
        holder.name.setText(reciptAddressparam.getConsignee());
        holder.phone.setText(reciptAddressparam.getPhone());
        holder.address.setText(reciptAddressparam.getAddress());
        if(reciptAddressparam.getDefaults()){
            holder.im.setVisibility(View.VISIBLE);
        }else {
            holder.im.setVisibility(View.INVISIBLE);
        }

        return convertView;
    }

    private class ViewHolder {
        TextView name;
        TextView phone;
        TextView address;
        ImageView im;
    }
}
