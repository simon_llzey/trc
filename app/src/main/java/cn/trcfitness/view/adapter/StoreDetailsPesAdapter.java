package cn.trcfitness.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lidroid.xutils.BitmapUtils;
import com.squareup.picasso.Picasso;

import java.util.List;

import cn.trcfitness.R;
import cn.trcfitness.model.ProductsBean;

/**
 * Created by ziv on 2016/5/23.
 * 商品评论
 */
public class StoreDetailsPesAdapter extends BaseAdapter {
    private Context context;
    private List<ProductsBean.ProductEvaluate> pes;
    private LayoutInflater inflater;
    private BitmapUtils bitmapUtils;

    public StoreDetailsPesAdapter(Context context, List<ProductsBean.ProductEvaluate> pes) {
        this.context = context;
        this.pes = pes;
        inflater = LayoutInflater.from(context);
        bitmapUtils = new BitmapUtils(context);
    }

    @Override
    public int getCount() {
        return pes.size();
    }

    @Override
    public ProductsBean.ProductEvaluate getItem(int position) {
        return pes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_storedetailspes, null);
            holder = new ViewHolder();
            holder.im = (ImageView) convertView.findViewById(R.id.im_item_storedetailspec);
            holder.tv_name = (TextView) convertView.findViewById(R.id.tv_item_storedetailspec_name);
            holder.tv_evaluate = (TextView) convertView.findViewById(R.id.tv_item_storedetailspec_evaluate);
            holder.tv_pid = (TextView) convertView.findViewById(R.id.tv_item_storedetailspec_pid);
            holder.tv_create_time = (TextView) convertView.findViewById(R.id.tv_item_storedetailspec_create_time);
            holder.viewGroup = (ViewGroup) convertView.findViewById(R.id.linlay_item_storedetailspec);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        ProductsBean.ProductEvaluate pesBean = pes.get(position);
        Picasso.with(context).load(pesBean.getImg()).into(holder.im);

        holder.tv_name.setText(pesBean.getName());
        holder.tv_evaluate.setText(pesBean.getEvaluate());
        if (pesBean.getPname() == null) {
            holder.tv_pid.setText("");

        } else {
            holder.tv_pid.setText(pesBean.getPname() + "");
        }

        for (int i = 0; i < pesBean.getScore(); i++) {
            ((ImageView) holder.viewGroup.getChildAt(i)).setImageResource(R.drawable.star);// getChildAt获取布局
        }
        return convertView;
    }

    private class ViewHolder {
        ImageView im;
        TextView tv_name, tv_evaluate, tv_pid, tv_create_time;
        ViewGroup viewGroup;
    }
}
