package cn.trcfitness.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

import cn.trcfitness.R;
import cn.trcfitness.controller.UserController;
import cn.trcfitness.http.model.ResultParam;
import cn.trcfitness.model.OrdersBean;
import cn.trcfitness.model.RequestupdateOrderStatus;
import cn.trcfitness.view.activity.OrderEvaluationActivity;

/**
 * Created by ziv on 2016/5/31.
 * 我的订单
 */
public class MyOrderAdapter extends BaseAdapter {
    Context context;
    List<OrdersBean> mlist;
    RequestupdateOrderStatus requestupdateOrderStatus;//更新用户订单状态上传参数
    String type;//0在全部界面

    public MyOrderAdapter(Context context, List<OrdersBean> mlist, String type) {
        this.context = context;
        this.mlist = mlist;
        this.type = type;
    }

    @Override
    public int getCount() {
        return mlist.size();
    }

    @Override
    public Object getItem(int position) {
        return mlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder vHolder = null;
        if (convertView == null) {
            vHolder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.item_myorder, null);
            vHolder.im = (ImageView) convertView.findViewById(R.id.im_item_myorder);
            vHolder.name = (TextView) convertView.findViewById(R.id.tv_item_myorder_name);
            vHolder.money = (TextView) convertView.findViewById(R.id.tv_item_myorder_money);
            vHolder.number = (TextView) convertView.findViewById(R.id.tv_item_myorder_number);
            vHolder.money2 = (TextView) convertView.findViewById(R.id.tv_item_myorder_money2);
            vHolder.postage = (TextView) convertView.findViewById(R.id.tv_item_myorder_postage);
            vHolder.bt = (TextView) convertView.findViewById(R.id.bt_item_myorder);
            vHolder.bt2 = (TextView) convertView.findViewById(R.id.bt_item_myorder2);
            convertView.setTag(vHolder);
        } else {
            vHolder = (ViewHolder) convertView.getTag();
        }
        final OrdersBean ordersBean = mlist.get(position);
        if (ordersBean.getProduct().getPcs() != null && ordersBean.getProduct().getPcs().size() > 0) {
            Picasso.with(context).load(ordersBean.getProduct().getPcs().get(0).getCover()).into(vHolder.im);
        }
        vHolder.name.setText(ordersBean.getProduct_name());
        vHolder.money.setText("￥" + ordersBean.getPrice() + "");
        vHolder.number.setText("X" + ordersBean.getNumber() + "");
        Float Postage = 0f;
        if (ordersBean.getPostage() != null) {
            Postage = ordersBean.getPostage();
            vHolder.postage.setText(Postage + "");
        } else {
            vHolder.postage.setText("0.00");
        }
        Float money = (ordersBean.getNumber().intValue() * ordersBean.getPrice() + Postage);
        vHolder.money2.setText(money + "");

        String[] status = {"待发货", " 确认收货", "订单完成", " 评价", " 退货", "已评价"};
        vHolder.bt.setText(status[ordersBean.getStatus()]);//订单状态 0 待发货 1 待收货 （2 完成已去掉） 3 待评价 4 退货 5已评价
        switch (ordersBean.getStatus()) {
            case 0:
                vHolder.bt.setBackgroundResource(R.color.white);
                vHolder.bt2.setVisibility(View.VISIBLE);
                break;
            case 1:
                vHolder.bt.setBackgroundResource(R.drawable.item_myorder_button);
                vHolder.bt2.setVisibility(View.VISIBLE);
                break;
            case 2:
                break;
            case 3:
                vHolder.bt.setBackgroundResource(R.drawable.item_myorder_button);
                vHolder.bt2.setVisibility(View.VISIBLE);
                break;
            case 4:
                vHolder.bt.setBackgroundResource(R.color.white);
                vHolder.bt2.setVisibility(View.GONE);
                break;
            case 5:
                vHolder.bt.setBackgroundResource(R.color.white);
                vHolder.bt2.setVisibility(View.GONE);
                break;
        }

        if (ordersBean.getProduct().getType() == 0) {
            vHolder.bt2.setVisibility(View.GONE);
        } else {
            switch (ordersBean.getRefund_status()) {
                case 0:
                    break;
                case 1:
                    vHolder.bt.setText("退货中");
                    break;
                case 2:
                    vHolder.bt.setText("退货成功");
                    break;
                case 3:
                    vHolder.bt.setText("退货失败");
                    break;
            }
        }

        vHolder.bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (ordersBean.getStatus()) {
                    case 0:
                        break;
                    case 1:
                        requestupdateOrderStatus = new RequestupdateOrderStatus();
                        requestupdateOrderStatus.setId(ordersBean.getId());
                        ChangeStatus(3, position);//确认收货  待收货-待评价
                        break;
                    case 2:
                        break;
                    case 3:
                        Intent intent = new Intent(context, OrderEvaluationActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("OrdersBean", ordersBean);
                        intent.putExtras(bundle);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                        break;
                    case 4:
                        break;
                    case 5:
                        break;
                }
            }
        });

        vHolder.bt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestupdateOrderStatus = new RequestupdateOrderStatus();
                requestupdateOrderStatus.setId(ordersBean.getId());
                ChangeStatus(4, position);//确认收货  待收货-待评价
            }
        });
        return convertView;
    }

    /**
     * @param Status    改变之后的状态
     * @param posintion
     */
    private void ChangeStatus(final int Status, final int posintion) {
        requestupdateOrderStatus.setStatus(Status);
        UserController.updateOrderStatus(new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                if (data != null && (ResultParam) data != null) {
                    ResultParam resultParam = (ResultParam) data;
                    if (resultParam.getStatus().equals("200")) {
                        if (!type.equals("0")) {
                            mlist.remove(posintion);
                        } else {
                            mlist.get(posintion).setStatus(Status);
                        }
                        notifyDataSetChanged();
                    }
                }
            }
        }, requestupdateOrderStatus);
    }

    public void setList(List<OrdersBean> list) {
        this.mlist = list;
    }

    private void mToast(String s) {
        Toast.makeText(context, s, Toast.LENGTH_LONG).show();
    }

    private class ViewHolder {
        ImageView im;
        TextView name;
        TextView money;
        TextView number;
        TextView money2;
        TextView postage;
        TextView bt;
        TextView bt2;
    }
}
