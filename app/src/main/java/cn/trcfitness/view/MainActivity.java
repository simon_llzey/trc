package cn.trcfitness.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import cn.trcfitness.R;
import cn.trcfitness.application.OverallApplication;
import cn.trcfitness.view.activity.LoginActivity;
import cn.trcfitness.view.activity.RegisterActivity;
import cn.trcfitness.view.fragmet.MainFragment;

/**
 * Created by Eric
 * on 2016/05/10
 */
public class MainActivity extends Activity {
    Button bt_login, bt_register;
    public static MainActivity instance = null;


    private Handler mHand = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Intent intent = new Intent();
            //if(isFirstIn || loginParam == null || StringUtils.isBlank(loginParam.getPassword())) {
            intent.setClass(MainActivity.this, LoginActivity.class);
            /*} else {
                intent.setClass(WelcomeActivity.this, MainActivity.class);
            }*/
            //    startActivity(intent);
            finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        instance = this;

//        mHand.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                mHand.sendMessage(new Message());
//            }
//        }, 1000);

        findViewById(R.id.bt_activity_main_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });

        findViewById(R.id.bt_activity_main_login2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, MainFragment.class);
                OverallApplication.tourists = true;
                startActivity(intent);
            }
        });

        findViewById(R.id.bt_activity_main_register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });
    }

}
