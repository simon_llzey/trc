package cn.trcfitness.model;

import java.io.Serializable;
import java.util.List;

import cn.trcfitness.http.model.ResultParam;

/**
 * Created by Administrator on 2016/5/27.
 * TRC健身列表
 */
public class TRCParam extends ResultParam {

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {

        private List<PostsBean> posts;

        public List<PostsBean> getPosts() {
            return posts;
        }

        public void setPosts(List<PostsBean> posts) {
            this.posts = posts;
        }

        public static class PostsBean implements Serializable {
            private Integer id;
            private String title;
            private String content;
            private String cover; //封面
            private Boolean status;
            private String create_time;
            private String str_create_time;

            public String getCover() {
                return cover;
            }

            public void setCover(String cover) {
                this.cover = cover;
            }

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public Boolean getStatus() {
                return status;
            }

            public void setStatus(Boolean status) {
                this.status = status;
            }

            public String getCreate_time() {
                return create_time;
            }

            public void setCreate_time(String create_time) {
                this.create_time = create_time;
            }

            public String getStr_create_time() {
                return str_create_time;
            }

            public void setStr_create_time(String str_create_time) {
                this.str_create_time = str_create_time;
            }
        }
    }
}
