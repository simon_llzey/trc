package cn.trcfitness.model;

import java.util.List;

import cn.trcfitness.http.model.ResultParam;

/**
 * Created by ziv on 2016/5/19.
 * 区
 */
public class ResultCity_CidParam extends ResultParam {
    private List<Cid> data;

    @Override
    public List<Cid> getData() {
        return data;
    }

    public void setData(List<Cid> data) {
        this.data = data;
    }



    public class Cid {
        String id;
        String c_id;
        String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getC_id() {
            return c_id;
        }

        public void setC_id(String c_id) {
            this.c_id = c_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return "Cid{" +
                    "id='" + id + '\'' +
                    ", c_id='" + c_id + '\'' +
                    ", name='" + name + '\'' +
                    '}';
        }
    }
}
