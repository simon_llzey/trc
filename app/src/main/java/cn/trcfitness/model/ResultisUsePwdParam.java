package cn.trcfitness.model;

import cn.trcfitness.http.model.ResultParam;

/**
 * Created by ziv on 2016/6/13.
 */
public class ResultisUsePwdParam extends ResultParam {
    private boolean data;

    public boolean isData() {
        return data;
    }

    public void setData(boolean data) {
        this.data = data;
    }
}
