package cn.trcfitness.model;

import cn.trcfitness.http.model.ResultParam;

/**
 * Created by ziv on 2016/6/2.
 * 获取账户余额
 */
public class ResultBalanceParam extends ResultParam {

    private Float data;

    public Float getData() {
        return data;
    }

    public void setData(Float data) {
        this.data = data;
    }
}
