package cn.trcfitness.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ziv on 2016/5/20.
 * 商品对象
 */
public class ProductsBean implements Serializable {
    private Integer id; //id
    private Integer uid; //商家的id
    private Integer cid; //分类ID
    private String name; //商品名称
    private Integer type; //商品类型 0 服务预约类 1 实体商品类
    private Integer category;//0包年  1月卡 2次数
    private Integer category_count;//次数或天数
    private Integer cancel_time;//预约后多久不能取消 按分钟算
    private String detail; //商品卖点
    private String info; //商品卖点
    private Float postage; //邮费
    private Float price; //单价
    private Integer stock; //库存
    private Integer service_time; //服务预约类商品的 一次服务时间 （分钟）
    private Integer classify; //实体商品分类
    private String consumer_name; //预约类商品 消费商家的名称
    private String consumer_address; //预约类商品 消费商家的地址
    private String phone; //预约类商品 消费商家的电话
    private Boolean status; //是否上架
    private String create_time; //商品创建时间
    private String online_time; //商品上架时间
    private Boolean deleted; //是否删除
    private List<ProductCover> pcs; //商品的封面
    private List<Size> pss; //商品的尺寸
    private List<ProductEvaluate> pes; //商品的评论
    private List<ProductColor> pcos; //商品颜色

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getCid() {
        return cid;
    }

    public void setCid(Integer cid) {
        this.cid = cid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public Integer getCategory_count() {
        return category_count;
    }

    public void setCategory_count(Integer category_count) {
        this.category_count = category_count;
    }

    public Integer getCancel_time() {
        return cancel_time;
    }

    public void setCancel_time(Integer cancel_time) {
        this.cancel_time = cancel_time;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Float getPostage() {
        return postage;
    }

    public void setPostage(Float postage) {
        this.postage = postage;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Integer getService_time() {
        return service_time;
    }

    public void setService_time(Integer service_time) {
        this.service_time = service_time;
    }

    public Integer getClassify() {
        return classify;
    }

    public void setClassify(Integer classify) {
        this.classify = classify;
    }

    public String getConsumer_name() {
        return consumer_name;
    }

    public void setConsumer_name(String consumer_name) {
        this.consumer_name = consumer_name;
    }

    public String getConsumer_address() {
        return consumer_address;
    }

    public void setConsumer_address(String consumer_address) {
        this.consumer_address = consumer_address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getOnline_time() {
        return online_time;
    }

    public void setOnline_time(String online_time) {
        this.online_time = online_time;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public List<ProductCover> getPcs() {
        return pcs;
    }

    public void setPcs(List<ProductCover> pcs) {
        this.pcs = pcs;
    }

    public List<Size> getPss() {
        return pss;
    }

    public void setPss(List<Size> pss) {
        this.pss = pss;
    }

    public List<ProductEvaluate> getPes() {
        return pes;
    }

    public void setPes(List<ProductEvaluate> pes) {
        this.pes = pes;
    }

    public List<ProductColor> getPcos() {
        return pcos;
    }

    public void setPcos(List<ProductColor> pcos) {
        this.pcos = pcos;
    }

    public static class ProductCover implements Serializable {
        private int id;
        private int pid;
        private String cover;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getPid() {
            return pid;
        }

        public void setPid(int pid) {
            this.pid = pid;
        }

        public String getCover() {
            return cover;
        }

        public void setCover(String cover) {
            this.cover = cover;
        }

        @Override
        public String toString() {
            return "ProductCover{" +
                    "id=" + id +
                    ", pid=" + pid +
                    ", cover='" + cover + '\'' +
                    '}';
        }
    }

    public static class Size implements Serializable {
        private int id;
        private String size;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getSize() {
            return size;
        }

        public void setSize(String size) {
            this.size = size;
        }

        @Override
        public String toString() {
            return "Size{" +
                    "id=" + id +
                    ", size='" + size + '\'' +
                    '}';
        }
    }

    public static class ProductColor implements Serializable {
        private int id;
        private int pid;
        private String color;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getPid() {
            return pid;
        }

        public void setPid(int pid) {
            this.pid = pid;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        @Override
        public String toString() {
            return "ProductColor{" +
                    "id=" + id +
                    ", pid=" + pid +
                    ", color='" + color + '\'' +
                    '}';
        }
    }

    public class ProductEvaluate implements Serializable{
        private Object type;
        private Integer user_id; //用户ID
        private String name; //姓名
        private String nick_name; //昵称
        private String phone; //电话
        private String birth; //生日
        private Integer sex; //性别
        private String card_num; //会员卡号
        private Float money; //开卡金额
        private Float real_money; //余额
        private Integer is_year; //开卡时是否购买年卡
        private String remark; //备注
        private String img; //头像
        private String create_time; //创建时间
        private String pay_pwd; //支付密码
        private String myId; //我的ID图片的路径
        private Integer exempt_money; //免密金额
        private Integer id; //id
        private Integer uid; //评论用户的ID
        private Integer pid; //商品的ID
        private Integer score; //评分
        private String evaluate; //评论
        private String eval_create_time; //评论时间
        private String tail; //用户电话尾号
        private String pname; //商品名称

        public Object getType() {
            return type;
        }

        public void setType(Object type) {
            this.type = type;
        }

        public Integer getUser_id() {
            return user_id;
        }

        public void setUser_id(Integer user_id) {
            this.user_id = user_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getNick_name() {
            return nick_name;
        }

        public void setNick_name(String nick_name) {
            this.nick_name = nick_name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getBirth() {
            return birth;
        }

        public void setBirth(String birth) {
            this.birth = birth;
        }

        public Integer getSex() {
            return sex;
        }

        public void setSex(Integer sex) {
            this.sex = sex;
        }

        public String getCard_num() {
            return card_num;
        }

        public void setCard_num(String card_num) {
            this.card_num = card_num;
        }

        public Float getMoney() {
            return money;
        }

        public void setMoney(Float money) {
            this.money = money;
        }

        public Float getReal_money() {
            return real_money;
        }

        public void setReal_money(Float real_money) {
            this.real_money = real_money;
        }

        public Integer getIs_year() {
            return is_year;
        }

        public void setIs_year(Integer is_year) {
            this.is_year = is_year;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getCreate_time() {
            return create_time;
        }

        public void setCreate_time(String create_time) {
            this.create_time = create_time;
        }

        public String getPay_pwd() {
            return pay_pwd;
        }

        public void setPay_pwd(String pay_pwd) {
            this.pay_pwd = pay_pwd;
        }

        public String getMyId() {
            return myId;
        }

        public void setMyId(String myId) {
            this.myId = myId;
        }

        public Integer getExempt_money() {
            return exempt_money;
        }

        public void setExempt_money(Integer exempt_money) {
            this.exempt_money = exempt_money;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getUid() {
            return uid;
        }

        public void setUid(Integer uid) {
            this.uid = uid;
        }

        public Integer getPid() {
            return pid;
        }

        public void setPid(Integer pid) {
            this.pid = pid;
        }

        public Integer getScore() {
            return score;
        }

        public void setScore(Integer score) {
            this.score = score;
        }

        public String getEvaluate() {
            return evaluate;
        }

        public void setEvaluate(String evaluate) {
            this.evaluate = evaluate;
        }

        public String getEval_create_time() {
            return eval_create_time;
        }

        public void setEval_create_time(String eval_create_time) {
            this.eval_create_time = eval_create_time;
        }

        public String getTail() {
            return tail;
        }

        public void setTail(String tail) {
            this.tail = tail;
        }

        public String getPname() {
            return pname;
        }

        public void setPname(String pname) {
            this.pname = pname;
        }
    }

    @Override
    public String toString() {
        return "ProductsBean{" +
                "id=" + id +
                ", uid=" + uid +
                ", cid=" + cid +
                ", name='" + name + '\'' +
                ", type=" + type +
                ", category=" + category +
                ", category_count=" + category_count +
                ", cancel_time=" + cancel_time +
                ", detail='" + detail + '\'' +
                ", info='" + info + '\'' +
                ", postage=" + postage +
                ", price=" + price +
                ", stock=" + stock +
                ", service_time=" + service_time +
                ", classify=" + classify +
                ", consumer_name='" + consumer_name + '\'' +
                ", consumer_address='" + consumer_address + '\'' +
                ", phone='" + phone + '\'' +
                ", status=" + status +
                ", create_time='" + create_time + '\'' +
                ", online_time='" + online_time + '\'' +
                ", deleted=" + deleted +
                ", pcs=" + pcs +
                ", pss=" + pss +
                ", pes=" + pes +
                ", pcos=" + pcos +
                '}';
    }
}
