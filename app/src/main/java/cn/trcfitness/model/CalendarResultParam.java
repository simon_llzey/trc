package cn.trcfitness.model;

import java.util.List;

import cn.trcfitness.http.model.ResultParam;

/**
 * Created by Administrator on 2016/5/23.
 * 日历事件
 */
public class CalendarResultParam extends ResultParam {

    private CalendarData data;

    public CalendarData getData() {
        return data;
    }

    public void setData(CalendarData data) {
        this.data = data;
    }

    public static class CalendarData {

        private List<CalendarEvent> CalendarEvents;

        public List<CalendarEvent> getCalendarEvents() {
            return CalendarEvents;
        }

        public void setCalendarEvents(List<CalendarEvent> calendarEvents) {
            CalendarEvents = calendarEvents;
        }

    }

}
