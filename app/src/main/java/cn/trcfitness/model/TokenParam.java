package cn.trcfitness.model;

/**
 * Created by Administrator on 2016/7/15.
 * 推送保存token参数
 */
public class TokenParam {
    private String token;
    private Integer app_type;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getApp_type() {
        return app_type;
    }

    public void setApp_type(Integer app_type) {
        this.app_type = app_type;
    }

    @Override
    public String toString() {
        return "TokenParam{" +
                "token='" + token + '\'' +
                ", app_type=" + app_type +
                '}';
    }
}
