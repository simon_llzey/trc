package cn.trcfitness.model;

import java.util.List;

import cn.trcfitness.http.model.ResultParam;

/**
 * Created by Administrator on 2016/5/27.
 * 我的邀请码参数
 */
public class UserInviteResultParam extends ResultParam{

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 103
         * uid : 62
         * code : 62FainiG
         * used : false
         */

        private List<Codes> Codes;

        public List<Codes> getCodes() {
            return Codes;
        }

        public void setCodes(List<Codes> Codes) {
            this.Codes = Codes;
        }

    }
}
