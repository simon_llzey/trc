package cn.trcfitness.model;

import java.util.List;

import cn.trcfitness.http.model.ResultParam;

/**
 * Created by ziv on 2016/5/30.
 */
public class ResultBillParam extends ResultParam {


    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {

        private List<ConsumeDetailsBean> ConsumeDetails;

        public List<ConsumeDetailsBean> getConsumeDetails() {
            return ConsumeDetails;
        }

        public void setConsumeDetails(List<ConsumeDetailsBean> ConsumeDetails) {
            this.ConsumeDetails = ConsumeDetails;
        }

        public static class ConsumeDetailsBean {
            private int id;
            private int uid;
            private String name;
            private Float money;
            private Float balance;
            private int type;
            private Long create_time;
            private String remark;
            private Object userDetail;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getUid() {
                return uid;
            }

            public void setUid(int uid) {
                this.uid = uid;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public Float getMoney() {
                return money;
            }

            public void setMoney(Float money) {
                this.money = money;
            }

            public Float getBalance() {
                return balance;
            }

            public void setBalance(Float balance) {
                this.balance = balance;
            }

            public int getType() {
                return type;
            }

            public void setType(int type) {
                this.type = type;
            }

            public Long getCreate_time() {
                return create_time;
            }

            public void setCreate_time(Long create_time) {
                this.create_time = create_time;
            }

            public String getRemark() {
                return remark;
            }

            public void setRemark(String remark) {
                this.remark = remark;
            }

            public Object getUserDetail() {
                return userDetail;
            }

            public void setUserDetail(Object userDetail) {
                this.userDetail = userDetail;
            }
        }
    }
}
