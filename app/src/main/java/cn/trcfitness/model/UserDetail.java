package cn.trcfitness.model;

import java.io.Serializable;

/**
 * Created by Eric
 * on 2016/5/10
 * for project TRC
 */
public class UserDetail implements Serializable {
    private Integer id; //id
    private Integer user_id; //用户ID
    private String name; //姓名
    private String nick_name; //昵称
    private String phone; //电话
    private String birth; //生日
    private Integer sex; //性别
    private String card_num; //会员卡号
    private Float money; //开卡金额
    private Float real_money; //余额
    private Integer is_year; //开卡时是否购买年卡
    private String remark; //备注
    private String img; //头像
    private String create_time; //创建时间
    private String pay_pwd; //支付密码
    private String myId; //我的ID图片地址
    private Integer exempt_money; //免密金额

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getCard_num() {
        return card_num;
    }

    public void setCard_num(String card_num) {
        this.card_num = card_num;
    }

    public Float getMoney() {
        return money;
    }

    public void setMoney(Float money) {
        this.money = money;
    }

    public Float getReal_money() {
        return real_money;
    }

    public void setReal_money(Float real_money) {
        this.real_money = real_money;
    }

    public Integer getIs_year() {
        return is_year;
    }

    public void setIs_year(Integer is_year) {
        this.is_year = is_year;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getPay_pwd() {
        return pay_pwd;
    }

    public void setPay_pwd(String pay_pwd) {
        this.pay_pwd = pay_pwd;
    }

    public String getMyId() {
        return myId;
    }

    public void setMyId(String myId) {
        this.myId = myId;
    }

    public Integer getExempt_money() {
        return exempt_money;
    }

    public void setExempt_money(Integer exempt_money) {
        this.exempt_money = exempt_money;
    }

    @Override
    public String toString() {
        return "UserDetail{" +
                "id=" + id +
                ", user_id=" + user_id +
                ", name='" + name + '\'' +
                ", nick_name='" + nick_name + '\'' +
                ", phone='" + phone + '\'' +
                ", birth='" + birth + '\'' +
                ", sex=" + sex +
                ", card_num='" + card_num + '\'' +
                ", money=" + money +
                ", real_money=" + real_money +
                ", is_year=" + is_year +
                ", remark='" + remark + '\'' +
                ", img='" + img + '\'' +
                ", create_time='" + create_time + '\'' +
                ", pay_pwd='" + pay_pwd + '\'' +
                ", myId='" + myId + '\'' +
                ", exempt_money=" + exempt_money +
                '}';
    }
}
