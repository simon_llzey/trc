package cn.trcfitness.model;

import java.util.List;

import cn.trcfitness.http.model.ResultParam;

/**
 * Created by ziv on 2016/5/25.
 */
public class ResultProductCategoryParam extends ResultParam {


    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {

        private List<ClassifyBean> Classify;

        public List<ClassifyBean> getClassify() {
            return Classify;
        }

        public void setClassify(List<ClassifyBean> Classify) {
            this.Classify = Classify;
        }

        public static class ClassifyBean {
            private Integer id;
            private String name;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }
    }
}
