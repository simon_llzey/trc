package cn.trcfitness.model;

/**
 * Created by ziv on 2016/5/23.
 */
public class RequestProducts {
    String name;
    Integer classify;
    Integer id;
    Integer pageSize;
    Integer pageOffset;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getClassify() {
        return classify;
    }

    public void setClassify(Integer classify) {
        this.classify = classify;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getPageOffset() {
        return pageOffset;
    }

    public void setPageOffset(Integer pageOffset) {
        this.pageOffset = pageOffset;
    }

    @Override
    public String toString() {
        return "RequestProducts{" +
                "name='" + name + '\'' +
                ", classify=" + classify +
                ", id=" + id +
                ", pageSize=" + pageSize +
                ", pageOffset=" + pageOffset +
                '}';
    }
}
