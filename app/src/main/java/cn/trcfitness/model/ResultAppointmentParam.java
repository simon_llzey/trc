package cn.trcfitness.model;

import java.io.Serializable;
import java.util.List;

import cn.trcfitness.http.model.ResultParam;

/**
 * Created by ziv on 2016/5/31.
 * 用户订单
 */
public class ResultAppointmentParam extends ResultParam {

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        private List<CesBean> ces;

        public List<CesBean> getCes() {
            return ces;
        }

        public void setCes(List<CesBean> ces) {
            this.ces = ces;
        }

        public static class CesBean implements Serializable{
            private int id;
            private int uid;
            private Object oid;
            private Object sid;
            private int type;
            private int status;
            private Object reason;
            private String title;
            private String location;
            private String phone;
            private int remind;
            private long begin;
            private long end;
            private String remark;
            private long specific_time;
            private long create_time;
            private boolean deleted;
            private Object sdate;
            private Object edate;
            private Object push_id;
            private OrdersBean order;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getUid() {
                return uid;
            }

            public void setUid(int uid) {
                this.uid = uid;
            }

            public Object getOid() {
                return oid;
            }

            public void setOid(Object oid) {
                this.oid = oid;
            }

            public Object getSid() {
                return sid;
            }

            public void setSid(Object sid) {
                this.sid = sid;
            }

            public int getType() {
                return type;
            }

            public void setType(int type) {
                this.type = type;
            }

            public int getStatus() {
                return status;
            }

            public void setStatus(int status) {
                this.status = status;
            }

            public Object getReason() {
                return reason;
            }

            public void setReason(Object reason) {
                this.reason = reason;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getLocation() {
                return location;
            }

            public void setLocation(String location) {
                this.location = location;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            public int getRemind() {
                return remind;
            }

            public void setRemind(int remind) {
                this.remind = remind;
            }

            public long getBegin() {
                return begin;
            }

            public void setBegin(long begin) {
                this.begin = begin;
            }

            public long getEnd() {
                return end;
            }

            public void setEnd(long end) {
                this.end = end;
            }

            public String getRemark() {
                return remark;
            }

            public void setRemark(String remark) {
                this.remark = remark;
            }

            public long getSpecific_time() {
                return specific_time;
            }

            public void setSpecific_time(long specific_time) {
                this.specific_time = specific_time;
            }

            public long getCreate_time() {
                return create_time;
            }

            public void setCreate_time(long create_time) {
                this.create_time = create_time;
            }

            public boolean isDeleted() {
                return deleted;
            }

            public void setDeleted(boolean deleted) {
                this.deleted = deleted;
            }

            public Object getSdate() {
                return sdate;
            }

            public void setSdate(Object sdate) {
                this.sdate = sdate;
            }

            public Object getEdate() {
                return edate;
            }

            public void setEdate(Object edate) {
                this.edate = edate;
            }

            public Object getPush_id() {
                return push_id;
            }

            public void setPush_id(Object push_id) {
                this.push_id = push_id;
            }

            public OrdersBean getOrder() {
                return order;
            }

            public void setOrder(OrdersBean order) {
                this.order = order;
            }

            @Override
            public String toString() {
                return "CesBean{" +
                        "id=" + id +
                        ", uid=" + uid +
                        ", oid=" + oid +
                        ", sid=" + sid +
                        ", type=" + type +
                        ", status=" + status +
                        ", reason=" + reason +
                        ", title='" + title + '\'' +
                        ", location='" + location + '\'' +
                        ", phone='" + phone + '\'' +
                        ", remind=" + remind +
                        ", begin=" + begin +
                        ", end=" + end +
                        ", remark='" + remark + '\'' +
                        ", specific_time=" + specific_time +
                        ", create_time=" + create_time +
                        ", deleted=" + deleted +
                        ", sdate=" + sdate +
                        ", edate=" + edate +
                        ", push_id=" + push_id +
                        ", order=" + order +
                        '}';
            }
        }

        @Override
        public String toString() {
            return "DataBean{" +
                    "ces=" + ces +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "ResultAppointmentParam{" +
                "data=" + data +
                '}';
    }
}

