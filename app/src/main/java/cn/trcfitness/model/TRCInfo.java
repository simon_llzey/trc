package cn.trcfitness.model;

/**
 * Created by Administrator on 2016/5/11.
 */
public class TRCInfo {
    private String name;
    private Integer image;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getImage() {
        return image;
    }

    public void setImage(Integer image) {
        this.image = image;
    }
}
