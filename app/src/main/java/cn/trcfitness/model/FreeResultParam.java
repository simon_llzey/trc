package cn.trcfitness.model;

import java.util.List;

import cn.trcfitness.http.model.ResultParam;

/**
 * Created by Administrator on 2016/7/7.
 * 免费活动返回参数
 */
public class FreeResultParam extends ResultParam {


    private CalendarFreeEvent data;

    public CalendarFreeEvent getData() {
        return data;
    }

    public void setData(CalendarFreeEvent data) {
        this.data = data;
    }

    public static class CalendarFreeEvent {
        /**
         * 所有的免费 课程 或 活动
         */

        private List<AllCesBean> allCes;
        /**
         * 某天的免费 课程 或 活动
         */
        private List<CesBean> ces;

        public List<AllCesBean> getAllCes() {
            return allCes;
        }

        public void setAllCes(List<AllCesBean> allCes) {
            this.allCes = allCes;
        }

        public List<CesBean> getCes() {
            return ces;
        }

        public void setCes(List<CesBean> ces) {
            this.ces = ces;
        }

        public static class AllCesBean {
            private int id;
            private Object uid;
            private Object oid;
            private Object sid;
            private int type;
            private int status;
            private Object reason;
            private String title;
            private String location;
            private String phone;
            private int remind;
            private long begin;
            private long end;
            private String remark;
            private long specific_time;
            private long create_time;
            private boolean deleted;
            private Object sdate;
            private Object edate;
            private Object order;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public Object getUid() {
                return uid;
            }

            public void setUid(Object uid) {
                this.uid = uid;
            }

            public Object getOid() {
                return oid;
            }

            public void setOid(Object oid) {
                this.oid = oid;
            }

            public Object getSid() {
                return sid;
            }

            public void setSid(Object sid) {
                this.sid = sid;
            }

            public int getType() {
                return type;
            }

            public void setType(int type) {
                this.type = type;
            }

            public int getStatus() {
                return status;
            }

            public void setStatus(int status) {
                this.status = status;
            }

            public Object getReason() {
                return reason;
            }

            public void setReason(Object reason) {
                this.reason = reason;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getLocation() {
                return location;
            }

            public void setLocation(String location) {
                this.location = location;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            public int getRemind() {
                return remind;
            }

            public void setRemind(int remind) {
                this.remind = remind;
            }

            public long getBegin() {
                return begin;
            }

            public void setBegin(long begin) {
                this.begin = begin;
            }

            public long getEnd() {
                return end;
            }

            public void setEnd(long end) {
                this.end = end;
            }

            public String getRemark() {
                return remark;
            }

            public void setRemark(String remark) {
                this.remark = remark;
            }

            public long getSpecific_time() {
                return specific_time;
            }

            public void setSpecific_time(long specific_time) {
                this.specific_time = specific_time;
            }

            public long getCreate_time() {
                return create_time;
            }

            public void setCreate_time(long create_time) {
                this.create_time = create_time;
            }

            public boolean isDeleted() {
                return deleted;
            }

            public void setDeleted(boolean deleted) {
                this.deleted = deleted;
            }

            public Object getSdate() {
                return sdate;
            }

            public void setSdate(Object sdate) {
                this.sdate = sdate;
            }

            public Object getEdate() {
                return edate;
            }

            public void setEdate(Object edate) {
                this.edate = edate;
            }

            public Object getOrder() {
                return order;
            }

            public void setOrder(Object order) {
                this.order = order;
            }

            @Override
            public String toString() {
                return "AllCesBean{" +
                        "id=" + id +
                        ", uid=" + uid +
                        ", oid=" + oid +
                        ", sid=" + sid +
                        ", type=" + type +
                        ", status=" + status +
                        ", reason=" + reason +
                        ", title='" + title + '\'' +
                        ", location='" + location + '\'' +
                        ", phone='" + phone + '\'' +
                        ", remind=" + remind +
                        ", begin=" + begin +
                        ", end=" + end +
                        ", remark='" + remark + '\'' +
                        ", specific_time=" + specific_time +
                        ", create_time=" + create_time +
                        ", deleted=" + deleted +
                        ", sdate=" + sdate +
                        ", edate=" + edate +
                        ", order=" + order +
                        '}';
            }
        }

        public static class CesBean {
            private int id;
            private Object uid;
            private Object oid;
            private Object sid;
            private int type;
            private int status;
            private Object reason;
            private String title;
            private String location;
            private String phone;
            private int remind;
            private long begin;
            private long end;
            private String remark;
            private long specific_time;
            private long create_time;
            private boolean deleted;
            private Object sdate;
            private Object edate;
            private Object order;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public Object getUid() {
                return uid;
            }

            public void setUid(Object uid) {
                this.uid = uid;
            }

            public Object getOid() {
                return oid;
            }

            public void setOid(Object oid) {
                this.oid = oid;
            }

            public Object getSid() {
                return sid;
            }

            public void setSid(Object sid) {
                this.sid = sid;
            }

            public int getType() {
                return type;
            }

            public void setType(int type) {
                this.type = type;
            }

            public int getStatus() {
                return status;
            }

            public void setStatus(int status) {
                this.status = status;
            }

            public Object getReason() {
                return reason;
            }

            public void setReason(Object reason) {
                this.reason = reason;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getLocation() {
                return location;
            }

            public void setLocation(String location) {
                this.location = location;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            public int getRemind() {
                return remind;
            }

            public void setRemind(int remind) {
                this.remind = remind;
            }

            public long getBegin() {
                return begin;
            }

            public void setBegin(long begin) {
                this.begin = begin;
            }

            public long getEnd() {
                return end;
            }

            public void setEnd(long end) {
                this.end = end;
            }

            public String getRemark() {
                return remark;
            }

            public void setRemark(String remark) {
                this.remark = remark;
            }

            public long getSpecific_time() {
                return specific_time;
            }

            public void setSpecific_time(long specific_time) {
                this.specific_time = specific_time;
            }

            public long getCreate_time() {
                return create_time;
            }

            public void setCreate_time(long create_time) {
                this.create_time = create_time;
            }

            public boolean isDeleted() {
                return deleted;
            }

            public void setDeleted(boolean deleted) {
                this.deleted = deleted;
            }

            public Object getSdate() {
                return sdate;
            }

            public void setSdate(Object sdate) {
                this.sdate = sdate;
            }

            public Object getEdate() {
                return edate;
            }

            public void setEdate(Object edate) {
                this.edate = edate;
            }

            public Object getOrder() {
                return order;
            }

            public void setOrder(Object order) {
                this.order = order;
            }
        }
    }
}
