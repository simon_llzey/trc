package cn.trcfitness.model;

/**
 * Created by Administrator on 2016/5/24.
 * 添加日历事件参数
 */
public class AddCalendarEvent {

    private Integer id; //id
    private Integer uid; //用户id
    private Integer oid; //订单ID
    private Integer status; //预约类事件状态 0 等待确认 1 确认 2 完成 3 取消 4 失败
    private String title; //事件标题
    private String location; //事件位置
    private Integer remind; //提醒时间
    private String begin; //开始时间
    private String end; //结束时间
    private String remark; //备注
    private String specific_time; //事件具体时间
    private String create_time; //事件创建时间
    private String specific_month;//当月事件


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getOid() {
        return oid;
    }

    public void setOid(Integer oid) {
        this.oid = oid;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getRemind() {
        return remind;
    }

    public void setRemind(Integer remind) {
        this.remind = remind;
    }

    public String getBegin() {
        return begin;
    }

    public void setBegin(String begin) {
        this.begin = begin;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSpecific_time() {
        return specific_time;
    }

    public void setSpecific_time(String specific_time) {
        this.specific_time = specific_time;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getSpecific_month() {
        return specific_month;
    }

    public void setSpecific_month(String specific_month) {
        this.specific_month = specific_month;
    }
}
