package cn.trcfitness.model;

/**
 * Created by ziv on 2016/6/1.
 * 获取用户订单上传数据
 */
public class RequestOrders {
    Integer id;
    Integer status;
    Integer service_type;
    private Integer pageSize;//一页的大小
    private Integer pageOffset;//从第几个开始

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getPageOffset() {
        return pageOffset;
    }

    public void setPageOffset(Integer pageOffset) {
        this.pageOffset = pageOffset;
    }

    public Integer getId() {

        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getService_type() {
        return service_type;
    }

    public void setService_type(Integer service_type) {
        this.service_type = service_type;
    }
}
