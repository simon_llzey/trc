package cn.trcfitness.model;

/**
 * Created by ziv on 2016/5/11.
 * 获取验证码请求
 */
public class ValidateCode {
    String phone;
    String type;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "ValidateCode{" +
                "phone='" + phone + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
