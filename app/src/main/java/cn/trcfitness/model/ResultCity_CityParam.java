package cn.trcfitness.model;

import java.util.List;

import cn.trcfitness.http.model.ResultParam;

/**
 * Created by ziv on 2016/5/19.
 * 市
 */
public class ResultCity_CityParam extends ResultParam {
    private List<City> data;

    @Override
    public List<City> getData() {
        return data;
    }

    public void setData(List<City> data) {
        this.data = data;
    }


    public class City {
        String id;
        String provinceId;
        String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getProvinceId() {
            return provinceId;
        }

        public void setProvinceId(String provinceId) {
            this.provinceId = provinceId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return "City{" +
                    "id='" + id + '\'' +
                    ", provinceId='" + provinceId + '\'' +
                    ", name='" + name + '\'' +
                    '}';
        }
    }
}
