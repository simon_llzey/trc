package cn.trcfitness.model;

import java.util.List;

import cn.trcfitness.http.model.ResultParam;

/**
 * Created by ziv on 2016/5/31.
 * 用户订单
 */
public class ResultMyOrderPaam extends ResultParam {

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        private List<OrdersBean> Orders;

        public List<OrdersBean> getOrders() {
            return Orders;
        }

        public void setOrders(List<OrdersBean> Orders) {
            this.Orders = Orders;
        }
    }
}
