package cn.trcfitness.model;

/**
 * Created by ziv on 2016/6/8.
 */
public class RequestFeedback {
    String title;
    String content;
    String link;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
