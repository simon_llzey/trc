package cn.trcfitness.model;

/**
 * Created by Administrator on 2016/7/5.
 * 充值请求参数
 */
public class RechargeRequestParam {

    private Integer charge_type;//充值类型 1：支付宝 2：微信
    private Float price;//充值金额

    public Integer getCharge_type() {
        return charge_type;
    }

    public void setCharge_type(Integer charge_type) {
        this.charge_type = charge_type;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }
}
