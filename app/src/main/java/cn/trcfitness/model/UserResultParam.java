package cn.trcfitness.model;

import cn.trcfitness.http.model.ResultParam;

/**
 * Created by Administrator on 2016/5/26.
 * 我的信息上传返回参数
 */
public class UserResultParam extends ResultParam {

    private UploadHead data;

    @Override
    public UploadHead getData() {
        return data;
    }

    public void setData(UploadHead data) {
        this.data = data;
    }

    public static class UploadHead {
        String path = "";

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        @Override
        public String toString() {
            return "UploadHead{" +
                    "path='" + path + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "UserResultParam{" +
                "data=" + data +
                '}';
    }
}
