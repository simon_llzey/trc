package cn.trcfitness.model;

/**
 * Created by ziv on 2016/5/12.
 */
public class ValidateVCode {
    String phone;
    String checkValicode;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCheckValicode() {
        return checkValicode;
    }

    public void setCheckValicode(String checkValicode) {
        this.checkValicode = checkValicode;
    }

    @Override
    public String toString() {
        return "ValidateVCode{" +
                "phone='" + phone + '\'' +
                ", checkValicode='" + checkValicode + '\'' +
                '}';
    }
}
