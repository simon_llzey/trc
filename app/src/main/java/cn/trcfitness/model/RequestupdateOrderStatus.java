package cn.trcfitness.model;

/**
 * Created by ziv on 2016/6/8.
 * 更新订单状态上传参数
 */
public class RequestupdateOrderStatus {
    Integer id;
    Integer status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
