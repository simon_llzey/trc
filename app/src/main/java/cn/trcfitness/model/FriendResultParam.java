package cn.trcfitness.model;

import java.util.List;

import cn.trcfitness.http.model.ResultParam;

/**
 * Created by Administrator on 2016/5/16.
 * 好友列表返回数据
 */
public class FriendResultParam extends ResultParam {



    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {

        private List<Frends> Frends;

        public List<Frends> getFrends() {
            return Frends;
        }

        public void setFrends(List<Frends> Frends) {
            this.Frends = Frends;
        }
    }


}



