package cn.trcfitness.model;

import cn.trcfitness.http.model.ResultParam;

/**
 * Created by ziv on 2016/5/18.
 */
public class UserData extends ResultParam {

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {

        private UserDetail User;

        public UserDetail getUser() {
            return User;
        }

        public void setUser(UserDetail User) {
            this.User = User;
        }
    }
}
