package cn.trcfitness.model;

/**
 * Created by Administrator on 2016/5/25.
 * Trc健身会所，免费课程信息
 */
public class FreeInfo {
    private String name;
    private int image;//分类图标

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

}
