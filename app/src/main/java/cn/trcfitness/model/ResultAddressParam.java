package cn.trcfitness.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import cn.trcfitness.http.model.ResultParam;

/**
 * Created by ziv on 2016/5/18.
 */
public class ResultAddressParam extends ResultParam {
    private DataBean data;

    @Override
    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        List<ReciptAddressparam> ReciptAddress = new ArrayList<ReciptAddressparam>();

        public List<ReciptAddressparam> getReciptAddress() {
            return ReciptAddress;
        }

        public void setReciptAddress(List<ReciptAddressparam> reciptAddress) {
            ReciptAddress = reciptAddress;
        }

    }

    public class ReciptAddressparam implements Serializable {
        private Integer id; //id
        private Integer uid; //用户ID
        private String consignee; //联系人
        private String phone; //联系电话
        private String postcode; //邮编
        private String region; //地区
        private String address; //具体地址
        private Boolean defaults; //是否是默认收货地址

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getUid() {
            return uid;
        }

        public void setUid(Integer uid) {
            this.uid = uid;
        }

        public String getConsignee() {
            return consignee;
        }

        public void setConsignee(String consignee) {
            this.consignee = consignee;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getPostcode() {
            return postcode;
        }

        public void setPostcode(String postcode) {
            this.postcode = postcode;
        }

        public String getRegion() {
            return region;
        }

        public void setRegion(String region) {
            this.region = region;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public Boolean getDefaults() {
            return defaults;
        }

        public void setDefaults(Boolean defaults) {
            this.defaults = defaults;
        }

        @Override
        public String toString() {
            return "ReciptAddressparam{" +
                    "id=" + id +
                    ", uid=" + uid +
                    ", consignee='" + consignee + '\'' +
                    ", phone='" + phone + '\'' +
                    ", postcode='" + postcode + '\'' +
                    ", region='" + region + '\'' +
                    ", address='" + address + '\'' +
                    ", defaults=" + defaults +
                    '}';
        }
    }

}


