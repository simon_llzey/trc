package cn.trcfitness.model;

/**
 * Created by ziv on 2016/5/12.
 * 验证验证码
 */
public class RequestValidateVCode {
    String phone;
    String checkValicode;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCheckValicode() {
        return checkValicode;
    }

    public void setCheckValicode(String checkValicode) {
        this.checkValicode = checkValicode;
    }

}
