package cn.trcfitness.model;

import java.io.Serializable;

/**
 * Created by Administrator on 2016/7/11.
 * 所有日历事件
 */
public class AllEvent implements Serializable{

    //用户添加的事件参数
    private Integer id; //id
    private Integer uid; //用户id
    private Integer oid; //订单ID
    private Integer status; //预约类事件状态 0 等待确认 1 确认 2 完成 3 取消 4 失败
    private String title; //事件标题
    private String location; //事件位置
    private Integer remind; //提醒时间
    private Long begin; //开始时间
    private Long end; //结束时间
    private String remark; //备注
    private Long specific_time; //事件具体时间
    private Long create_time; //事件创建时间

    //免费活动参数
    private Integer sid;
    private Integer type;
    private String reason;
    private String phone;
    private boolean deleted;
    private String sdate;
    private String edate;
    private String order;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getOid() {
        return oid;
    }

    public void setOid(Integer oid) {
        this.oid = oid;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getRemind() {
        return remind;
    }

    public void setRemind(Integer remind) {
        this.remind = remind;
    }

    public Long getBegin() {
        return begin;
    }

    public void setBegin(Long begin) {
        this.begin = begin;
    }

    public Long getEnd() {
        return end;
    }

    public void setEnd(Long end) {
        this.end = end;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Long getSpecific_time() {
        return specific_time;
    }

    public void setSpecific_time(Long specific_time) {
        this.specific_time = specific_time;
    }

    public Long getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Long create_time) {
        this.create_time = create_time;
    }

    public Integer getSid() {
        return sid;
    }

    public void setSid(Integer sid) {
        this.sid = sid;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getSdate() {
        return sdate;
    }

    public void setSdate(String sdate) {
        this.sdate = sdate;
    }

    public String getEdate() {
        return edate;
    }

    public void setEdate(String edate) {
        this.edate = edate;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }
}
