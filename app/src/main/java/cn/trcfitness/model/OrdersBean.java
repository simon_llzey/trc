package cn.trcfitness.model;

import java.io.Serializable;

/**
 * Created by ziv on 2016/7/18.
 * 订单类
 */
public class OrdersBean implements Serializable {
    private Integer id; //id
    private Integer uid; //购买商品用户的ID
    private Integer pid; //商品ID
    private String express_number; //快递单号
    private String order_number; //订单编号
    private Integer status; //订单状态 0 待发货 1 待收货 2 待评价 3 完成 4 退货
    private Integer refund_status; //退货状态 0 未申请退货 1 申请退货 2 成功 3 失败
    private String refund_faild_reason; //退货失败理由
    private String consignee; //联系人
    private String phone; //联系电话
    private String address; //地址
    private String product_name; //商品名称
    private String color_classify; //颜色分类
    private String product_size; //商品尺寸
    private Integer type; //配送方式
    private String message; //买家留言
    private Integer number; //购买数量
    private Float price; //商品单价
    private Float postage; //邮费
    private String completed_time; //订单完成时间
    private String create_time; //订单创建时间
    private Boolean deleted; //是否删除
    private Integer order_type; //订单类型 0 预约类 1 实体类
    private Boolean service_type; //预约类型 0 次数 1 包年
    private Long service_date; //包年购买时间
    private Integer service_number; //次数
    private Integer service_day; //包年天数
    private Integer service_time; //每次服务多长时间
    private String consumer_name; //消费商家名称
    private String consumer_address; //消费商家地址
    private String consumer_phone; //消费商家电话
    private ProductsBean product;//商品
    private String pre_time;//预约时间
    private int closed;
    private Object return_order_status;
    private Object return_time;
    private String postCode;
    private Object send_time;
    private int appointment;




    public ProductsBean getProduct() {
        return product;
    }

    public void setProduct(ProductsBean product) {
        this.product = product;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getExpress_number() {
        return express_number;
    }

    public void setExpress_number(String express_number) {
        this.express_number = express_number;
    }

    public String getOrder_number() {
        return order_number;
    }

    public void setOrder_number(String order_number) {
        this.order_number = order_number;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getRefund_status() {
        return refund_status;
    }

    public void setRefund_status(Integer refund_status) {
        this.refund_status = refund_status;
    }

    public String getRefund_faild_reason() {
        return refund_faild_reason;
    }

    public void setRefund_faild_reason(String refund_faild_reason) {
        this.refund_faild_reason = refund_faild_reason;
    }

    public String getConsignee() {
        return consignee;
    }

    public void setConsignee(String consignee) {
        this.consignee = consignee;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getColor_classify() {
        return color_classify;
    }

    public void setColor_classify(String color_classify) {
        this.color_classify = color_classify;
    }

    public String getProduct_size() {
        return product_size;
    }

    public void setProduct_size(String product_size) {
        this.product_size = product_size;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getPostage() {
        return postage;
    }

    public void setPostage(Float postage) {
        this.postage = postage;
    }

    public String getCompleted_time() {
        return completed_time;
    }

    public void setCompleted_time(String completed_time) {
        this.completed_time = completed_time;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Integer getOrder_type() {
        return order_type;
    }

    public void setOrder_type(Integer order_type) {
        this.order_type = order_type;
    }

    public Boolean getService_type() {
        return service_type;
    }

    public void setService_type(Boolean service_type) {
        this.service_type = service_type;
    }

    public Long getService_date() {
        return service_date;
    }

    public void setService_date(Long service_date) {
        this.service_date = service_date;
    }

    public Integer getService_number() {
        return service_number;
    }

    public void setService_number(Integer service_number) {
        this.service_number = service_number;
    }

    public Integer getService_day() {
        return service_day;
    }

    public void setService_day(Integer service_day) {
        this.service_day = service_day;
    }

    public Integer getService_time() {
        return service_time;
    }

    public void setService_time(Integer service_time) {
        this.service_time = service_time;
    }

    public String getConsumer_name() {
        return consumer_name;
    }

    public void setConsumer_name(String consumer_name) {
        this.consumer_name = consumer_name;
    }

    public String getConsumer_address() {
        return consumer_address;
    }

    public void setConsumer_address(String consumer_address) {
        this.consumer_address = consumer_address;
    }

    public String getConsumer_phone() {
        return consumer_phone;
    }

    public void setConsumer_phone(String consumer_phone) {
        this.consumer_phone = consumer_phone;
    }

    public String getPre_time() {
        return pre_time;
    }

    public void setPre_time(String pre_time) {
        this.pre_time = pre_time;
    }

    public int getClosed() {
        return closed;
    }

    public void setClosed(int closed) {
        this.closed = closed;
    }

    public Object getReturn_order_status() {
        return return_order_status;
    }

    public void setReturn_order_status(Object return_order_status) {
        this.return_order_status = return_order_status;
    }

    public Object getReturn_time() {
        return return_time;
    }

    public void setReturn_time(Object return_time) {
        this.return_time = return_time;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public Object getSend_time() {
        return send_time;
    }

    public void setSend_time(Object send_time) {
        this.send_time = send_time;
    }

    public int getAppointment() {
        return appointment;
    }

    public void setAppointment(int appointment) {
        this.appointment = appointment;
    }


}
