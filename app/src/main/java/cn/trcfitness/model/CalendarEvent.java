package cn.trcfitness.model;

import java.io.Serializable;

/**
 * Created by Administrator on 2016/5/23.
 * 日历事件参数类
 */
public class CalendarEvent implements Serializable {
    private Integer id; //id
    private Integer uid; //用户id
    private Integer oid; //订单ID
    private Integer status; //预约类事件状态 0 等待确认 1 确认 2 完成 3 取消 4 失败
    private String title; //事件标题
    private String location; //事件位置
    private Integer remind; //提醒时间
    private Long begin; //开始时间
    private Long end; //结束时间
    private String remark; //备注
    private Long specific_time; //事件具体时间
    private Long create_time; //事件创建时间

    private Object sid;
    private Integer type;
    private Object reason;
    private Object phone;
    private boolean deleted;
    private Object sdate;
    private Object edate;
    private Object push_id;
    private Object order;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getOid() {
        return oid;
    }

    public void setOid(Integer oid) {
        this.oid = oid;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getRemind() {
        return remind;
    }

    public void setRemind(Integer remind) {
        this.remind = remind;
    }

    public Long getBegin() {
        return begin;
    }

    public void setBegin(Long begin) {
        this.begin = begin;
    }

    public Long getEnd() {
        return end;
    }

    public void setEnd(Long end) {
        this.end = end;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Long getSpecific_time() {
        return specific_time;
    }

    public void setSpecific_time(Long specific_time) {
        this.specific_time = specific_time;
    }

    public Long getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Long create_time) {
        this.create_time = create_time;
    }




    @Override
    public String toString() {
        return "CalendarEvent{" +
                "id=" + id +
                ", uid=" + uid +
                ", oid=" + oid +
                ", status=" + status +
                ", title='" + title + '\'' +
                ", location='" + location + '\'' +
                ", remind=" + remind +
                ", begin=" + begin +
                ", end=" + end +
                ", remark='" + remark + '\'' +
                ", specific_time=" + specific_time +
                ", create_time=" + create_time +
                '}';
    }
    //------------------------------后面添加的参数
    public Object getSid() {
        return sid;
    }

    public void setSid(Object sid) {
        this.sid = sid;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Object getReason() {
        return reason;
    }

    public void setReason(Object reason) {
        this.reason = reason;
    }

    public Object getPhone() {
        return phone;
    }

    public void setPhone(Object phone) {
        this.phone = phone;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Object getSdate() {
        return sdate;
    }

    public void setSdate(Object sdate) {
        this.sdate = sdate;
    }

    public Object getEdate() {
        return edate;
    }

    public void setEdate(Object edate) {
        this.edate = edate;
    }

    public Object getPush_id() {
        return push_id;
    }

    public void setPush_id(Object push_id) {
        this.push_id = push_id;
    }

    public Object getOrder() {
        return order;
    }

    public void setOrder(Object order) {
        this.order = order;
    }
}

