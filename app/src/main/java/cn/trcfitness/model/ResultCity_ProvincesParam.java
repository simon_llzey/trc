package cn.trcfitness.model;

import java.util.List;

import cn.trcfitness.http.model.ResultParam;

/**
 * Created by ziv on 2016/5/19.
 * 省份
 */
public class ResultCity_ProvincesParam extends ResultParam {
    private List<Provincesparpm> data;

    @Override
    public List<Provincesparpm> getData() {
        return data;
    }

    public void setData(List<Provincesparpm> data) {
        this.data = data;
    }


    public class Provincesparpm {
        String id;
        String name;
        String detailListl;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDetailListl() {
            return detailListl;
        }

        public void setDetailListl(String detailListl) {
            this.detailListl = detailListl;
        }

        @Override
        public String toString() {
            return "DataBean{" +
                    "id='" + id + '\'' +
                    ", name='" + name + '\'' +
                    ", detailListl='" + detailListl + '\'' +
                    '}';
        }
    }
}
