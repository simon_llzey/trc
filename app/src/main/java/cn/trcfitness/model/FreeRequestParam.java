package cn.trcfitness.model;

/**
 * Created by Administrator on 2016/7/7.
 * 免费日历请求参数
 */
public class FreeRequestParam {
    //7 免费课程 8 免费活动
    private Integer type;
    //那天的日历事件，yyyy-MM-dd , 月、日 不足两位需补0，如2000-01-05；默认获取当天的
    private String date;


    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
