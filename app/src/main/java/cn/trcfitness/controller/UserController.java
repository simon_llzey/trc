package cn.trcfitness.controller;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;

import java.io.File;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import cn.trcfitness.http.MultipartRequest;
import cn.trcfitness.http.MultipartRequestParams;
import cn.trcfitness.http.RequesterBase;
import cn.trcfitness.http.listener.MyErrorListener;
import cn.trcfitness.http.model.ResultParam;
import cn.trcfitness.http.model.TestResultParam;
import cn.trcfitness.http.services.RequestService;
import cn.trcfitness.http.utils.GsonUtils;
import cn.trcfitness.model.AddCalendarEvent;
import cn.trcfitness.model.CalendarResultParam;
import cn.trcfitness.model.FreeRequestParam;
import cn.trcfitness.model.FreeResultParam;
import cn.trcfitness.model.LoginParam;
import cn.trcfitness.model.RechargeRequestParam;
import cn.trcfitness.model.RechargeResultParam;
import cn.trcfitness.model.RequestChangepwdParam;
import cn.trcfitness.model.RequestFeedback;
import cn.trcfitness.model.RequestOrderParam;
import cn.trcfitness.model.RequestOrders;
import cn.trcfitness.model.RequestProducts;
import cn.trcfitness.model.RequestSaveAddressParam;
import cn.trcfitness.model.RequestUser;
import cn.trcfitness.model.RequestValidateCode;
import cn.trcfitness.model.RequestValidateVCode;
import cn.trcfitness.model.RequestoApplyAppointmentParam;
import cn.trcfitness.model.RequestupdateOrderStatus;
import cn.trcfitness.model.ResultAddressParam;
import cn.trcfitness.model.ResultAppointmentParam;
import cn.trcfitness.model.ResultBalanceParam;
import cn.trcfitness.model.ResultBillParam;
import cn.trcfitness.model.ResultCity_CidParam;
import cn.trcfitness.model.ResultCity_CityParam;
import cn.trcfitness.model.ResultCity_ProvincesParam;
import cn.trcfitness.model.ResultMessageParam;
import cn.trcfitness.model.ResultMyOrderPaam;
import cn.trcfitness.model.ResultProductCategoryParam;
import cn.trcfitness.model.ResultProductParam;
import cn.trcfitness.model.ResultUnPreingOrdersParam;
import cn.trcfitness.model.ResultisUsePwdParam;
import cn.trcfitness.model.TRCParam;
import cn.trcfitness.model.TokenParam;
import cn.trcfitness.model.UserData;
import cn.trcfitness.model.UserDetail;
import cn.trcfitness.model.UserInviteResultParam;
import cn.trcfitness.model.UserRequestParam;
import cn.trcfitness.model.UserResultParam;

/**
 * Created by Eric
 * on 2016/5/10
 * for project TRC
 */
public class UserController extends HttpController {

    /**
     * 登录的方法
     *
     * @param lp
     * @param observer
     */
    public static void loginToMain(LoginParam lp, Observer observer) {
        //调用中间层，进行数据请求
        RequestService.sendRequest(POST, RequesterBase.API_Login, observer, lp, UserData.class);
    }

    public static void getUserInfo(final Observer observer, boolean isAutologin) {
        RequestService.sendRequest(POST, RequesterBase.API_Test, observer, null, TestResultParam.class, isAutologin, new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                getUserInfo(observer, false);
            }
        });
    }

//    public static void registerUser(final Observable observable){
//        RequestService.sendRequest(POST,RequesterBase.API_Regist,observable,);
//
//    }

    /**
     * 获取验证码
     */
    public static void validate_code(final Observer observer, RequestValidateCode requestValidateCode) {
        Log.i("测试111", requestValidateCode.toString());
        RequestService.sendRequest(GET, RequesterBase.
                API_getvalidata + "?phone=" + requestValidateCode.getPhone() + "&type=" + requestValidateCode.getType(), observer, null, ResultParam.class);
    }

    /**
     * 验证验证码是否正确
     */
    public static void v_validatecode(final Observer observer, RequestValidateVCode requestValidateVCode) {
        RequestService.sendRequest(GET, RequesterBase.
                API_testvalidata + "?phone=" + requestValidateVCode.getPhone() + "&checkValicode=" + requestValidateVCode.getCheckValicode(), observer, null, ResultParam.class);
    }

    /**
     * 用户注册
     */
    public static void registerUser(final Observer observer, RequestUser requestUser) {
        RequestService.sendRequest(POST, RequesterBase.
                API_Regist, observer, requestUser, ResultParam.class);
    }

    /**
     * 修改密码
     */
    public static void change_pwd(final Observer observer, RequestChangepwdParam requestChangepwdParam) {
        RequestService.sendRequest(GET,
                RequesterBase.API_ChangePassword + "?phone=" + requestChangepwdParam.getPhone() + "&password=" + requestChangepwdParam.getPassword() + "&validateCode=" + requestChangepwdParam.getValidateCode(), observer, null, ResultParam.class);
    }


    /**
     * 获取收货地址
     */
    public static void get_ReciptAddress(final Observer observer, String id) {
        RequestService.sendRequest(GET, RequesterBase.API_getReciptAddress + id, observer, null, ResultAddressParam.class);

    }

    /**
     * 保存收货地址    增加收货地址
     */
    public static void saveReciptAddress(final Observer observer, RequestSaveAddressParam requestSaveAddressParam) {
        RequestService.sendRequest(POST, RequesterBase.
                API_saveReciptAddress, observer, requestSaveAddressParam, ResultParam.class);
    }

    /**
     * 删除收货地址
     */
    public static void delReciptAddress(final Observer observer, String id) {
        RequestService.sendRequest(GET, RequesterBase.
                API_delReciptAddress + "?id=" + id, observer, null, ResultParam.class);
    }

    /**
     * 获取城市省份列表
     */
    public static void get_Provinces(final Observer observer) {
        RequestService.sendRequest(GET, RequesterBase.
                API_getProvinces, observer, null, ResultCity_ProvincesParam.class);
    }

    /**
     * 获取城市市级列表
     */
    public static void get_City(final Observer observer, String id) {
        RequestService.sendRequest(GET, RequesterBase.
                API_getCitys + "?pid=" + id, observer, null, ResultCity_CityParam.class);
    }

    /**
     * 获取城市区列表
     */
    public static void get_Cid(final Observer observer, String id) {
        RequestService.sendRequest(GET, RequesterBase.
                API_getPositions + "?cid=" + id, observer, null, ResultCity_CidParam.class);
    }

    /**
     * 获取商品详情
     */
    public static void get_Products(final Observer observer, RequestProducts requestProducts) {
        RequestService.sendRequest(POST, RequesterBase.
                API_getProducts, observer, requestProducts, ResultProductParam.class);
    }

    /**
     * 获取商品分类
     */
    public static void get_ProductCategory(final Observer observer) {
        RequestService.sendRequest(POST, RequesterBase.
                API_getProductCategory, observer, null, ResultProductCategoryParam.class);
    }

    /**
     * 设置支付密码
     */
    public static void set_paycode(UserRequestParam requestParam, Observer observer) {
        RequestService.sendRequest(POST, RequesterBase.
                API_PAY_CODE, observer, requestParam, ResultParam.class);
    }


    /**
     * 用户充值
     *
     * @param requestParam
     * @param observer
     */
    public static void set_recharge(UserRequestParam requestParam, Observer observer) {
        RequestService.sendRequest(POST, RequesterBase.
                API_RECHARGE_PAY, observer, requestParam, ResultParam.class);
    }


    /**
     * 校验支付密码
     *
     * @param requestParam
     * @param observer
     */
    public static void set_check_paypasswd(UserRequestParam requestParam, Observer observer) {
        RequestService.sendRequest(POST, RequesterBase.
                API_CHECK_PAYPWD, observer, requestParam, ResultParam.class);
    }


    /**
     * 设置我的信息
     *
     * @param observer
     */
    public static void set_user_info(UserDetail userDetail, Observer observer) {
        RequestService.sendRequest(POST, RequesterBase.
                API_MY_INFO, observer, userDetail, UserResultParam.class);
    }


    /**
     * 获取二维码
     *
     * @param observer
     */
    public static void get_myid(Observer observer) {
        RequestService.sendRequest(POST, RequesterBase.
                API_MY_ID, observer, null, UserResultParam.class);
    }


    /**
     * 获取我的邀请码
     *
     * @param observer
     */
    public static void get_invite_code(Observer observer) {
        RequestService.sendRequest(POST, RequesterBase.
                API_MY_INVITE, observer, null, UserInviteResultParam.class);
    }


    /**
     * 获取日历事件
     *
     * @param calendar 携带详细日期当天的所有事件
     * @param observer
     */
    public static void get_calendar_event(AddCalendarEvent calendar, Observer observer) {
        RequestService.sendRequest(POST, RequesterBase.
                API_CALENDAR_EVENT, observer, calendar, CalendarResultParam.class);
    }

    /**
     * 保存日历事件
     *
     * @param observer
     */
    public static void save_calendar_event(AddCalendarEvent calendarEvent, Observer observer) {
        RequestService.sendRequest(POST, RequesterBase.
                API_CALENDAR_SAVE, observer, calendarEvent, ResultParam.class);
    }


    /**
     * 删除日历事件
     *
     * @param param    事件ID
     * @param observer
     */
    public static void del_calendar_event(UserRequestParam param, Observer observer) {
        RequestService.sendRequest(POST, RequesterBase.
                API_CALENDAR_DEL, observer, param, ResultParam.class);
    }

    /**
     * 免费日历事件
     *
     * @param param    事件ID
     * @param observer
     */
    public static void free_calendar_event(FreeRequestParam param, Observer observer) {
        RequestService.sendRequest(POST, RequesterBase.
                API_FREE_EVENT, observer, param, FreeResultParam.class);
    }


    /**
     * 上传头像图片
     *
     * @param imgs
     * @param observer
     */
    public static void uploadImgs(UserRequestParam param, final Map<String, File> imgs, final Observer observer) {
        MultipartRequestParams params = new MultipartRequestParams();
        params.put("userId", param.getUserId() + "");
        for (Map.Entry<String, File> entry : imgs.entrySet()) {
            params.put("file", imgs.get(entry.getKey()));
        }
        RequestService.getQueue().add(new MultipartRequest(Request.Method.POST, params, RequesterBase.API_Upload_Head, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Tag", "response is " + response);
                UserResultParam result = GsonUtils.asEntity(response, UserResultParam.class);

                if (result.isStatus()) {
                    observer.update(null, result);
                } else {
                    reportError(result);
                    observer.update(null, null);
                }
                //TODO
            }
        }, new MyErrorListener(observer)));

    }

    /**
     * 获取用户订单
     */
    public static void get_bills(Observer observer, int day) {

        String s = "";
        if (day != 0) {
            s = "?days=" + day;
        }

        RequestService.sendRequest(GET, RequesterBase.
                API_Bills + s, observer, null, ResultBillParam.class);
    }

    /**
     * 设置支付免密金额
     */
    public static void set_ExemptMoney(Observer observer, String s) {
        RequestService.sendRequest(GET, RequesterBase.
                API_ExemptMoney + "?money=" + s, observer, null, ResultParam.class);
    }

    /**
     * 45.未消费服务 次数 接口
     */
    public static void get_UnPreingOrders(Observer observer, UserRequestParam userRequest) {
        RequestService.sendRequest(POST, RequesterBase.
                API_UnPreingOrders, observer, userRequest, ResultUnPreingOrdersParam.class);
    }

    /**
     * 28.获取用户订单接口
     */
    public static void get_Orders(Observer observer, RequestOrders requestOrders) {
        RequestService.sendRequest(POST, RequesterBase.
                API_Orders, observer, requestOrders, ResultMyOrderPaam.class);
    }

    /**
     * 保存用户订单接口
     */
    public static void save_Order(Observer observer, RequestOrderParam requestOrderParam) {
        RequestService.sendRequest(POST, RequesterBase.
                API_saveOrder, observer, requestOrderParam, ResultParam.class);
    }

    /**
     * 获取账户余额
     */
    public static void get_Balance(Observer observer) {
        RequestService.sendRequest(POST, RequesterBase.
                API_getBalance, observer, null, ResultBalanceParam.class);
    }

    /**
     * 获取帖子列表
     */
    public static void get_Posts(Observer observer, UserRequestParam pageParam) {
        RequestService.sendRequest(POST, RequesterBase.
                API_getPosts, observer, pageParam, TRCParam.class);
    }

    /**
     * 添加好友
     */
    public static void add_frend(Observer observer, String s) {
        RequestService.sendRequest(GET, RequesterBase.
                API_add_frend + "?uid=" + s, observer, null, ResultParam.class);
    }

    /**
     * 更新订单状态
     */
    public static void updateOrderStatus(Observer observer, RequestupdateOrderStatus requestupdateOrderStatus) {
        RequestService.sendRequest(POST, RequesterBase.
                API_updateOrderStatus, observer, requestupdateOrderStatus, ResultParam.class);
    }

    /**
     * 42.保存用户反馈接口
     */
    public static void save_feedback(Observer observer, RequestFeedback requestFeedback) {
        RequestService.sendRequest(POST, RequesterBase.
                API_save_feedback, observer, requestFeedback, ResultParam.class);
    }


    /**
     * 预约管理
     *
     * @param observer
     * @param param    请求参数
     */
    public static void get_reservation(UserRequestParam param, Observer observer) {
        RequestService.sendRequest(GET, RequesterBase.
                API_RESERVATION, observer, param, ResultAppointmentParam.class);
    }

    /**
     * 42.保存用户反馈接口
     */
    public static void isUsePwd(Observer observer, String money) {
        RequestService.sendRequest(GET, RequesterBase.
                API_isusePwd + "?money=" + money, observer, null, ResultisUsePwdParam.class);
    }


    /**
     * 扫描签到
     *
     * @param observer
     */
    public static void user_sign(String path, UserRequestParam pwd, Observer observer) {
        RequestService.sendRequest(POST, path, observer, pwd, ResultParam.class);

    }


    /**
     * 充值接口
     *
     * @param param
     * @param observer
     */
    public static void user_recharge(RechargeRequestParam param, Observer observer) {
        RequestService.sendRequest(POST, RequesterBase.
                API_RECHARGE, observer, param, RechargeResultParam.class);
    }

    /**
     * 退出登录接口
     *
     * @param observer
     */
    public static void ExitLogin(Observer observer) {
        RequestService.sendRequest(GET, RequesterBase.
                API_ExitLogin, observer, null, ResultParam.class);
    }

    /**
     * 消息列表接口
     *
     * @param observer
     */
    public static void Message(Observer observer,UserRequestParam userRequest) {
        RequestService.sendRequest(POST, RequesterBase.
                API_Message, observer, userRequest, ResultMessageParam.class);
    }

    /**
     * 日历事件分享接口
     *
     * @param observer
     */
    public static void Share(Observer observer, UserRequestParam requestParam) {
        RequestService.sendRequest(POST, RequesterBase.
                API_Share, observer, requestParam, ResultParam.class);
    }

    /**
     * 用户申请预约接口
     *
     * @param observer
     */
    public static void applyAppointment(Observer observer, RequestoApplyAppointmentParam requestParam) {
        Log.e("预约",requestParam.toString());
        RequestService.sendRequest(POST, RequesterBase.
                API_ApplyAppointment, observer, requestParam, ResultParam.class);
    }

    /**
     * 修改支付密码
     */
    public static void change_paypwd(final Observer observer, UserRequestParam userRequestParam) {
        RequestService.sendRequest(POST,
                RequesterBase.API_PayChangePassword, observer, userRequestParam, ResultParam.class);
    }

    /**
     * 消息删除接口
     */
    public static void delMessage(final Observer observer, UserRequestParam userRequestParam) {
        RequestService.sendRequest(POST,
                RequesterBase.API_delMessage, observer, userRequestParam, ResultParam.class);
    }

    /**
     * 33.订单完成后用户对商品进行评价
     */
    public static void evaluate(final Observer observer, UserRequestParam userRequestParam) {
        RequestService.sendRequest(POST,
                RequesterBase.API_evaluate, observer, userRequestParam, ResultParam.class);
    }

    /**
     * 日历分享消息确认接口
     */
    public static void agree(final Observer observer, UserRequestParam userRequestParam) {
        RequestService.sendRequest(POST,
                RequesterBase.API_agree, observer, userRequestParam, ResultParam.class);
    }

    /**
     * 好友申请确认接口
     */
    public static void friendagree(final Observer observer, UserRequestParam userRequestParam) {
        RequestService.sendRequest(POST,
                RequesterBase.API_friendagree, observer, userRequestParam, ResultParam.class);
    }


    /**
     * 保存token
     * @param param
     * @param observer
     */
    public static void save_token(TokenParam param, Observer observer){
        RequestService.sendRequest(POST,
                RequesterBase.API_SAVE_TOKEN, observer, param, ResultParam.class);
    }

    /**
     * 取消预约   56.修改日历事件状态接口
     */
    public static void updateCalendarStatus(final Observer observer, UserRequestParam userRequestParam) {
        RequestService.sendRequest(POST,
                RequesterBase.API_updateCalendarStatus, observer, userRequestParam, ResultParam.class);
    }


}
