package cn.trcfitness.controller;

import java.util.Observer;

import cn.trcfitness.http.RequesterBase;
import cn.trcfitness.http.services.RequestService;
import cn.trcfitness.model.FriendResultParam;
import cn.trcfitness.model.UserRequestParam;

/**
 * Created by Administrator on 2016/5/16.
 */
public class FriendController extends HttpController {

    /**
     * 获取我的好友列表
     *
     * @param observer
     */
    public static void getMyFriendList(Observer observer,UserRequestParam userRequest) {
        RequestService.sendRequest(POST, RequesterBase.API_My_FRIEND_LIST, observer, userRequest, FriendResultParam.class);
    }

    /**
     *获取所有用户
     */
    public static void getFriendList(Observer observer, UserRequestParam userRequest) {
        RequestService.sendRequest(POST, RequesterBase.API_FRIEND_LIST, observer, userRequest, FriendResultParam.class);
    }
}



