package cn.trcfitness.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

/**
 * 用于加载活动中的图片
 * create by Eric
 * create date 2016/05/10
 */
public class LoadImageUtils {
    private HashMap<String, SoftReference<Bitmap>> imageCache;

    public LoadImageUtils(){
        imageCache = new HashMap<String, SoftReference<Bitmap>>();
    }



    public Bitmap loadImage(final String urlStr, final CallBack callBack){
        //读取链接/后面的作为保存图片名称，防止重名，（/）替换成（转义，zy）将前3位字符加入到名称中
        String sub = urlStr.substring(urlStr.lastIndexOf("/")-3, urlStr.length());
        //将替换后的字符串作为图片名称
        final String subStr = sub.replace("/", "zy");

        if (imageCache.containsKey(subStr)){
            Bitmap bitmap = getCacheImage(subStr);
            if (bitmap != null)
                return bitmap;
        }


        final Handler handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                //使用接口回调更新view
                callBack.getImage((Bitmap) msg.obj, urlStr);
            }
        };


        new Thread(){
            @Override
            public void run() {

                URL url;
                InputStream input = null;
                try {
                    url = new URL(urlStr);
                    input = url.openStream();

                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 5;
                    options.inPreferredConfig = Bitmap.Config.RGB_565;
                    Bitmap bitmap = BitmapFactory.decodeStream(input, null, options);
                    setCacheImage(subStr, bitmap);

                    //通知handler更新view
                    Message message = Message.obtain();
                    message.obj = bitmap;
                    handler.sendMessage(message);


                } catch (MalformedURLException e1) {
                    e1.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }.start();

        return null;
    }



    public Bitmap getCacheImage(String imageUrl){
        SoftReference<Bitmap> softReference = imageCache.get(imageUrl);
        if (softReference != null){
            Bitmap bitmap = softReference.get();
            return bitmap;
        }

        return null;
    }

    public void setCacheImage(String subStr, Bitmap bitmap){

        if (getCacheImage(subStr) == null && bitmap != null)
            imageCache.put(subStr, new SoftReference<Bitmap>(bitmap));
    }


    /**
     * 定义接口用于更新图片
     */
    public interface CallBack{
        public void getImage(Bitmap bitmap, String url);
    }





















}
