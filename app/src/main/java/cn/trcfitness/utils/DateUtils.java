package cn.trcfitness.utils;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Administrator on 2016/5/24.
 * 时间转换
 */
public class DateUtils {


    /**
     * 年月日时分转时间戳
     *
     * @param time
     * @return
     */
    public static Long date(String time) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Long d = 0L;
        try {
            Date date = format.parse(time);
            d = date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return d;
    }

    /**
     * 时间戳转年月日时分秒
     *
     * @param time
     * @return
     */
    public static String date2(long time) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        String dateStr = "";
        dateStr = format.format(time);

        return dateStr;
    }

    /**
     * 时间戳转年月日时分
     *
     * @param time
     * @return
     */
    public static String date3(long time) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        String dateStr = "";
        dateStr = format.format(time);

        return dateStr;
    }

    /**
     * 时间戳转时分
     *
     * @param time
     * @return
     */
    public static String date4(long time) {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        String dateStr = "";
        dateStr = format.format(time);

        return dateStr;
    }

    /**
     * 时间戳转年月日
     *
     * @param time
     * @return
     */
    public static String timesTurnDate(Long time) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
        Long t = new Long(time);
        String d = format.format(t);
        return d;
    }

    /**
     * 时间戳转年月日格式不同
     *
     * @param time
     * @return
     */
    public static String yearMonthDay(Long time) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Long t = new Long(time);
        String d = format.format(t);
        return d;
    }

    /**
     * 时间戳获取日
     *
     * @param time
     * @return
     */
    public static String timesTurnDay(Long time) {
        SimpleDateFormat format = new SimpleDateFormat("dd");
        Long t = new Long(time);
        String d = format.format(t);
        return d;
    }

    /**
     * 时间戳获取年月
     *
     * @param time
     * @return
     */
    public static String timesTurnMonth(Long time) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM");
        Long t = new Long(time);
        String d = format.format(t);
        return d;
    }

    /**
     * 年月日转时间戳
     *
     * @param time
     * @return
     */
    public static Long dateTurnTimes(String time) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
        Long d = 0L;
        try {
            Date date = format.parse(time);
            d = date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return d;
    }


    /**
     * 时间转时间戳
     *
     * @param time
     * @return
     */
    public static Long timeTurnTimes(String time) {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
        Long d = 0L;
        try {
            Date date = format.parse(time);
            d = date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return d;
    }

    /**
     * 时间戳转时间
     *
     * @param time
     * @return
     */
    public static String timesTurnTime(Long time) {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
        Long t = new Long(time);
        String d = format.format(t);
        return d;
    }

    /**
     * 时间戳转化为Date
     *
     * @return
     */
    public static Date timesTurnDate1(Long time) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String d = format.format(time);
        Date date = null;
        try {
            date = format.parse(d);
        } catch (ParseException e) {
            e.printStackTrace();
            Log.e("DateUtils", "时间戳转化为Date错误");
        }
        return date;
    }

}
