package cn.trcfitness.utils;

import android.content.Context;
import android.os.CountDownTimer;
import android.widget.TextView;

import cn.trcfitness.R;


/**
 * Created by dreamtec-001 on 2016/1/19.
 * 获取验证码时倒计时
 */
public class TimerCount extends CountDownTimer {

    private Context context;
    private TextView textView;
    public TimerCount(long millisInFuture, long countDownInterval, Context c, TextView t) {
        super(millisInFuture, countDownInterval);
        this.context = c;
        this.textView = t;
    }

    @Override
    public void onFinish() {
        textView.setClickable(true);
        textView.setText("重新获取验证码");
        textView.setTextSize(16);
        textView.setTextColor(context.getResources().getColor(R.color.un_countdown_color));
    }

    @Override
    public void onTick(long arg0) {
        textView.setClickable(false);
        textView.setText((arg0 / 1000)+"s后重新获取");
        textView.setTextSize(16);
        textView.setTextColor(context.getResources().getColor(R.color.countdown_color));
    }
}
