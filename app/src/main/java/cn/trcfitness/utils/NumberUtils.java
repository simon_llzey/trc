package cn.trcfitness.utils;

import java.text.DecimalFormat;

/**
 * Created by Administrator on 2016/7/27.
 */

public class NumberUtils {

    /**
     * 保留两位小数
     * @param num
     * @return
     */
    public static String getDecimalpoint2(Float num){
        DecimalFormat fnum = new DecimalFormat("##0.00");
        String dd=fnum.format(num);
        return dd;
    }

}
