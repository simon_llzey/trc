package cn.trcfitness.utils;

/**
 * Created by Administrator on 2016/7/13.
 */
public class KeyUtils {
    //支付宝的
    // 商户PID
    public static final String PARTNER = "2088221830989462";
    // 商户收款账号
    public static final String SELLER = "2218876301@qq.com";
    // 商户私钥，pkcs8格式
    public static final String RSA_PRIVATE = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAMm2WZO3s8frpI7N" +
            "x+5z6wdFW6KiHVS+7aJ+Ahg+OzQu58DsKLoB6hktOu6/ymPZl31NVHwK7GiahFcg" +
            "qkKhepyb1rVRGZ/RavSKXim/fKIF289VoeJcwuUlt/+iaIptE9nofTQbRrWDtbLc" +
            "6SGstkxaBZGmPfSP4bRSCN8vOP+hAgMBAAECgYAOktUv/gLIO/c8thNu4AMoGFPB" +
            "kVLrL4w9xDrkg4KFh6oI30cmAUNnpEPwwfyecDcov3COMb/Y4ukS3nJOtBoGm8cY" +
            "7knVBiVkiOS/czapVU89N0l/nkIOyqc2YN74n8+mzjew2alOyqZBxJkPIGfoEj7X" +
            "3URDg+m7+FDU0TSgAQJBAOjt7B+SQYkPsEF8DzC3SlcMlNHl+/LY+K3KmrSpad5Q" +
            "7pA4f+xzlIC3LtcV3AjX4D+pFyCw91aV1YzDbe9EaKECQQDdsOXhogF0EqyPv23f" +
            "6sfDG262oAHkY7sDodzzFIYauNRtmO8mOELOD71yuPUA3BJYmGCQdwzkq6BgV345" +
            "OjcBAkAQsLuyp3A6uf0PNCyCeR/CjQw83hXnmNR434OmXl1PqMzvWJ0R/FYvwvCA" +
            "SPizSYSwCxW1KfkPIz1iEL7X3jcBAkEAsLKqx+jk16t6tyiRXwQ3LJ/U4uMuHugO" +
            "y5pcDbOyH+JqDuDy3LfDJ5MMIUCip4jBZrQtY3hAYpMZRTD3fo2UAQJBAJd4zFYc" +
            "VdkrWWsWf30wddR2Y2eOBhZW229o58bIYTvDVUzi7r2EK2mHsEFtDFO7XPQmVeIS" +
            "1OJAO5lmbIevTwk=";
    // 支付宝公钥
    public static final String RSA_PUBLIC = "MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBALEbsQ+Jfk8P9HQ2sujVMZtaC/TCxUzAeR" +
            "OSDlL1XtkZ4H9tl0KjLU2CB/02gIXsn3RJKdWg7jVPHGNwFTgzT/sCAwEAAQ==";


    //微信key
    public static final String APP_ID = "wx3c6f488d79a54028";


}
