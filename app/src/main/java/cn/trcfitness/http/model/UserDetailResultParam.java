package cn.trcfitness.http.model;

import cn.trcfitness.model.UserDetail;

/**
 * Created by Eric
 * on 2016/5/10
 * for project TRC
 */
public class UserDetailResultParam extends ResultParam {
    @Override
    public UserDetail getData() {
        return data.getUser();
    }

    public void setData(UserDetailReultData data) {
        this.data = data;
    }

    private UserDetailReultData data;

    public class UserDetailReultData {
        public UserDetail getUser() {
            return User;
        }

        public void setUser(UserDetail user) {
            User = user;
        }

        private UserDetail User;
    }
}
