package cn.trcfitness.http;

/**
 * create by Eric
 * create date 2016/05/10
 */
public class RequesterBase {
    protected static final String API_Root = "http://www.trcfitness.cn/trc/services/";
//    protected static final String API_Root = "http://www.xnljl.com/BlueCollar/services/";
//    protected static final String API_Root = "http://10.0.0.25:8080/beautymodel/services/";

    public static final String API_Get_Update_Info = API_Root + "upgrade/info"; //获取更新
    public static final String API_Setting = API_Root + "getSetting";//获取后台设置接口
    public static final String API_SavePushMsg = API_Root + "savePushMsg";//保存推送信息
    public static final String API_SaveFeedBack = API_Root + "user/saveFeedBack";//用户0反馈保存

    public static final String API_Upload_Img = API_Root + "upload/uploadImage";
    public static final String API_Upload_Head = API_Root + "upload/uploadHeadImage";//头像替换

    public static final String API_Test = API_Root + "user/information";

    public static final String API_Login = API_Root + "login";
    public static final String API_Login_Out = API_Root + "login_out";
    public static final String API_Regist = API_Root + "reg";//注册新用户接口
    public static final String API_getvalidata = API_Root + "validate_code";//获取验证码接口
    public static final String API_testvalidata = API_Root + "checkValicode";//4.检验验证码是否正确接口
    public static final String API_ChangePassword = API_Root + "change_pwd";//修改密码

    //商城
    public static final String API_getReciptAddress = API_Root + "user/getReciptAddress";//获取收货地址接口
    public static final String API_saveReciptAddress = API_Root + "user/saveReciptAddress";//保存收货地址接口
    public static final String API_delReciptAddress = API_Root + "user/delReciptAddress";//保存收货地址接口


    public static final String API_getProvinces = API_Root + "getProvinces";//城市省份
    public static final String API_getCitys = API_Root + "getCitys";//城市市级
    public static final String API_getPositions = API_Root + "getPositions";//城市区


    public static final String API_getProducts = API_Root + "product/getProducts";//获取商品详情
    public static final String API_getProductCategory = API_Root + "product/getProductCategory";//获取商品分类


    //我的好友列表
    public static final String API_My_FRIEND_LIST = API_Root + "user/frends";
    //获取所有用户
    public static final String API_FRIEND_LIST = API_Root + "user/findFrends";
    //添加好友
    public static final String API_add_frend = API_Root + "user/add_frend";

    //设置支付密码
    public static final String API_PAY_CODE = API_Root + "user/setPayPwd";
    //用户充值
    public static final String API_RECHARGE_PAY = API_Root + "user/recharge";
    //校验支付密码
    public static final String API_CHECK_PAYPWD = API_Root + "user/checkPayPwd";

    //个人信息设置
    public static final String API_MY_INFO = API_Root + "user/setting";
    //我的ID，二维码
    public static final String API_MY_ID = API_Root + "user/getMyId";
    //我的邀请码
    public static final String API_MY_INVITE = API_Root + "user/myCode";
    //预约管理
    public static final String API_RESERVATION = API_Root + "order/getPreingOrders";

    //日历事件
    public static final String API_CALENDAR_EVENT = API_Root + "user/getCalendarEvent";
    //保存日历
    public static final String API_CALENDAR_SAVE = API_Root + "user/saveCalendarEvent";
    //删除日历
    public static final String API_CALENDAR_DEL = API_Root + "appointment/delCalendarEvent";

    //获取账单
    public static final String API_Bills = API_Root + "user/getBills";

    //设置支付免密金额
    public static final String API_ExemptMoney = API_Root + "user/setExemptMoney";
    //36.用户获取自己的次数订单接口
    public static final String API_UnPreingOrders = API_Root + "order/getUnPreingOrders";
    //28.获取用户订单接口
    public static final String API_Orders = API_Root + "user/getOrders";

    //保存用户订单接口
    public static final String API_saveOrder = API_Root + "user/saveOrder";
    //获取账户余额
    public static final String API_getBalance = API_Root + "user/getBalance";

    //判断支付时是否需要密码接口
    public static final String API_isusePwd = API_Root + "user/usePwd";

    //更新用户订单状态
    public static final String API_updateOrderStatus = API_Root + "user/updateOrderStatus";


    //获取帖子列表
    public static final String API_getPosts = API_Root + "post/getPosts";

    //保存用户反馈接口
    public static final String API_save_feedback = API_Root + "feedback/save_feedback";

    //用户充值接口
    public static final String API_RECHARGE = API_Root + "user/recharge";

    //用户退出登录接口
    public static final String API_ExitLogin = API_Root + "login_out";

    //消息列表接口
    public static final String API_Message = API_Root + "message/list";

    //免费日历事件
    public static final String API_FREE_EVENT = API_Root + "appointment/getFree";

    //日历事件分享接口
    public static final String API_Share = API_Root + "appointment/share";

    //用户申请预约接口
    public static final String API_ApplyAppointment = API_Root + "appointment/applyAppointment";

    //修改密码
    public static final String API_PayChangePassword = API_Root + "user/forgetPayPwd";

    //删除消息
    public static final String API_delMessage = API_Root + "message/delMessage";

    //评价订单
    public static final String API_evaluate = API_Root + "user/evaluate";

    //日历分享消息确认
    public static final String API_agree = API_Root + "message/agree";

    //好友申请确认
    public static final String API_friendagree = API_Root + "user/isAgree";

    //保存token
    public static final String API_SAVE_TOKEN = API_Root+"user/saveToken";

    //取消预约  56.修改日历事件状态接口
    public static final String API_updateCalendarStatus = API_Root+"appointment/updateCalendarStatus";

}
