package cn.trcfitness.view.fragmet;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshScrollView;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import cn.trcfitness.R;
import cn.trcfitness.application.OverallApplication;
import cn.trcfitness.controller.UserController;
import cn.trcfitness.custom.MyDialog;
import cn.trcfitness.custom.MyGridView;
import cn.trcfitness.model.ProductsBean;
import cn.trcfitness.model.RequestProducts;
import cn.trcfitness.model.ResultProductCategoryParam;
import cn.trcfitness.model.ResultProductParam;
import cn.trcfitness.view.activity.StoreDetailsActivity;
import cn.trcfitness.view.activity.StorePerActivity;
import cn.trcfitness.view.adapter.ProductAdapter;
import cn.trcfitness.view.adapter.ProductCategoryAdapter;


/**
 * 商店
 */
public class StoreFra extends Fragment implements View.OnClickListener {
    private View view;
    private LinearLayout Linlay_percenter;
    private ListView lv;
    private MyGridView gv;
    private ResultProductParam resultProductParam;//商品详情
    private TextView tv_storename;
    private ImageView im_storename;
    private LinearLayout Linlay_no_content;
    private LinearLayout Linlay_progressbar;

    private List<ResultProductCategoryParam.DataBean.ClassifyBean> List_Category;
    private List<ProductsBean> List_Product;
    private ProductAdapter productAdapter;//商品详情适配器
    private ProductCategoryAdapter productCategoryAdapter;//商品分类适配器

    private PullToRefreshScrollView scrollView;
    private RequestProducts requestProducts;//获取商品上传数据
    private int pageOffset = 0;//页数
    private static final int REFRESH = 0;
    private static final int MORE = 1;

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    Linlay_progressbar.setVisibility(View.GONE);
                    if (List_Product != null && !List_Product.isEmpty()) {
                        Linlay_no_content.setVisibility(View.GONE);
                    } else {
                        Linlay_no_content.setVisibility(View.VISIBLE);
                    }
                    productAdapter.setList(List_Product);
                    scrollView.onRefreshComplete();
                    productAdapter.notifyDataSetChanged();
                    break;
            }
            super.handleMessage(msg);
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_store, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
        loadData();
    }

    private void initView() {
        Linlay_percenter = (LinearLayout) view.findViewById(R.id.linlay_activity_store_percenter);//个人中心
        lv = (ListView) view.findViewById(R.id.lv_fragment_store);
        gv = (MyGridView) view.findViewById(R.id.gv_fragment_store);
        tv_storename = (TextView) view.findViewById(R.id.tv_fragment_store_storename);
        im_storename = (ImageView) view.findViewById(R.id.im_fragment_store_storename);
        scrollView = (PullToRefreshScrollView) view.findViewById(R.id.sc_fragment_store);
        Linlay_no_content = (LinearLayout) view.findViewById(R.id.Linlay_frigment_store_no_content);
        Linlay_progressbar = (LinearLayout) view.findViewById(R.id.Linlay_fragment_strre_progressbar);

        Linlay_percenter.setOnClickListener(this);
        im_storename.setOnClickListener(this);

        scrollView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ScrollView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ScrollView> refreshView) {
                pageOffset = 0;
                requestProducts.setPageOffset(pageOffset);
                getProducts(REFRESH);
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ScrollView> refreshView) {
                if (List_Product != null && List_Product.size() > 0) {
                    pageOffset = List_Product.size();
                }
                requestProducts.setPageOffset(pageOffset);
                getProducts(MORE);
            }
        });

        tv_storename.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    HideKeyboard(v);

                    requestProducts.setName(tv_storename.getText().toString().trim());
                    requestProducts.setClassify(null);
                    requestProducts.setPageOffset(0);
                    getProducts(REFRESH);
                    productCategoryAdapter.setCurrentItem(-1);
                    productCategoryAdapter.notifyDataSetChanged();
                    lv.setAdapter(productCategoryAdapter);
                }
                return false;
            }
        });
    }

    /**
     * @param v 隐藏虚拟键盘
     */
    public static void HideKeyboard(View v) {
        InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm.isActive()) {
            imm.hideSoftInputFromWindow(v.getApplicationWindowToken(), 0);
        }
    }

    /**
     * 初始化数据 显示左边商品分类ListView
     */
    private void loadData() {
        requestProducts = new RequestProducts();
        requestProducts.setPageSize(20);

        UserController.get_ProductCategory(new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                if (data != null && (ResultProductCategoryParam) data != null) {
                    ResultProductCategoryParam resultProductCategoryParam = (ResultProductCategoryParam) data;
                    if (resultProductCategoryParam.getStatus().equals("200")) {
                        List_Category = resultProductCategoryParam.getData().getClassify();
                        ResultProductCategoryParam.DataBean.ClassifyBean classifyBean = new ResultProductCategoryParam.DataBean.ClassifyBean();
                        classifyBean.setName("全部商品");
                        classifyBean.setId(0);
                        List_Category.add(0, classifyBean);
                        productCategoryAdapter = new ProductCategoryAdapter(List_Category, getActivity());
                        lv.setAdapter(productCategoryAdapter);
                    }
                }
            }
        });

        List_Product = new ArrayList<ProductsBean>();
        productAdapter = new ProductAdapter(List_Product, getActivity());
        gv.setAdapter(productAdapter);
        getProducts(REFRESH);  //初始化商品数据

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                productCategoryAdapter.setCurrentItem(position);
                productCategoryAdapter.notifyDataSetChanged();

                if (!(List_Category.get(position).getId() == 0)) {
                    requestProducts.setClassify(List_Category.get(position).getId());
                } else {
                    requestProducts.setClassify(null);
                }
                requestProducts.setPageOffset(0);
                requestProducts.setName("");

                List_Product.clear();
                productAdapter.notifyDataSetChanged();
                Linlay_progressbar.setVisibility(View.VISIBLE);
                Linlay_no_content.setVisibility(View.GONE);
                getProducts(REFRESH);
            }

        });

        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), StoreDetailsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("productsBean", productAdapter.getItem(position));//传递商品对象到商品详情页
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    /**
     * 获取商品详情 并刷新显示在GridView
     *
     * @param state
     */
    private void getProducts(final int state) {
        UserController.get_Products(new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                if (data != null && (ResultProductParam) data != null) {
                    resultProductParam = (ResultProductParam) data;
                    if (resultProductParam.getStatus().equals("200")) {
                        Message msg = new Message();
                        msg.what = 1;
                        if (state == REFRESH) {
                            List_Product = resultProductParam.getData().getProducts();
                            gv.smoothScrollToPositionFromTop(0, 0);
                            handler.sendMessage(msg);
                        } else if (state == MORE) {
                            List_Product.addAll(resultProductParam.getData().getProducts());
                            handler.sendMessage(msg);
                            if (resultProductParam.getData().getProducts().size() == 0) {
                                mToast("没有更多数据");
                            }
                        }
                    }
                } else {
                    scrollView.onRefreshComplete();
                }
            }
        }, requestProducts);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.linlay_activity_store_percenter://跳转到个人中心
                if (OverallApplication.tourists) {
                    MyDialog.Dialog(getActivity());
                    return;
                }
                Intent intent = new Intent(getActivity(), StorePerActivity.class);
                startActivity(intent);
                break;
            case R.id.im_fragment_store_storename://清空输入框
                tv_storename.setText("");
                break;
        }
    }

    private void mToast(String text) {
        Toast.makeText(getContext().getApplicationContext(), text, Toast.LENGTH_SHORT).show();
    }
}
