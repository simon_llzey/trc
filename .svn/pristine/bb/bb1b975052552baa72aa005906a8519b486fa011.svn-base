package cn.trcfitness.view.activity.trc;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Toast;


import com.alipay.sdk.app.PayTask;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;
import java.util.Random;

import cn.trcfitness.R;
import cn.trcfitness.controller.UserController;
import cn.trcfitness.model.RechargeRequestParam;
import cn.trcfitness.model.RechargeResultParam;
import cn.trcfitness.utils.KeyUtils;
import cn.trcfitness.view.pay.H5PayDemoActivity;
import cn.trcfitness.view.pay.PayResult;
import cn.trcfitness.view.pay.SignUtils;
import cn.trcfitness.wxapi.WXEntryActivity;

/**
 * 充值-选择支付方式
 */
public class SelectPayAct extends Activity implements View.OnClickListener {

    private Context mContext = this;
    RechargeRequestParam requestParam = new RechargeRequestParam();

    //充值的金额
    private Float money_num = 0f;


    //订单号
    String feeNumber = null;


    private static final int SDK_PAY_FLAG = 1;

    private static final int DATA_RESILT_FLAG = 2;

    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        @SuppressWarnings("unused")
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case SDK_PAY_FLAG: {
                    PayResult payResult = new PayResult((String) msg.obj);
                    /**
                     * 同步返回的结果必须放置到服务端进行验证（验证的规则请看https://doc.open.alipay.com/doc2/
                     * detail.htm?spm=0.0.0.0.xdvAU6&treeId=59&articleId=103665&
                     * docType=1) 建议商户依赖异步通知
                     */
                    String resultInfo = payResult.getResult();// 同步返回需要验证的信息

                    String resultStatus = payResult.getResultStatus();
                    // 判断resultStatus 为“9000”则代表支付成功，具体状态码代表含义可参考接口文档
                    if (TextUtils.equals(resultStatus, "9000")) {
                        Toast.makeText(mContext, "支付成功", Toast.LENGTH_SHORT).show();
                        Intent successIntent = new Intent(getApplicationContext(), PayResultAct.class);
                        successIntent.putExtra("isSuccess" , true);
                        successIntent.putExtra("recharge", money_num);
                        startActivity(successIntent);
                    } else {
                        // 判断resultStatus 为非"9000"则代表可能支付失败
                        // "8000"代表支付结果因为支付渠道原因或者系统原因还在等待支付结果确认，最终交易是否成功以服务端异步通知为准（小概率状态）
                        if (TextUtils.equals(resultStatus, "8000")) {
                            Toast.makeText(mContext, "支付结果确认中", Toast.LENGTH_SHORT).show();
                        } else {
                            // 其他值就可以判断为支付失败，包括用户主动取消支付，或者系统返回的错误
                            Toast.makeText(mContext, "支付失败"+resultInfo, Toast.LENGTH_SHORT).show();

                        }
                    }
                    break;
                }
                default:
                    break;
            }
        }
    };


    /**
     * 获取服务器返回的订单
     */
    private Handler resHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what){
                case DATA_RESILT_FLAG:
                    feeNumber = (String) msg.obj;
                    pay();
                    break;
            }

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_select_pay);
        initView();
    }

    private void initView() {

        money_num = getIntent().getFloatExtra("money_num", 0);
        requestParam.setPrice(money_num);
        Log.i("Select", "支付宝信息金额"+ money_num);
        findViewById(R.id.back).setOnClickListener(this);
        findViewById(R.id.zhifubao_lin).setOnClickListener(this);
        findViewById(R.id.weixin_lin).setOnClickListener(this);

    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                SelectPayAct.this.finish();
                break;

            case R.id.zhifubao_lin:
                getData(1);
                break;
            case R.id.weixin_lin:
                getData(2);
                break;
        }
    }


    /**
     * @param chargeType 充值类型
     */
    private void getData(int chargeType){
        requestParam.setCharge_type(chargeType);

        UserController.user_recharge(requestParam, new Observer() {
            @Override
            public void update(Observable observable, Object data) {
                RechargeResultParam result = (RechargeResultParam)data;
                if (result != null){
                    RechargeResultParam.PayBean payBean = result.getData();
                    if (payBean != null){
                        if (result.isStatus()){
                            if (requestParam.getCharge_type() == 1){
                                Log.i("Select", "支付宝信息"+payBean.getFeeNumber());
                                Message message = Message.obtain();
                                message.what = DATA_RESILT_FLAG;
                                message.obj = payBean.getFeeNumber();
                                resHandler.handleMessage(message);
                            }else {
                                Intent weixinIntent = new Intent(mContext, WXEntryActivity.class);
                                Bundle bundle= new Bundle();
                                bundle.putSerializable("weixin", payBean);
                                startActivity(weixinIntent);
                            }
                        }
                    } else {
                        Toast.makeText(mContext, "没有数据", Toast.LENGTH_LONG).show();
                    }
                }

            }
        });
    }


    /**
     * call alipay sdk pay. 调用SDK支付
     */
    public void pay() {
        if (TextUtils.isEmpty(KeyUtils.PARTNER) || TextUtils.isEmpty(KeyUtils.RSA_PRIVATE) || TextUtils.isEmpty(KeyUtils.SELLER)) {
            new AlertDialog.Builder(this).setTitle("警告").setMessage("需要配置PARTNER | RSA_PRIVATE| SELLER")
                    .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialoginterface, int i) {
                            //
                            finish();
                        }
                    }).show();
            return;
        }
        String orderInfo = ZFB.getOrderInfo("测试的商品", "该测试商品的详细描述", "0.01", feeNumber);

        /**
         * 特别注意，这里的签名逻辑需要放在服务端，切勿将私钥泄露在代码中！
         */
        String sign = ZFB.sign(orderInfo);
        try {
            /**
             * 仅需对sign 做URL编码
             */
            sign = URLEncoder.encode(sign, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        /**
         * 完整的符合支付宝参数规范的订单信息
         */
        final String payInfo = orderInfo + "&sign=\"" + sign + "\"&" + ZFB.getSignType();

        Runnable payRunnable = new Runnable() {

            @Override
            public void run() {
                // 构造PayTask 对象
                PayTask alipay = new PayTask(SelectPayAct.this);
                // 调用支付接口，获取支付结果
                String result = alipay.pay(payInfo, true);

                Message msg = new Message();
                msg.what = SDK_PAY_FLAG;
                msg.obj = result;
                mHandler.sendMessage(msg);
            }
        };

        // 必须异步调用
        Thread payThread = new Thread(payRunnable);
        payThread.start();
    }




}
